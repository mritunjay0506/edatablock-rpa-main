package com.edatablock.rpa.common;

public class Constants {
	
	public static final String SYSTEM = "SYSTEM";
	public static final String OCR_PROCESSED = "OCR_PROCESSED";
	public static final String SUCCESS = "SUCCESS";
	
	//private static final String SYSTEM= "SYSTEM";

	public static final String EMAIL_ADDRESS_NOT_MATCH = "Client didn't match";
	public static final String ERROR_CODE_CLIENT_NOT_MATCH = "CLIENT_NOT_MATCH";
	public static final String PROCESS_TYPE_EMAIL_PROCESSING = "EMAIL_PROCESSING";
	public static final int PROCESS_ID_EMAIL_PROCESSING = 222;
	
	public static final String EMAIL_ADDRESS_KEY = "clientEmailAddress";
	public static final String MESSAGE_ID_KEY = "emailMessageId";
	public static final String FILENAME_KEY = "attachmentfileName";
	public static final String TEMPLATE_ID_KEY = "inputTemplateId";
	public static final String TEMPLATE_TEMPLATE_KEY = "inputTemplateTemplateName";
	public static final String JSON_KEY = "extractOcrDataJson";
	public static final String SEARCH_ID_KEY = "searchId";
	public static final String STATUS = "IN-PROGRESS";
	
	public static final String RULE_ID_CLIENT = "00001";
	public static final String RULE_ID_TEMPLATE = "00002";
	public static final String RULE_ID_GROUP_FILES = "00003";
	
	public static final String TEMPLATE_NOT_MATCH = "Tempalte didn't match";
	public static final String ERROR_CODE_TEMPLATE_NOT_MATCH = "TEMPLATE_NOT_MATCH";
	public static final String PROCESS_TYPE_TEMPLATE_IDENTIFICATION = "TEMPLATE_IDENTIFICATION";
	public static final int PROCESS_ID_TETEMPLATE_IDENTIFICATION = 333;
	
	
}
