package com.edatablock.rpa.service;

import com.edatablock.rpa.service.dto.RulesBookDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing RulesBook.
 */
public interface RulesBookService {

    /**
     * Save a rulesBook.
     *
     * @param rulesBookDTO the entity to save
     * @return the persisted entity
     */
    RulesBookDTO save(RulesBookDTO rulesBookDTO);

    /**
     * Get all the rulesBooks.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<RulesBookDTO> findAll(Pageable pageable);


    /**
     * Get the "id" rulesBook.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<RulesBookDTO> findOne(Long id);

    /**
     * Delete the "id" rulesBook.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
