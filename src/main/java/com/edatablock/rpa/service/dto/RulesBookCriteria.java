package com.edatablock.rpa.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the RulesBook entity. This class is used in RulesBookResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /rules-books?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class RulesBookCriteria implements Serializable {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter ruleNumber;

    private IntegerFilter ruleSequence;

    private IntegerFilter key;

    private StringFilter lookupPlace1;

    private StringFilter operator1;

    private StringFilter value1;

    private StringFilter joinField1;

    private StringFilter lookupPlace2;

    private StringFilter operator2;

    private StringFilter value2;

    private StringFilter joinField2;

    private StringFilter lookupPlace3;

    private StringFilter operator3;

    private StringFilter value3;

    private IntegerFilter result;

    private StringFilter description;

    public RulesBookCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getRuleNumber() {
        return ruleNumber;
    }

    public void setRuleNumber(StringFilter ruleNumber) {
        this.ruleNumber = ruleNumber;
    }

    public IntegerFilter getRuleSequence() {
        return ruleSequence;
    }

    public void setRuleSequence(IntegerFilter ruleSequence) {
        this.ruleSequence = ruleSequence;
    }

    public IntegerFilter getKey() {
        return key;
    }

    public void setKey(IntegerFilter key) {
        this.key = key;
    }

    public StringFilter getLookupPlace1() {
        return lookupPlace1;
    }

    public void setLookupPlace1(StringFilter lookupPlace1) {
        this.lookupPlace1 = lookupPlace1;
    }

    public StringFilter getOperator1() {
        return operator1;
    }

    public void setOperator1(StringFilter operator1) {
        this.operator1 = operator1;
    }

    public StringFilter getValue1() {
        return value1;
    }

    public void setValue1(StringFilter value1) {
        this.value1 = value1;
    }

    public StringFilter getJoinField1() {
        return joinField1;
    }

    public void setJoinField1(StringFilter joinField1) {
        this.joinField1 = joinField1;
    }

    public StringFilter getLookupPlace2() {
        return lookupPlace2;
    }

    public void setLookupPlace2(StringFilter lookupPlace2) {
        this.lookupPlace2 = lookupPlace2;
    }

    public StringFilter getOperator2() {
        return operator2;
    }

    public void setOperator2(StringFilter operator2) {
        this.operator2 = operator2;
    }

    public StringFilter getValue2() {
        return value2;
    }

    public void setValue2(StringFilter value2) {
        this.value2 = value2;
    }

    public StringFilter getJoinField2() {
        return joinField2;
    }

    public void setJoinField2(StringFilter joinField2) {
        this.joinField2 = joinField2;
    }

    public StringFilter getLookupPlace3() {
        return lookupPlace3;
    }

    public void setLookupPlace3(StringFilter lookupPlace3) {
        this.lookupPlace3 = lookupPlace3;
    }

    public StringFilter getOperator3() {
        return operator3;
    }

    public void setOperator3(StringFilter operator3) {
        this.operator3 = operator3;
    }

    public StringFilter getValue3() {
        return value3;
    }

    public void setValue3(StringFilter value3) {
        this.value3 = value3;
    }

    public IntegerFilter getResult() {
        return result;
    }

    public void setResult(IntegerFilter result) {
        this.result = result;
    }

    public StringFilter getDescription() {
        return description;
    }

    public void setDescription(StringFilter description) {
        this.description = description;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final RulesBookCriteria that = (RulesBookCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(ruleNumber, that.ruleNumber) &&
            Objects.equals(ruleSequence, that.ruleSequence) &&
            Objects.equals(key, that.key) &&
            Objects.equals(lookupPlace1, that.lookupPlace1) &&
            Objects.equals(operator1, that.operator1) &&
            Objects.equals(value1, that.value1) &&
            Objects.equals(joinField1, that.joinField1) &&
            Objects.equals(lookupPlace2, that.lookupPlace2) &&
            Objects.equals(operator2, that.operator2) &&
            Objects.equals(value2, that.value2) &&
            Objects.equals(joinField2, that.joinField2) &&
            Objects.equals(lookupPlace3, that.lookupPlace3) &&
            Objects.equals(operator3, that.operator3) &&
            Objects.equals(value3, that.value3) &&
            Objects.equals(result, that.result) &&
            Objects.equals(description, that.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        ruleNumber,
        ruleSequence,
        key,
        lookupPlace1,
        operator1,
        value1,
        joinField1,
        lookupPlace2,
        operator2,
        value2,
        joinField2,
        lookupPlace3,
        operator3,
        value3,
        result,
        description
        );
    }

    @Override
    public String toString() {
        return "RulesBookCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (ruleNumber != null ? "ruleNumber=" + ruleNumber + ", " : "") +
                (ruleSequence != null ? "ruleSequence=" + ruleSequence + ", " : "") +
                (key != null ? "key=" + key + ", " : "") +
                (lookupPlace1 != null ? "lookupPlace1=" + lookupPlace1 + ", " : "") +
                (operator1 != null ? "operator1=" + operator1 + ", " : "") +
                (value1 != null ? "value1=" + value1 + ", " : "") +
                (joinField1 != null ? "joinField1=" + joinField1 + ", " : "") +
                (lookupPlace2 != null ? "lookupPlace2=" + lookupPlace2 + ", " : "") +
                (operator2 != null ? "operator2=" + operator2 + ", " : "") +
                (value2 != null ? "value2=" + value2 + ", " : "") +
                (joinField2 != null ? "joinField2=" + joinField2 + ", " : "") +
                (lookupPlace3 != null ? "lookupPlace3=" + lookupPlace3 + ", " : "") +
                (operator3 != null ? "operator3=" + operator3 + ", " : "") +
                (value3 != null ? "value3=" + value3 + ", " : "") +
                (result != null ? "result=" + result + ", " : "") +
                (description != null ? "description=" + description + ", " : "") +
            "}";
    }

}
