package com.edatablock.rpa.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the RulesBook entity.
 */
public class RulesBookDTO implements Serializable {

    private Long id;

    private String ruleNumber;

    private Integer ruleSequence;

    private Integer key;

    private String lookupPlace1;

    private String operator1;

    private String value1;

    private String joinField1;

    private String lookupPlace2;

    private String operator2;

    private String value2;

    private String joinField2;

    private String lookupPlace3;

    private String operator3;

    private String value3;

    private Integer result;

    private String description;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRuleNumber() {
        return ruleNumber;
    }

    public void setRuleNumber(String ruleNumber) {
        this.ruleNumber = ruleNumber;
    }

    public Integer getRuleSequence() {
        return ruleSequence;
    }

    public void setRuleSequence(Integer ruleSequence) {
        this.ruleSequence = ruleSequence;
    }

    public Integer getKey() {
        return key;
    }

    public void setKey(Integer key) {
        this.key = key;
    }

    public String getLookupPlace1() {
        return lookupPlace1;
    }

    public void setLookupPlace1(String lookupPlace1) {
        this.lookupPlace1 = lookupPlace1;
    }

    public String getOperator1() {
        return operator1;
    }

    public void setOperator1(String operator1) {
        this.operator1 = operator1;
    }

    public String getValue1() {
        return value1;
    }

    public void setValue1(String value1) {
        this.value1 = value1;
    }

    public String getJoinField1() {
        return joinField1;
    }

    public void setJoinField1(String joinField1) {
        this.joinField1 = joinField1;
    }

    public String getLookupPlace2() {
        return lookupPlace2;
    }

    public void setLookupPlace2(String lookupPlace2) {
        this.lookupPlace2 = lookupPlace2;
    }

    public String getOperator2() {
        return operator2;
    }

    public void setOperator2(String operator2) {
        this.operator2 = operator2;
    }

    public String getValue2() {
        return value2;
    }

    public void setValue2(String value2) {
        this.value2 = value2;
    }

    public String getJoinField2() {
        return joinField2;
    }

    public void setJoinField2(String joinField2) {
        this.joinField2 = joinField2;
    }

    public String getLookupPlace3() {
        return lookupPlace3;
    }

    public void setLookupPlace3(String lookupPlace3) {
        this.lookupPlace3 = lookupPlace3;
    }

    public String getOperator3() {
        return operator3;
    }

    public void setOperator3(String operator3) {
        this.operator3 = operator3;
    }

    public String getValue3() {
        return value3;
    }

    public void setValue3(String value3) {
        this.value3 = value3;
    }

    public Integer getResult() {
        return result;
    }

    public void setResult(Integer result) {
        this.result = result;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        RulesBookDTO rulesBookDTO = (RulesBookDTO) o;
        if (rulesBookDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), rulesBookDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "RulesBookDTO{" +
            "id=" + getId() +
            ", ruleNumber='" + getRuleNumber() + "'" +
            ", ruleSequence=" + getRuleSequence() +
            ", key=" + getKey() +
            ", lookupPlace1='" + getLookupPlace1() + "'" +
            ", operator1='" + getOperator1() + "'" +
            ", value1='" + getValue1() + "'" +
            ", joinField1='" + getJoinField1() + "'" +
            ", lookupPlace2='" + getLookupPlace2() + "'" +
            ", operator2='" + getOperator2() + "'" +
            ", value2='" + getValue2() + "'" +
            ", joinField2='" + getJoinField2() + "'" +
            ", lookupPlace3='" + getLookupPlace3() + "'" +
            ", operator3='" + getOperator3() + "'" +
            ", value3='" + getValue3() + "'" +
            ", result=" + getResult() +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
