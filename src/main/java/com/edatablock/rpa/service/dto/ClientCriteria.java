package com.edatablock.rpa.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the Client entity. This class is used in ClientResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /clients?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ClientCriteria implements Serializable {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter clientName;

    private StringFilter description;

    private StringFilter clientAddress;

    private StringFilter clientEmailAddress;

    private IntegerFilter isActive;

    private InstantFilter createDate;

    private StringFilter createdBy;

    private InstantFilter updateDate;

    private IntegerFilter isMergedDocument;

    private StringFilter updatedBy;

    private LongFilter orgNameId;

    private LongFilter inputTemplateId;

    public ClientCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getClientName() {
        return clientName;
    }

    public void setClientName(StringFilter clientName) {
        this.clientName = clientName;
    }

    public StringFilter getDescription() {
        return description;
    }

    public void setDescription(StringFilter description) {
        this.description = description;
    }

    public StringFilter getClientAddress() {
        return clientAddress;
    }

    public void setClientAddress(StringFilter clientAddress) {
        this.clientAddress = clientAddress;
    }

    public StringFilter getClientEmailAddress() {
        return clientEmailAddress;
    }

    public void setClientEmailAddress(StringFilter clientEmailAddress) {
        this.clientEmailAddress = clientEmailAddress;
    }

    public IntegerFilter getIsActive() {
        return isActive;
    }

    public void setIsActive(IntegerFilter isActive) {
        this.isActive = isActive;
    }

    public InstantFilter getCreateDate() {
        return createDate;
    }

    public void setCreateDate(InstantFilter createDate) {
        this.createDate = createDate;
    }

    public StringFilter getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(StringFilter createdBy) {
        this.createdBy = createdBy;
    }

    public InstantFilter getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(InstantFilter updateDate) {
        this.updateDate = updateDate;
    }

    public IntegerFilter getIsMergedDocument() {
        return isMergedDocument;
    }

    public void setIsMergedDocument(IntegerFilter isMergedDocument) {
        this.isMergedDocument = isMergedDocument;
    }

    public StringFilter getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(StringFilter updatedBy) {
        this.updatedBy = updatedBy;
    }

    public LongFilter getOrgNameId() {
        return orgNameId;
    }

    public void setOrgNameId(LongFilter orgNameId) {
        this.orgNameId = orgNameId;
    }

    public LongFilter getInputTemplateId() {
        return inputTemplateId;
    }

    public void setInputTemplateId(LongFilter inputTemplateId) {
        this.inputTemplateId = inputTemplateId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ClientCriteria that = (ClientCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(clientName, that.clientName) &&
            Objects.equals(description, that.description) &&
            Objects.equals(clientAddress, that.clientAddress) &&
            Objects.equals(clientEmailAddress, that.clientEmailAddress) &&
            Objects.equals(isActive, that.isActive) &&
            Objects.equals(createDate, that.createDate) &&
            Objects.equals(createdBy, that.createdBy) &&
            Objects.equals(updateDate, that.updateDate) &&
            Objects.equals(isMergedDocument, that.isMergedDocument) &&
            Objects.equals(updatedBy, that.updatedBy) &&
            Objects.equals(orgNameId, that.orgNameId) &&
            Objects.equals(inputTemplateId, that.inputTemplateId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        clientName,
        description,
        clientAddress,
        clientEmailAddress,
        isActive,
        createDate,
        createdBy,
        updateDate,
        isMergedDocument,
        updatedBy,
        orgNameId,
        inputTemplateId
        );
    }

    @Override
    public String toString() {
        return "ClientCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (clientName != null ? "clientName=" + clientName + ", " : "") +
                (description != null ? "description=" + description + ", " : "") +
                (clientAddress != null ? "clientAddress=" + clientAddress + ", " : "") +
                (clientEmailAddress != null ? "clientEmailAddress=" + clientEmailAddress + ", " : "") +
                (isActive != null ? "isActive=" + isActive + ", " : "") +
                (createDate != null ? "createDate=" + createDate + ", " : "") +
                (createdBy != null ? "createdBy=" + createdBy + ", " : "") +
                (updateDate != null ? "updateDate=" + updateDate + ", " : "") +
                (isMergedDocument != null ? "isMergedDocument=" + isMergedDocument + ", " : "") +
                (updatedBy != null ? "updatedBy=" + updatedBy + ", " : "") +
                (orgNameId != null ? "orgNameId=" + orgNameId + ", " : "") +
                (inputTemplateId != null ? "inputTemplateId=" + inputTemplateId + ", " : "") +
            "}";
    }

}
