package com.edatablock.rpa.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the Organization entity. This class is used in OrganizationResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /organizations?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class OrganizationCriteria implements Serializable {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter orgName;

    private StringFilter description;

    private StringFilter orgAddress;

    private StringFilter orgEmail;

    private IntegerFilter isActive;

    private InstantFilter createDate;

    private StringFilter createdBy;

    private InstantFilter updateDate;

    private StringFilter updatedBy;

    public OrganizationCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getOrgName() {
        return orgName;
    }

    public void setOrgName(StringFilter orgName) {
        this.orgName = orgName;
    }

    public StringFilter getDescription() {
        return description;
    }

    public void setDescription(StringFilter description) {
        this.description = description;
    }

    public StringFilter getOrgAddress() {
        return orgAddress;
    }

    public void setOrgAddress(StringFilter orgAddress) {
        this.orgAddress = orgAddress;
    }

    public StringFilter getOrgEmail() {
        return orgEmail;
    }

    public void setOrgEmail(StringFilter orgEmail) {
        this.orgEmail = orgEmail;
    }

    public IntegerFilter getIsActive() {
        return isActive;
    }

    public void setIsActive(IntegerFilter isActive) {
        this.isActive = isActive;
    }

    public InstantFilter getCreateDate() {
        return createDate;
    }

    public void setCreateDate(InstantFilter createDate) {
        this.createDate = createDate;
    }

    public StringFilter getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(StringFilter createdBy) {
        this.createdBy = createdBy;
    }

    public InstantFilter getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(InstantFilter updateDate) {
        this.updateDate = updateDate;
    }

    public StringFilter getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(StringFilter updatedBy) {
        this.updatedBy = updatedBy;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final OrganizationCriteria that = (OrganizationCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(orgName, that.orgName) &&
            Objects.equals(description, that.description) &&
            Objects.equals(orgAddress, that.orgAddress) &&
            Objects.equals(orgEmail, that.orgEmail) &&
            Objects.equals(isActive, that.isActive) &&
            Objects.equals(createDate, that.createDate) &&
            Objects.equals(createdBy, that.createdBy) &&
            Objects.equals(updateDate, that.updateDate) &&
            Objects.equals(updatedBy, that.updatedBy);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        orgName,
        description,
        orgAddress,
        orgEmail,
        isActive,
        createDate,
        createdBy,
        updateDate,
        updatedBy
        );
    }

    @Override
    public String toString() {
        return "OrganizationCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (orgName != null ? "orgName=" + orgName + ", " : "") +
                (description != null ? "description=" + description + ", " : "") +
                (orgAddress != null ? "orgAddress=" + orgAddress + ", " : "") +
                (orgEmail != null ? "orgEmail=" + orgEmail + ", " : "") +
                (isActive != null ? "isActive=" + isActive + ", " : "") +
                (createDate != null ? "createDate=" + createDate + ", " : "") +
                (createdBy != null ? "createdBy=" + createdBy + ", " : "") +
                (updateDate != null ? "updateDate=" + updateDate + ", " : "") +
                (updatedBy != null ? "updatedBy=" + updatedBy + ", " : "") +
            "}";
    }

}
