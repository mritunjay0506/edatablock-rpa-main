package com.edatablock.rpa.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the EmailMessages entity. This class is used in EmailMessagesResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /email-messages?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class EmailMessagesCriteria implements Serializable {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter messageId;

    private StringFilter emailSubject;

    private StringFilter emailBody;

    private StringFilter status;

    private StringFilter clientEmailAddress;

    private StringFilter receiveFrom;

    private InstantFilter receivedTime;

    private IntegerFilter numberOfAttachments;

    private StringFilter attachments;

    private LongFilter emailAttachmentId;

    private LongFilter clientId;

    public EmailMessagesCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getMessageId() {
        return messageId;
    }

    public void setMessageId(StringFilter messageId) {
        this.messageId = messageId;
    }

    public StringFilter getEmailSubject() {
        return emailSubject;
    }

    public void setEmailSubject(StringFilter emailSubject) {
        this.emailSubject = emailSubject;
    }

    public StringFilter getEmailBody() {
        return emailBody;
    }

    public void setEmailBody(StringFilter emailBody) {
        this.emailBody = emailBody;
    }

    public StringFilter getStatus() {
        return status;
    }

    public void setStatus(StringFilter status) {
        this.status = status;
    }

    public StringFilter getClientEmailAddress() {
        return clientEmailAddress;
    }

    public void setClientEmailAddress(StringFilter clientEmailAddress) {
        this.clientEmailAddress = clientEmailAddress;
    }

    public StringFilter getReceiveFrom() {
        return receiveFrom;
    }

    public void setReceiveFrom(StringFilter receiveFrom) {
        this.receiveFrom = receiveFrom;
    }

    public InstantFilter getReceivedTime() {
        return receivedTime;
    }

    public void setReceivedTime(InstantFilter receivedTime) {
        this.receivedTime = receivedTime;
    }

    public IntegerFilter getNumberOfAttachments() {
        return numberOfAttachments;
    }

    public void setNumberOfAttachments(IntegerFilter numberOfAttachments) {
        this.numberOfAttachments = numberOfAttachments;
    }

    public StringFilter getAttachments() {
        return attachments;
    }

    public void setAttachments(StringFilter attachments) {
        this.attachments = attachments;
    }

    public LongFilter getEmailAttachmentId() {
        return emailAttachmentId;
    }

    public void setEmailAttachmentId(LongFilter emailAttachmentId) {
        this.emailAttachmentId = emailAttachmentId;
    }

    public LongFilter getClientId() {
        return clientId;
    }

    public void setClientId(LongFilter clientId) {
        this.clientId = clientId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final EmailMessagesCriteria that = (EmailMessagesCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(messageId, that.messageId) &&
            Objects.equals(emailSubject, that.emailSubject) &&
            Objects.equals(emailBody, that.emailBody) &&
            Objects.equals(status, that.status) &&
            Objects.equals(clientEmailAddress, that.clientEmailAddress) &&
            Objects.equals(receiveFrom, that.receiveFrom) &&
            Objects.equals(receivedTime, that.receivedTime) &&
            Objects.equals(numberOfAttachments, that.numberOfAttachments) &&
            Objects.equals(attachments, that.attachments) &&
            Objects.equals(emailAttachmentId, that.emailAttachmentId) &&
            Objects.equals(clientId, that.clientId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        messageId,
        emailSubject,
        emailBody,
        status,
        clientEmailAddress,
        receiveFrom,
        receivedTime,
        numberOfAttachments,
        attachments,
        emailAttachmentId,
        clientId
        );
    }

    @Override
    public String toString() {
        return "EmailMessagesCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (messageId != null ? "messageId=" + messageId + ", " : "") +
                (emailSubject != null ? "emailSubject=" + emailSubject + ", " : "") +
                (emailBody != null ? "emailBody=" + emailBody + ", " : "") +
                (status != null ? "status=" + status + ", " : "") +
                (clientEmailAddress != null ? "clientEmailAddress=" + clientEmailAddress + ", " : "") +
                (receiveFrom != null ? "receiveFrom=" + receiveFrom + ", " : "") +
                (receivedTime != null ? "receivedTime=" + receivedTime + ", " : "") +
                (numberOfAttachments != null ? "numberOfAttachments=" + numberOfAttachments + ", " : "") +
                (attachments != null ? "attachments=" + attachments + ", " : "") +
                (emailAttachmentId != null ? "emailAttachmentId=" + emailAttachmentId + ", " : "") +
                (clientId != null ? "clientId=" + clientId + ", " : "") +
            "}";
    }

}
