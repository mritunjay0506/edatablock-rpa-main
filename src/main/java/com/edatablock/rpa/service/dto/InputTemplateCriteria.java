package com.edatablock.rpa.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the InputTemplate entity. This class is used in InputTemplateResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /input-templates?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class InputTemplateCriteria implements Serializable {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter templateName;

    private StringFilter templateDescription;

    private StringFilter templateType;

    private BooleanFilter isStandardTemplate;

    private IntegerFilter isActive;

    private InstantFilter createDate;

    private StringFilter createdBy;

    private InstantFilter updateDate;

    private StringFilter templateIdentifier;

    private IntegerFilter numberOfPages;

    private StringFilter updatedBy;

    private LongFilter clientId;

    public InputTemplateCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getTemplateName() {
        return templateName;
    }

    public void setTemplateName(StringFilter templateName) {
        this.templateName = templateName;
    }

    public StringFilter getTemplateDescription() {
        return templateDescription;
    }

    public void setTemplateDescription(StringFilter templateDescription) {
        this.templateDescription = templateDescription;
    }

    public StringFilter getTemplateType() {
        return templateType;
    }

    public void setTemplateType(StringFilter templateType) {
        this.templateType = templateType;
    }

    public BooleanFilter getIsStandardTemplate() {
        return isStandardTemplate;
    }

    public void setIsStandardTemplate(BooleanFilter isStandardTemplate) {
        this.isStandardTemplate = isStandardTemplate;
    }

    public IntegerFilter getIsActive() {
        return isActive;
    }

    public void setIsActive(IntegerFilter isActive) {
        this.isActive = isActive;
    }

    public InstantFilter getCreateDate() {
        return createDate;
    }

    public void setCreateDate(InstantFilter createDate) {
        this.createDate = createDate;
    }

    public StringFilter getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(StringFilter createdBy) {
        this.createdBy = createdBy;
    }

    public InstantFilter getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(InstantFilter updateDate) {
        this.updateDate = updateDate;
    }

    public StringFilter getTemplateIdentifier() {
        return templateIdentifier;
    }

    public void setTemplateIdentifier(StringFilter templateIdentifier) {
        this.templateIdentifier = templateIdentifier;
    }

    public IntegerFilter getNumberOfPages() {
        return numberOfPages;
    }

    public void setNumberOfPages(IntegerFilter numberOfPages) {
        this.numberOfPages = numberOfPages;
    }

    public StringFilter getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(StringFilter updatedBy) {
        this.updatedBy = updatedBy;
    }

    public LongFilter getClientId() {
        return clientId;
    }

    public void setClientId(LongFilter clientId) {
        this.clientId = clientId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final InputTemplateCriteria that = (InputTemplateCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(templateName, that.templateName) &&
            Objects.equals(templateDescription, that.templateDescription) &&
            Objects.equals(templateType, that.templateType) &&
            Objects.equals(isStandardTemplate, that.isStandardTemplate) &&
            Objects.equals(isActive, that.isActive) &&
            Objects.equals(createDate, that.createDate) &&
            Objects.equals(createdBy, that.createdBy) &&
            Objects.equals(updateDate, that.updateDate) &&
            Objects.equals(templateIdentifier, that.templateIdentifier) &&
            Objects.equals(numberOfPages, that.numberOfPages) &&
            Objects.equals(updatedBy, that.updatedBy) &&
            Objects.equals(clientId, that.clientId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        templateName,
        templateDescription,
        templateType,
        isStandardTemplate,
        isActive,
        createDate,
        createdBy,
        updateDate,
        templateIdentifier,
        numberOfPages,
        updatedBy,
        clientId
        );
    }

    @Override
    public String toString() {
        return "InputTemplateCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (templateName != null ? "templateName=" + templateName + ", " : "") +
                (templateDescription != null ? "templateDescription=" + templateDescription + ", " : "") +
                (templateType != null ? "templateType=" + templateType + ", " : "") +
                (isStandardTemplate != null ? "isStandardTemplate=" + isStandardTemplate + ", " : "") +
                (isActive != null ? "isActive=" + isActive + ", " : "") +
                (createDate != null ? "createDate=" + createDate + ", " : "") +
                (createdBy != null ? "createdBy=" + createdBy + ", " : "") +
                (updateDate != null ? "updateDate=" + updateDate + ", " : "") +
                (templateIdentifier != null ? "templateIdentifier=" + templateIdentifier + ", " : "") +
                (numberOfPages != null ? "numberOfPages=" + numberOfPages + ", " : "") +
                (updatedBy != null ? "updatedBy=" + updatedBy + ", " : "") +
                (clientId != null ? "clientId=" + clientId + ", " : "") +
            "}";
    }

}
