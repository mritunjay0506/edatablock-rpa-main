package com.edatablock.rpa.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the FileToFtp entity. This class is used in FileToFtpResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /file-to-ftps?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class FileToFtpCriteria implements Serializable {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter messageId;

    private StringFilter clientEmailAddress;

    private StringFilter status;

    private StringFilter fileName;

    private StringFilter fileType;

    private IntegerFilter transactionId;

    private InstantFilter createDate;

    private LongFilter clientDataOcrId;

    public FileToFtpCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getMessageId() {
        return messageId;
    }

    public void setMessageId(StringFilter messageId) {
        this.messageId = messageId;
    }

    public StringFilter getClientEmailAddress() {
        return clientEmailAddress;
    }

    public void setClientEmailAddress(StringFilter clientEmailAddress) {
        this.clientEmailAddress = clientEmailAddress;
    }

    public StringFilter getStatus() {
        return status;
    }

    public void setStatus(StringFilter status) {
        this.status = status;
    }

    public StringFilter getFileName() {
        return fileName;
    }

    public void setFileName(StringFilter fileName) {
        this.fileName = fileName;
    }

    public StringFilter getFileType() {
        return fileType;
    }

    public void setFileType(StringFilter fileType) {
        this.fileType = fileType;
    }

    public IntegerFilter getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(IntegerFilter transactionId) {
        this.transactionId = transactionId;
    }

    public InstantFilter getCreateDate() {
        return createDate;
    }

    public void setCreateDate(InstantFilter createDate) {
        this.createDate = createDate;
    }

    public LongFilter getClientDataOcrId() {
        return clientDataOcrId;
    }

    public void setClientDataOcrId(LongFilter clientDataOcrId) {
        this.clientDataOcrId = clientDataOcrId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final FileToFtpCriteria that = (FileToFtpCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(messageId, that.messageId) &&
            Objects.equals(clientEmailAddress, that.clientEmailAddress) &&
            Objects.equals(status, that.status) &&
            Objects.equals(fileName, that.fileName) &&
            Objects.equals(fileType, that.fileType) &&
            Objects.equals(transactionId, that.transactionId) &&
            Objects.equals(createDate, that.createDate) &&
            Objects.equals(clientDataOcrId, that.clientDataOcrId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        messageId,
        clientEmailAddress,
        status,
        fileName,
        fileType,
        transactionId,
        createDate,
        clientDataOcrId
        );
    }

    @Override
    public String toString() {
        return "FileToFtpCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (messageId != null ? "messageId=" + messageId + ", " : "") +
                (clientEmailAddress != null ? "clientEmailAddress=" + clientEmailAddress + ", " : "") +
                (status != null ? "status=" + status + ", " : "") +
                (fileName != null ? "fileName=" + fileName + ", " : "") +
                (fileType != null ? "fileType=" + fileType + ", " : "") +
                (transactionId != null ? "transactionId=" + transactionId + ", " : "") +
                (createDate != null ? "createDate=" + createDate + ", " : "") +
                (clientDataOcrId != null ? "clientDataOcrId=" + clientDataOcrId + ", " : "") +
            "}";
    }

}
