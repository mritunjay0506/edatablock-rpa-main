package com.edatablock.rpa.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the TemplateFields entity. This class is used in TemplateFieldsResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /template-fields?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class TemplateFieldsCriteria implements Serializable {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter fieldName;

    private DoubleFilter fieldZoneMinX;

    private DoubleFilter fieldZoneMinY;

    private DoubleFilter fieldZoneMaxX;

    private DoubleFilter fieldZoneMaxY;

    private DoubleFilter width;

    private DoubleFilter height;

    private IntegerFilter sequence;

    private IntegerFilter isTemplateIdentifier;

    private IntegerFilter isLabel;

    private IntegerFilter pageNumebr;

    private IntegerFilter fieldValidationRequire;

    private StringFilter fieldValidationRule;

    private LongFilter inputTemplateId;

    public TemplateFieldsCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getFieldName() {
        return fieldName;
    }

    public void setFieldName(StringFilter fieldName) {
        this.fieldName = fieldName;
    }

    public DoubleFilter getFieldZoneMinX() {
        return fieldZoneMinX;
    }

    public void setFieldZoneMinX(DoubleFilter fieldZoneMinX) {
        this.fieldZoneMinX = fieldZoneMinX;
    }

    public DoubleFilter getFieldZoneMinY() {
        return fieldZoneMinY;
    }

    public void setFieldZoneMinY(DoubleFilter fieldZoneMinY) {
        this.fieldZoneMinY = fieldZoneMinY;
    }

    public DoubleFilter getFieldZoneMaxX() {
        return fieldZoneMaxX;
    }

    public void setFieldZoneMaxX(DoubleFilter fieldZoneMaxX) {
        this.fieldZoneMaxX = fieldZoneMaxX;
    }

    public DoubleFilter getFieldZoneMaxY() {
        return fieldZoneMaxY;
    }

    public void setFieldZoneMaxY(DoubleFilter fieldZoneMaxY) {
        this.fieldZoneMaxY = fieldZoneMaxY;
    }

    public DoubleFilter getWidth() {
        return width;
    }

    public void setWidth(DoubleFilter width) {
        this.width = width;
    }

    public DoubleFilter getHeight() {
        return height;
    }

    public void setHeight(DoubleFilter height) {
        this.height = height;
    }

    public IntegerFilter getSequence() {
        return sequence;
    }

    public void setSequence(IntegerFilter sequence) {
        this.sequence = sequence;
    }

    public IntegerFilter getIsTemplateIdentifier() {
        return isTemplateIdentifier;
    }

    public void setIsTemplateIdentifier(IntegerFilter isTemplateIdentifier) {
        this.isTemplateIdentifier = isTemplateIdentifier;
    }

    public IntegerFilter getIsLabel() {
        return isLabel;
    }

    public void setIsLabel(IntegerFilter isLabel) {
        this.isLabel = isLabel;
    }

    public IntegerFilter getPageNumebr() {
        return pageNumebr;
    }

    public void setPageNumebr(IntegerFilter pageNumebr) {
        this.pageNumebr = pageNumebr;
    }

    public IntegerFilter getFieldValidationRequire() {
        return fieldValidationRequire;
    }

    public void setFieldValidationRequire(IntegerFilter fieldValidationRequire) {
        this.fieldValidationRequire = fieldValidationRequire;
    }

    public StringFilter getFieldValidationRule() {
        return fieldValidationRule;
    }

    public void setFieldValidationRule(StringFilter fieldValidationRule) {
        this.fieldValidationRule = fieldValidationRule;
    }

    public LongFilter getInputTemplateId() {
        return inputTemplateId;
    }

    public void setInputTemplateId(LongFilter inputTemplateId) {
        this.inputTemplateId = inputTemplateId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final TemplateFieldsCriteria that = (TemplateFieldsCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(fieldName, that.fieldName) &&
            Objects.equals(fieldZoneMinX, that.fieldZoneMinX) &&
            Objects.equals(fieldZoneMinY, that.fieldZoneMinY) &&
            Objects.equals(fieldZoneMaxX, that.fieldZoneMaxX) &&
            Objects.equals(fieldZoneMaxY, that.fieldZoneMaxY) &&
            Objects.equals(width, that.width) &&
            Objects.equals(height, that.height) &&
            Objects.equals(sequence, that.sequence) &&
            Objects.equals(isTemplateIdentifier, that.isTemplateIdentifier) &&
            Objects.equals(isLabel, that.isLabel) &&
            Objects.equals(pageNumebr, that.pageNumebr) &&
            Objects.equals(fieldValidationRequire, that.fieldValidationRequire) &&
            Objects.equals(fieldValidationRule, that.fieldValidationRule) &&
            Objects.equals(inputTemplateId, that.inputTemplateId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        fieldName,
        fieldZoneMinX,
        fieldZoneMinY,
        fieldZoneMaxX,
        fieldZoneMaxY,
        width,
        height,
        sequence,
        isTemplateIdentifier,
        isLabel,
        pageNumebr,
        fieldValidationRequire,
        fieldValidationRule,
        inputTemplateId
        );
    }

    @Override
    public String toString() {
        return "TemplateFieldsCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (fieldName != null ? "fieldName=" + fieldName + ", " : "") +
                (fieldZoneMinX != null ? "fieldZoneMinX=" + fieldZoneMinX + ", " : "") +
                (fieldZoneMinY != null ? "fieldZoneMinY=" + fieldZoneMinY + ", " : "") +
                (fieldZoneMaxX != null ? "fieldZoneMaxX=" + fieldZoneMaxX + ", " : "") +
                (fieldZoneMaxY != null ? "fieldZoneMaxY=" + fieldZoneMaxY + ", " : "") +
                (width != null ? "width=" + width + ", " : "") +
                (height != null ? "height=" + height + ", " : "") +
                (sequence != null ? "sequence=" + sequence + ", " : "") +
                (isTemplateIdentifier != null ? "isTemplateIdentifier=" + isTemplateIdentifier + ", " : "") +
                (isLabel != null ? "isLabel=" + isLabel + ", " : "") +
                (pageNumebr != null ? "pageNumebr=" + pageNumebr + ", " : "") +
                (fieldValidationRequire != null ? "fieldValidationRequire=" + fieldValidationRequire + ", " : "") +
                (fieldValidationRule != null ? "fieldValidationRule=" + fieldValidationRule + ", " : "") +
                (inputTemplateId != null ? "inputTemplateId=" + inputTemplateId + ", " : "") +
            "}";
    }

}
