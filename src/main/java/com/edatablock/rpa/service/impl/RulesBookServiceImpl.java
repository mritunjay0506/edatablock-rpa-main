package com.edatablock.rpa.service.impl;

import com.edatablock.rpa.service.RulesBookService;
import com.edatablock.rpa.domain.RulesBook;
import com.edatablock.rpa.repository.RulesBookRepository;
import com.edatablock.rpa.service.dto.RulesBookDTO;
import com.edatablock.rpa.service.mapper.RulesBookMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing RulesBook.
 */
@Service
@Transactional
public class RulesBookServiceImpl implements RulesBookService {

    private final Logger log = LoggerFactory.getLogger(RulesBookServiceImpl.class);

    private final RulesBookRepository rulesBookRepository;

    private final RulesBookMapper rulesBookMapper;

    public RulesBookServiceImpl(RulesBookRepository rulesBookRepository, RulesBookMapper rulesBookMapper) {
        this.rulesBookRepository = rulesBookRepository;
        this.rulesBookMapper = rulesBookMapper;
    }

    /**
     * Save a rulesBook.
     *
     * @param rulesBookDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public RulesBookDTO save(RulesBookDTO rulesBookDTO) {
        log.debug("Request to save RulesBook : {}", rulesBookDTO);
        RulesBook rulesBook = rulesBookMapper.toEntity(rulesBookDTO);
        rulesBook = rulesBookRepository.save(rulesBook);
        return rulesBookMapper.toDto(rulesBook);
    }

    /**
     * Get all the rulesBooks.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<RulesBookDTO> findAll(Pageable pageable) {
        log.debug("Request to get all RulesBooks");
        return rulesBookRepository.findAll(pageable)
            .map(rulesBookMapper::toDto);
    }


    /**
     * Get one rulesBook by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<RulesBookDTO> findOne(Long id) {
        log.debug("Request to get RulesBook : {}", id);
        return rulesBookRepository.findById(id)
            .map(rulesBookMapper::toDto);
    }

    /**
     * Delete the rulesBook by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete RulesBook : {}", id);
        rulesBookRepository.deleteById(id);
    }
}
