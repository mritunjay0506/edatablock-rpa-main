package com.edatablock.rpa.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.edatablock.rpa.domain.EmailMessages;
import com.edatablock.rpa.domain.*; // for static metamodels
import com.edatablock.rpa.repository.EmailMessagesRepository;
import com.edatablock.rpa.service.dto.EmailMessagesCriteria;
import com.edatablock.rpa.service.dto.EmailMessagesDTO;
import com.edatablock.rpa.service.mapper.EmailMessagesMapper;

/**
 * Service for executing complex queries for EmailMessages entities in the database.
 * The main input is a {@link EmailMessagesCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link EmailMessagesDTO} or a {@link Page} of {@link EmailMessagesDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class EmailMessagesQueryService extends QueryService<EmailMessages> {

    private final Logger log = LoggerFactory.getLogger(EmailMessagesQueryService.class);

    private final EmailMessagesRepository emailMessagesRepository;

    private final EmailMessagesMapper emailMessagesMapper;

    public EmailMessagesQueryService(EmailMessagesRepository emailMessagesRepository, EmailMessagesMapper emailMessagesMapper) {
        this.emailMessagesRepository = emailMessagesRepository;
        this.emailMessagesMapper = emailMessagesMapper;
    }

    /**
     * Return a {@link List} of {@link EmailMessagesDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<EmailMessagesDTO> findByCriteria(EmailMessagesCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<EmailMessages> specification = createSpecification(criteria);
        return emailMessagesMapper.toDto(emailMessagesRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link EmailMessagesDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<EmailMessagesDTO> findByCriteria(EmailMessagesCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<EmailMessages> specification = createSpecification(criteria);
        return emailMessagesRepository.findAll(specification, page)
            .map(emailMessagesMapper::toDto);
    }

    /**
     * Function to convert EmailMessagesCriteria to a {@link Specification}
     */
    private Specification<EmailMessages> createSpecification(EmailMessagesCriteria criteria) {
        Specification<EmailMessages> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), EmailMessages_.id));
            }
            if (criteria.getMessageId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMessageId(), EmailMessages_.messageId));
            }
            if (criteria.getEmailSubject() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEmailSubject(), EmailMessages_.emailSubject));
            }
            if (criteria.getEmailBody() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEmailBody(), EmailMessages_.emailBody));
            }
            if (criteria.getStatus() != null) {
                specification = specification.and(buildStringSpecification(criteria.getStatus(), EmailMessages_.status));
            }
            if (criteria.getClientEmailAddress() != null) {
                specification = specification.and(buildStringSpecification(criteria.getClientEmailAddress(), EmailMessages_.clientEmailAddress));
            }
            if (criteria.getReceiveFrom() != null) {
                specification = specification.and(buildStringSpecification(criteria.getReceiveFrom(), EmailMessages_.receiveFrom));
            }
            if (criteria.getReceivedTime() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getReceivedTime(), EmailMessages_.receivedTime));
            }
            if (criteria.getNumberOfAttachments() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNumberOfAttachments(), EmailMessages_.numberOfAttachments));
            }
            if (criteria.getAttachments() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAttachments(), EmailMessages_.attachments));
            }
            if (criteria.getEmailAttachmentId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getEmailAttachmentId(), EmailMessages_.emailAttachments, EmailAttachment_.id));
            }
            if (criteria.getClientId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getClientId(), EmailMessages_.client, Client_.id));
            }
        }
        return specification;
    }
}
