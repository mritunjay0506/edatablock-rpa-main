package com.edatablock.rpa.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.edatablock.rpa.domain.TemplateFields;
import com.edatablock.rpa.domain.*; // for static metamodels
import com.edatablock.rpa.repository.TemplateFieldsRepository;
import com.edatablock.rpa.service.dto.TemplateFieldsCriteria;
import com.edatablock.rpa.service.dto.TemplateFieldsDTO;
import com.edatablock.rpa.service.mapper.TemplateFieldsMapper;

/**
 * Service for executing complex queries for TemplateFields entities in the database.
 * The main input is a {@link TemplateFieldsCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link TemplateFieldsDTO} or a {@link Page} of {@link TemplateFieldsDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class TemplateFieldsQueryService extends QueryService<TemplateFields> {

    private final Logger log = LoggerFactory.getLogger(TemplateFieldsQueryService.class);

    private final TemplateFieldsRepository templateFieldsRepository;

    private final TemplateFieldsMapper templateFieldsMapper;

    public TemplateFieldsQueryService(TemplateFieldsRepository templateFieldsRepository, TemplateFieldsMapper templateFieldsMapper) {
        this.templateFieldsRepository = templateFieldsRepository;
        this.templateFieldsMapper = templateFieldsMapper;
    }

    /**
     * Return a {@link List} of {@link TemplateFieldsDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<TemplateFieldsDTO> findByCriteria(TemplateFieldsCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<TemplateFields> specification = createSpecification(criteria);
        return templateFieldsMapper.toDto(templateFieldsRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link TemplateFieldsDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<TemplateFieldsDTO> findByCriteria(TemplateFieldsCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<TemplateFields> specification = createSpecification(criteria);
        return templateFieldsRepository.findAll(specification, page)
            .map(templateFieldsMapper::toDto);
    }

    /**
     * Function to convert TemplateFieldsCriteria to a {@link Specification}
     */
    private Specification<TemplateFields> createSpecification(TemplateFieldsCriteria criteria) {
        Specification<TemplateFields> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), TemplateFields_.id));
            }
            if (criteria.getFieldName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFieldName(), TemplateFields_.fieldName));
            }
            if (criteria.getFieldZoneMinX() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getFieldZoneMinX(), TemplateFields_.fieldZoneMinX));
            }
            if (criteria.getFieldZoneMinY() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getFieldZoneMinY(), TemplateFields_.fieldZoneMinY));
            }
            if (criteria.getFieldZoneMaxX() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getFieldZoneMaxX(), TemplateFields_.fieldZoneMaxX));
            }
            if (criteria.getFieldZoneMaxY() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getFieldZoneMaxY(), TemplateFields_.fieldZoneMaxY));
            }
            if (criteria.getWidth() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getWidth(), TemplateFields_.width));
            }
            if (criteria.getHeight() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getHeight(), TemplateFields_.height));
            }
            if (criteria.getSequence() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSequence(), TemplateFields_.sequence));
            }
            if (criteria.getIsTemplateIdentifier() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIsTemplateIdentifier(), TemplateFields_.isTemplateIdentifier));
            }
            if (criteria.getIsLabel() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIsLabel(), TemplateFields_.isLabel));
            }
            if (criteria.getPageNumebr() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getPageNumebr(), TemplateFields_.pageNumebr));
            }
            if (criteria.getFieldValidationRequire() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getFieldValidationRequire(), TemplateFields_.fieldValidationRequire));
            }
            if (criteria.getFieldValidationRule() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFieldValidationRule(), TemplateFields_.fieldValidationRule));
            }
            if (criteria.getInputTemplateId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getInputTemplateId(), TemplateFields_.inputTemplate, InputTemplate_.id));
            }
        }
        return specification;
    }
}
