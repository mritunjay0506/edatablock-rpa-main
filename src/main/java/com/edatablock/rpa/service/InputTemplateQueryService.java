package com.edatablock.rpa.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.edatablock.rpa.domain.InputTemplate;
import com.edatablock.rpa.domain.*; // for static metamodels
import com.edatablock.rpa.repository.InputTemplateRepository;
import com.edatablock.rpa.service.dto.InputTemplateCriteria;
import com.edatablock.rpa.service.dto.InputTemplateDTO;
import com.edatablock.rpa.service.mapper.InputTemplateMapper;

/**
 * Service for executing complex queries for InputTemplate entities in the database.
 * The main input is a {@link InputTemplateCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link InputTemplateDTO} or a {@link Page} of {@link InputTemplateDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class InputTemplateQueryService extends QueryService<InputTemplate> {

    private final Logger log = LoggerFactory.getLogger(InputTemplateQueryService.class);

    private final InputTemplateRepository inputTemplateRepository;

    private final InputTemplateMapper inputTemplateMapper;

    public InputTemplateQueryService(InputTemplateRepository inputTemplateRepository, InputTemplateMapper inputTemplateMapper) {
        this.inputTemplateRepository = inputTemplateRepository;
        this.inputTemplateMapper = inputTemplateMapper;
    }

    /**
     * Return a {@link List} of {@link InputTemplateDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<InputTemplateDTO> findByCriteria(InputTemplateCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<InputTemplate> specification = createSpecification(criteria);
        return inputTemplateMapper.toDto(inputTemplateRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link InputTemplateDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<InputTemplateDTO> findByCriteria(InputTemplateCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<InputTemplate> specification = createSpecification(criteria);
        return inputTemplateRepository.findAll(specification, page)
            .map(inputTemplateMapper::toDto);
    }

    /**
     * Function to convert InputTemplateCriteria to a {@link Specification}
     */
    private Specification<InputTemplate> createSpecification(InputTemplateCriteria criteria) {
        Specification<InputTemplate> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), InputTemplate_.id));
            }
            if (criteria.getTemplateName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTemplateName(), InputTemplate_.templateName));
            }
            if (criteria.getTemplateDescription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTemplateDescription(), InputTemplate_.templateDescription));
            }
            if (criteria.getTemplateType() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTemplateType(), InputTemplate_.templateType));
            }
            if (criteria.getIsStandardTemplate() != null) {
                specification = specification.and(buildSpecification(criteria.getIsStandardTemplate(), InputTemplate_.isStandardTemplate));
            }
            if (criteria.getIsActive() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIsActive(), InputTemplate_.isActive));
            }
            if (criteria.getCreateDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreateDate(), InputTemplate_.createDate));
            }
            if (criteria.getCreatedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCreatedBy(), InputTemplate_.createdBy));
            }
            if (criteria.getUpdateDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUpdateDate(), InputTemplate_.updateDate));
            }
            if (criteria.getTemplateIdentifier() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTemplateIdentifier(), InputTemplate_.templateIdentifier));
            }
            if (criteria.getNumberOfPages() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNumberOfPages(), InputTemplate_.numberOfPages));
            }
            if (criteria.getUpdatedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getUpdatedBy(), InputTemplate_.updatedBy));
            }
            if (criteria.getClientId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getClientId(), InputTemplate_.client, Client_.id));
            }
        }
        return specification;
    }
}
