package com.edatablock.rpa.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.edatablock.rpa.domain.RulesBook;
import com.edatablock.rpa.domain.*; // for static metamodels
import com.edatablock.rpa.repository.RulesBookRepository;
import com.edatablock.rpa.service.dto.RulesBookCriteria;
import com.edatablock.rpa.service.dto.RulesBookDTO;
import com.edatablock.rpa.service.mapper.RulesBookMapper;

/**
 * Service for executing complex queries for RulesBook entities in the database.
 * The main input is a {@link RulesBookCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link RulesBookDTO} or a {@link Page} of {@link RulesBookDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class RulesBookQueryService extends QueryService<RulesBook> {

    private final Logger log = LoggerFactory.getLogger(RulesBookQueryService.class);

    private final RulesBookRepository rulesBookRepository;

    private final RulesBookMapper rulesBookMapper;

    public RulesBookQueryService(RulesBookRepository rulesBookRepository, RulesBookMapper rulesBookMapper) {
        this.rulesBookRepository = rulesBookRepository;
        this.rulesBookMapper = rulesBookMapper;
    }

    /**
     * Return a {@link List} of {@link RulesBookDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<RulesBookDTO> findByCriteria(RulesBookCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<RulesBook> specification = createSpecification(criteria);
        return rulesBookMapper.toDto(rulesBookRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link RulesBookDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<RulesBookDTO> findByCriteria(RulesBookCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<RulesBook> specification = createSpecification(criteria);
        return rulesBookRepository.findAll(specification, page)
            .map(rulesBookMapper::toDto);
    }

    /**
     * Function to convert RulesBookCriteria to a {@link Specification}
     */
    private Specification<RulesBook> createSpecification(RulesBookCriteria criteria) {
        Specification<RulesBook> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), RulesBook_.id));
            }
            if (criteria.getRuleNumber() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRuleNumber(), RulesBook_.ruleNumber));
            }
            if (criteria.getRuleSequence() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getRuleSequence(), RulesBook_.ruleSequence));
            }
            if (criteria.getKey() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getKey(), RulesBook_.key));
            }
            if (criteria.getLookupPlace1() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLookupPlace1(), RulesBook_.lookupPlace1));
            }
            if (criteria.getOperator1() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOperator1(), RulesBook_.operator1));
            }
            if (criteria.getValue1() != null) {
                specification = specification.and(buildStringSpecification(criteria.getValue1(), RulesBook_.value1));
            }
            if (criteria.getJoinField1() != null) {
                specification = specification.and(buildStringSpecification(criteria.getJoinField1(), RulesBook_.joinField1));
            }
            if (criteria.getLookupPlace2() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLookupPlace2(), RulesBook_.lookupPlace2));
            }
            if (criteria.getOperator2() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOperator2(), RulesBook_.operator2));
            }
            if (criteria.getValue2() != null) {
                specification = specification.and(buildStringSpecification(criteria.getValue2(), RulesBook_.value2));
            }
            if (criteria.getJoinField2() != null) {
                specification = specification.and(buildStringSpecification(criteria.getJoinField2(), RulesBook_.joinField2));
            }
            if (criteria.getLookupPlace3() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLookupPlace3(), RulesBook_.lookupPlace3));
            }
            if (criteria.getOperator3() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOperator3(), RulesBook_.operator3));
            }
            if (criteria.getValue3() != null) {
                specification = specification.and(buildStringSpecification(criteria.getValue3(), RulesBook_.value3));
            }
            if (criteria.getResult() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getResult(), RulesBook_.result));
            }
            if (criteria.getDescription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescription(), RulesBook_.description));
            }
        }
        return specification;
    }
}
