package com.edatablock.rpa.service.mapper;

import com.edatablock.rpa.domain.*;
import com.edatablock.rpa.service.dto.ClientDataOcrDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity ClientDataOcr and its DTO ClientDataOcrDTO.
 */
@Mapper(componentModel = "spring", uses = {TransactionMapper.class, InputTemplateMapper.class})
public interface ClientDataOcrMapper extends EntityMapper<ClientDataOcrDTO, ClientDataOcr> {

    @Mapping(source = "transaction.id", target = "transactionId")
    @Mapping(source = "inputTemplate.id", target = "inputTemplateId")
    @Mapping(source = "inputTemplate.templateName", target = "inputTemplateTemplateName")
    ClientDataOcrDTO toDto(ClientDataOcr clientDataOcr);

    @Mapping(source = "transactionId", target = "transaction")
    @Mapping(source = "inputTemplateId", target = "inputTemplate")
    ClientDataOcr toEntity(ClientDataOcrDTO clientDataOcrDTO);

    default ClientDataOcr fromId(Long id) {
        if (id == null) {
            return null;
        }
        ClientDataOcr clientDataOcr = new ClientDataOcr();
        clientDataOcr.setId(id);
        return clientDataOcr;
    }
}
