package com.edatablock.rpa.service.mapper;

import com.edatablock.rpa.domain.*;
import com.edatablock.rpa.service.dto.RulesBookDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity RulesBook and its DTO RulesBookDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface RulesBookMapper extends EntityMapper<RulesBookDTO, RulesBook> {



    default RulesBook fromId(Long id) {
        if (id == null) {
            return null;
        }
        RulesBook rulesBook = new RulesBook();
        rulesBook.setId(id);
        return rulesBook;
    }
}
