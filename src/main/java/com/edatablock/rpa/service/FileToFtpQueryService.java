package com.edatablock.rpa.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.edatablock.rpa.domain.FileToFtp;
import com.edatablock.rpa.domain.*; // for static metamodels
import com.edatablock.rpa.repository.FileToFtpRepository;
import com.edatablock.rpa.service.dto.FileToFtpCriteria;
import com.edatablock.rpa.service.dto.FileToFtpDTO;
import com.edatablock.rpa.service.mapper.FileToFtpMapper;

/**
 * Service for executing complex queries for FileToFtp entities in the database.
 * The main input is a {@link FileToFtpCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link FileToFtpDTO} or a {@link Page} of {@link FileToFtpDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class FileToFtpQueryService extends QueryService<FileToFtp> {

    private final Logger log = LoggerFactory.getLogger(FileToFtpQueryService.class);

    private final FileToFtpRepository fileToFtpRepository;

    private final FileToFtpMapper fileToFtpMapper;

    public FileToFtpQueryService(FileToFtpRepository fileToFtpRepository, FileToFtpMapper fileToFtpMapper) {
        this.fileToFtpRepository = fileToFtpRepository;
        this.fileToFtpMapper = fileToFtpMapper;
    }

    /**
     * Return a {@link List} of {@link FileToFtpDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<FileToFtpDTO> findByCriteria(FileToFtpCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<FileToFtp> specification = createSpecification(criteria);
        return fileToFtpMapper.toDto(fileToFtpRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link FileToFtpDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<FileToFtpDTO> findByCriteria(FileToFtpCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<FileToFtp> specification = createSpecification(criteria);
        return fileToFtpRepository.findAll(specification, page)
            .map(fileToFtpMapper::toDto);
    }

    /**
     * Function to convert FileToFtpCriteria to a {@link Specification}
     */
    private Specification<FileToFtp> createSpecification(FileToFtpCriteria criteria) {
        Specification<FileToFtp> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), FileToFtp_.id));
            }
            if (criteria.getMessageId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMessageId(), FileToFtp_.messageId));
            }
            if (criteria.getClientEmailAddress() != null) {
                specification = specification.and(buildStringSpecification(criteria.getClientEmailAddress(), FileToFtp_.clientEmailAddress));
            }
            if (criteria.getStatus() != null) {
                specification = specification.and(buildStringSpecification(criteria.getStatus(), FileToFtp_.status));
            }
            if (criteria.getFileName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFileName(), FileToFtp_.fileName));
            }
            if (criteria.getFileType() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFileType(), FileToFtp_.fileType));
            }
            if (criteria.getTransactionId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTransactionId(), FileToFtp_.transactionId));
            }
            if (criteria.getCreateDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreateDate(), FileToFtp_.createDate));
            }
            if (criteria.getClientDataOcrId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getClientDataOcrId(), FileToFtp_.clientDataOcr, ClientDataOcr_.id));
            }
        }
        return specification;
    }
}
