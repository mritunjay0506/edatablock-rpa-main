package com.edatablock.rpa.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.edatablock.rpa.domain.Client;
import com.edatablock.rpa.domain.*; // for static metamodels
import com.edatablock.rpa.repository.ClientRepository;
import com.edatablock.rpa.service.dto.ClientCriteria;
import com.edatablock.rpa.service.dto.ClientDTO;
import com.edatablock.rpa.service.mapper.ClientMapper;

/**
 * Service for executing complex queries for Client entities in the database.
 * The main input is a {@link ClientCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ClientDTO} or a {@link Page} of {@link ClientDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ClientQueryService extends QueryService<Client> {

    private final Logger log = LoggerFactory.getLogger(ClientQueryService.class);

    private final ClientRepository clientRepository;

    private final ClientMapper clientMapper;

    public ClientQueryService(ClientRepository clientRepository, ClientMapper clientMapper) {
        this.clientRepository = clientRepository;
        this.clientMapper = clientMapper;
    }

    /**
     * Return a {@link List} of {@link ClientDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ClientDTO> findByCriteria(ClientCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Client> specification = createSpecification(criteria);
        return clientMapper.toDto(clientRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ClientDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ClientDTO> findByCriteria(ClientCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Client> specification = createSpecification(criteria);
        return clientRepository.findAll(specification, page)
            .map(clientMapper::toDto);
    }

    /**
     * Function to convert ClientCriteria to a {@link Specification}
     */
    private Specification<Client> createSpecification(ClientCriteria criteria) {
        Specification<Client> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Client_.id));
            }
            if (criteria.getClientName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getClientName(), Client_.clientName));
            }
            if (criteria.getDescription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescription(), Client_.description));
            }
            if (criteria.getClientAddress() != null) {
                specification = specification.and(buildStringSpecification(criteria.getClientAddress(), Client_.clientAddress));
            }
            if (criteria.getClientEmailAddress() != null) {
                specification = specification.and(buildStringSpecification(criteria.getClientEmailAddress(), Client_.clientEmailAddress));
            }
            if (criteria.getIsActive() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIsActive(), Client_.isActive));
            }
            if (criteria.getCreateDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreateDate(), Client_.createDate));
            }
            if (criteria.getCreatedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCreatedBy(), Client_.createdBy));
            }
            if (criteria.getUpdateDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUpdateDate(), Client_.updateDate));
            }
            if (criteria.getIsMergedDocument() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIsMergedDocument(), Client_.isMergedDocument));
            }
            if (criteria.getUpdatedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getUpdatedBy(), Client_.updatedBy));
            }
            if (criteria.getOrgNameId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getOrgNameId(), Client_.orgName, Organization_.id));
            }
            if (criteria.getInputTemplateId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getInputTemplateId(), Client_.inputTemplates, InputTemplate_.id));
            }
        }
        return specification;
    }
}
