package com.edatablock.rpa.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A InputTemplate.
 */
@Entity
@Table(name = "input_template")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class InputTemplate implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "template_name", nullable = false)
    private String templateName;

    @Column(name = "template_description")
    private String templateDescription;

    @NotNull
    @Column(name = "template_type", nullable = false)
    private String templateType;

    @NotNull
    @Column(name = "is_standard_template", nullable = false)
    private Boolean isStandardTemplate;

    @Column(name = "is_active")
    private Integer isActive;

    @Column(name = "create_date")
    private Instant createDate;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "update_date")
    private Instant updateDate;

    @Column(name = "template_identifier")
    private String templateIdentifier;

    @Column(name = "number_of_pages")
    private Integer numberOfPages;

    @Column(name = "updated_by")
    private String updatedBy;

    @ManyToOne
    @JsonIgnoreProperties("inputTemplates")
    private Client client;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTemplateName() {
        return templateName;
    }

    public InputTemplate templateName(String templateName) {
        this.templateName = templateName;
        return this;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public String getTemplateDescription() {
        return templateDescription;
    }

    public InputTemplate templateDescription(String templateDescription) {
        this.templateDescription = templateDescription;
        return this;
    }

    public void setTemplateDescription(String templateDescription) {
        this.templateDescription = templateDescription;
    }

    public String getTemplateType() {
        return templateType;
    }

    public InputTemplate templateType(String templateType) {
        this.templateType = templateType;
        return this;
    }

    public void setTemplateType(String templateType) {
        this.templateType = templateType;
    }

    public Boolean isIsStandardTemplate() {
        return isStandardTemplate;
    }

    public InputTemplate isStandardTemplate(Boolean isStandardTemplate) {
        this.isStandardTemplate = isStandardTemplate;
        return this;
    }

    public void setIsStandardTemplate(Boolean isStandardTemplate) {
        this.isStandardTemplate = isStandardTemplate;
    }

    public Integer getIsActive() {
        return isActive;
    }

    public InputTemplate isActive(Integer isActive) {
        this.isActive = isActive;
        return this;
    }

    public void setIsActive(Integer isActive) {
        this.isActive = isActive;
    }

    public Instant getCreateDate() {
        return createDate;
    }

    public InputTemplate createDate(Instant createDate) {
        this.createDate = createDate;
        return this;
    }

    public void setCreateDate(Instant createDate) {
        this.createDate = createDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public InputTemplate createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getUpdateDate() {
        return updateDate;
    }

    public InputTemplate updateDate(Instant updateDate) {
        this.updateDate = updateDate;
        return this;
    }

    public void setUpdateDate(Instant updateDate) {
        this.updateDate = updateDate;
    }

    public String getTemplateIdentifier() {
        return templateIdentifier;
    }

    public InputTemplate templateIdentifier(String templateIdentifier) {
        this.templateIdentifier = templateIdentifier;
        return this;
    }

    public void setTemplateIdentifier(String templateIdentifier) {
        this.templateIdentifier = templateIdentifier;
    }

    public Integer getNumberOfPages() {
        return numberOfPages;
    }

    public InputTemplate numberOfPages(Integer numberOfPages) {
        this.numberOfPages = numberOfPages;
        return this;
    }

    public void setNumberOfPages(Integer numberOfPages) {
        this.numberOfPages = numberOfPages;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public InputTemplate updatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
        return this;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Client getClient() {
        return client;
    }

    public InputTemplate client(Client client) {
        this.client = client;
        return this;
    }

    public void setClient(Client client) {
        this.client = client;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        InputTemplate inputTemplate = (InputTemplate) o;
        if (inputTemplate.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), inputTemplate.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "InputTemplate{" +
            "id=" + getId() +
            ", templateName='" + getTemplateName() + "'" +
            ", templateDescription='" + getTemplateDescription() + "'" +
            ", templateType='" + getTemplateType() + "'" +
            ", isStandardTemplate='" + isIsStandardTemplate() + "'" +
            ", isActive=" + getIsActive() +
            ", createDate='" + getCreateDate() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", updateDate='" + getUpdateDate() + "'" +
            ", templateIdentifier='" + getTemplateIdentifier() + "'" +
            ", numberOfPages=" + getNumberOfPages() +
            ", updatedBy='" + getUpdatedBy() + "'" +
            "}";
    }
}
