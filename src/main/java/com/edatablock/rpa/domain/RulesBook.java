package com.edatablock.rpa.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A RulesBook.
 */
@Entity
@Table(name = "rules_book")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class RulesBook implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "rule_number")
    private String ruleNumber;

    @Column(name = "rule_sequence")
    private Integer ruleSequence;

    @Column(name = "jhi_key")
    private Integer key;

    @Column(name = "lookup_place_1")
    private String lookupPlace1;

    @Column(name = "operator_1")
    private String operator1;

    @Column(name = "value_1")
    private String value1;

    @Column(name = "join_field_1")
    private String joinField1;

    @Column(name = "lookup_place_2")
    private String lookupPlace2;

    @Column(name = "operator_2")
    private String operator2;

    @Column(name = "value_2")
    private String value2;

    @Column(name = "join_field_2")
    private String joinField2;

    @Column(name = "lookup_place_3")
    private String lookupPlace3;

    @Column(name = "operator_3")
    private String operator3;

    @Column(name = "value_3")
    private String value3;

    @Column(name = "result")
    private Integer result;

    @Column(name = "description")
    private String description;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRuleNumber() {
        return ruleNumber;
    }

    public RulesBook ruleNumber(String ruleNumber) {
        this.ruleNumber = ruleNumber;
        return this;
    }

    public void setRuleNumber(String ruleNumber) {
        this.ruleNumber = ruleNumber;
    }

    public Integer getRuleSequence() {
        return ruleSequence;
    }

    public RulesBook ruleSequence(Integer ruleSequence) {
        this.ruleSequence = ruleSequence;
        return this;
    }

    public void setRuleSequence(Integer ruleSequence) {
        this.ruleSequence = ruleSequence;
    }

    public Integer getKey() {
        return key;
    }

    public RulesBook key(Integer key) {
        this.key = key;
        return this;
    }

    public void setKey(Integer key) {
        this.key = key;
    }

    public String getLookupPlace1() {
        return lookupPlace1;
    }

    public RulesBook lookupPlace1(String lookupPlace1) {
        this.lookupPlace1 = lookupPlace1;
        return this;
    }

    public void setLookupPlace1(String lookupPlace1) {
        this.lookupPlace1 = lookupPlace1;
    }

    public String getOperator1() {
        return operator1;
    }

    public RulesBook operator1(String operator1) {
        this.operator1 = operator1;
        return this;
    }

    public void setOperator1(String operator1) {
        this.operator1 = operator1;
    }

    public String getValue1() {
        return value1;
    }

    public RulesBook value1(String value1) {
        this.value1 = value1;
        return this;
    }

    public void setValue1(String value1) {
        this.value1 = value1;
    }

    public String getJoinField1() {
        return joinField1;
    }

    public RulesBook joinField1(String joinField1) {
        this.joinField1 = joinField1;
        return this;
    }

    public void setJoinField1(String joinField1) {
        this.joinField1 = joinField1;
    }

    public String getLookupPlace2() {
        return lookupPlace2;
    }

    public RulesBook lookupPlace2(String lookupPlace2) {
        this.lookupPlace2 = lookupPlace2;
        return this;
    }

    public void setLookupPlace2(String lookupPlace2) {
        this.lookupPlace2 = lookupPlace2;
    }

    public String getOperator2() {
        return operator2;
    }

    public RulesBook operator2(String operator2) {
        this.operator2 = operator2;
        return this;
    }

    public void setOperator2(String operator2) {
        this.operator2 = operator2;
    }

    public String getValue2() {
        return value2;
    }

    public RulesBook value2(String value2) {
        this.value2 = value2;
        return this;
    }

    public void setValue2(String value2) {
        this.value2 = value2;
    }

    public String getJoinField2() {
        return joinField2;
    }

    public RulesBook joinField2(String joinField2) {
        this.joinField2 = joinField2;
        return this;
    }

    public void setJoinField2(String joinField2) {
        this.joinField2 = joinField2;
    }

    public String getLookupPlace3() {
        return lookupPlace3;
    }

    public RulesBook lookupPlace3(String lookupPlace3) {
        this.lookupPlace3 = lookupPlace3;
        return this;
    }

    public void setLookupPlace3(String lookupPlace3) {
        this.lookupPlace3 = lookupPlace3;
    }

    public String getOperator3() {
        return operator3;
    }

    public RulesBook operator3(String operator3) {
        this.operator3 = operator3;
        return this;
    }

    public void setOperator3(String operator3) {
        this.operator3 = operator3;
    }

    public String getValue3() {
        return value3;
    }

    public RulesBook value3(String value3) {
        this.value3 = value3;
        return this;
    }

    public void setValue3(String value3) {
        this.value3 = value3;
    }

    public Integer getResult() {
        return result;
    }

    public RulesBook result(Integer result) {
        this.result = result;
        return this;
    }

    public void setResult(Integer result) {
        this.result = result;
    }

    public String getDescription() {
        return description;
    }

    public RulesBook description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RulesBook rulesBook = (RulesBook) o;
        if (rulesBook.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), rulesBook.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "RulesBook{" +
            "id=" + getId() +
            ", ruleNumber='" + getRuleNumber() + "'" +
            ", ruleSequence=" + getRuleSequence() +
            ", key=" + getKey() +
            ", lookupPlace1='" + getLookupPlace1() + "'" +
            ", operator1='" + getOperator1() + "'" +
            ", value1='" + getValue1() + "'" +
            ", joinField1='" + getJoinField1() + "'" +
            ", lookupPlace2='" + getLookupPlace2() + "'" +
            ", operator2='" + getOperator2() + "'" +
            ", value2='" + getValue2() + "'" +
            ", joinField2='" + getJoinField2() + "'" +
            ", lookupPlace3='" + getLookupPlace3() + "'" +
            ", operator3='" + getOperator3() + "'" +
            ", value3='" + getValue3() + "'" +
            ", result=" + getResult() +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
