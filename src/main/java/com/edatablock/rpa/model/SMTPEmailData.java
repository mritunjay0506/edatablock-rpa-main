/**
 * 
 */
package com.edatablock.rpa.model;

/**
 * @author Manish
 *
 */
public class SMTPEmailData {
	
	private String fromAddress;
	private String toAddress;
	private String ccAddress;
	private String bccAddress;
	private String subjct;
	private String mailBody;
	private boolean isHtmlContent;
	
	public String getFromAddress() {
		return fromAddress;
	}
	public void setFromAddress(String fromAddress) {
		this.fromAddress = fromAddress;
	}
	public String getToAddress() {
		return toAddress;
	}
	public void setToAddress(String toAddress) {
		this.toAddress = toAddress;
	}
	public String getCcAddress() {
		return ccAddress;
	}
	public void setCcAddress(String ccAddress) {
		this.ccAddress = ccAddress;
	}
	public String getBccAddress() {
		return bccAddress;
	}
	public void setBccAddress(String bccAddress) {
		this.bccAddress = bccAddress;
	}
	public String getSubjct() {
		return subjct;
	}
	public void setSubjct(String subjct) {
		this.subjct = subjct;
	}
	public String getMailBody() {
		return mailBody;
	}
	public void setMailBody(String mailBody) {
		this.mailBody = mailBody;
	}
	public boolean isHtmlContent() {
		return isHtmlContent;
	}
	public void setHtmlContent(boolean isHtmlContent) {
		this.isHtmlContent = isHtmlContent;
	}
	
	
}
