package com.edatablock.rpa.model;

public class RuleSet {
	
	private String lookupPlace;
	private String operator;
	private String value;
	private String fileName;
	private String awbFileName;
	private boolean isTemplateValidation;
	
	public String getLookupPlace() {
		return lookupPlace;
	}
	public void setLookupPlace(String lookupPlace) {
		this.lookupPlace = lookupPlace;
	}
	public String getOperator() {
		return operator;
	}
	public void setOperator(String operator) {
		this.operator = operator;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getAwbFileName() {
		return awbFileName;
	}
	public void setAwbFileName(String awbFileName) {
		this.awbFileName = awbFileName;
	}
	public boolean isTemplateValidation() {
		return isTemplateValidation;
	}
	public void setTemplateValidation(boolean isTemplateValidation) {
		this.isTemplateValidation = isTemplateValidation;
	}

}
