package com.edatablock.rpa.jms;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;
import java.util.stream.Collectors;

import javax.jms.BytesMessage;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.edatablock.rpa.common.Constants;
import com.edatablock.rpa.mail.EmailService;
import com.edatablock.rpa.model.RuleSet;
import com.edatablock.rpa.model.SMTPEmailData;
import com.edatablock.rpa.service.ClientDataOcrService;
import com.edatablock.rpa.service.EmailAttachmentService;
import com.edatablock.rpa.service.EmailMessagesService;
import com.edatablock.rpa.service.ErrorInProcessingService;
import com.edatablock.rpa.service.FileToFtpService;
import com.edatablock.rpa.service.InputTemplateQueryService;
import com.edatablock.rpa.service.InputTemplateService;
import com.edatablock.rpa.service.RulesBookQueryService;
import com.edatablock.rpa.service.TemplateFieldsQueryService;
import com.edatablock.rpa.service.TransactionService;
import com.edatablock.rpa.service.dto.ClientDataOcrDTO;
import com.edatablock.rpa.service.dto.EmailAttachmentDTO;
import com.edatablock.rpa.service.dto.EmailMessagesDTO;
import com.edatablock.rpa.service.dto.ErrorInProcessingDTO;
import com.edatablock.rpa.service.dto.FileToFtpDTO;
import com.edatablock.rpa.service.dto.InputTemplateCriteria;
import com.edatablock.rpa.service.dto.InputTemplateDTO;
import com.edatablock.rpa.service.dto.RulesBookCriteria;
import com.edatablock.rpa.service.dto.RulesBookDTO;
import com.edatablock.rpa.service.dto.TemplateFieldsCriteria;
import com.edatablock.rpa.service.dto.TemplateFieldsDTO;
import com.edatablock.rpa.service.dto.TransactionDTO;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

@Component
public class EDBMessageConsumer {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private EDBMessageProducer edbMessageProducer;

	@Autowired
	private EmailMessagesService emailMessagesService;

	@Autowired
	private EmailAttachmentService emailAttachmentService;

	@Autowired
	private InputTemplateService inputTemplateService;

	@Autowired
	private InputTemplateQueryService inputTemplateQueryService;

	@Autowired
	private TemplateFieldsQueryService templateFieldsQueryService;

	@Autowired
	private TransactionService transactionService;

	@Autowired
	private ClientDataOcrService clientDataOcrService;

	@Autowired
	private FileToFtpService fileToFtpService;

	@Autowired
	ErrorInProcessingService errorInProcessingService;

	@Autowired
	EmailService emailService;

	@Autowired
	ObjectMapper OBJECT_MAPPER_BEAN;

	@Autowired
	private RulesBookQueryService rulesBookQueryService;

	@Value("${folder.ocrDataFileLocation}")
	private String ocrDataFileLocation;

	@Value("${folder.ftpInbound}")
	private String ftpInbound;

	@JmsListener(destination = "${jms.queue.inbound.email}")
	public void receiveMailData(String msg) {
		logger.info("Start Email message received......");
		processEmailMessage(msg);
	}

	@JmsListener(destination = "${jms.queue.outbound.ocr}")
	public void receiveOCRProcessedData(Message message) {
		logger.info("receiveOCRProcessedData......" + message);

		if (message instanceof TextMessage) {
			TextMessage txtMsg = (TextMessage) message;
			try {
				logger.info("Inside receiveOCRProcessedData, message type is TextMessage......" + message);
				processOCRData(txtMsg.getText());
			} catch (JMSException e) {
				e.printStackTrace();
			}
		}

		if (message instanceof BytesMessage) {
			BytesMessage txtMsg = (BytesMessage) message;
			try {
				int TEXT_LENGTH = new Long(txtMsg.getBodyLength()).intValue();
				byte[] textBytes = new byte[TEXT_LENGTH];
				txtMsg.readBytes(textBytes, TEXT_LENGTH);
				String textString = new String(textBytes, "UTF-8");
				processOCRData(textString);

				logger.info("Inside receiveOCRProcessedData, message type is BytesMessage......" + message);

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@JmsListener(destination = "${jms.queue.smtpmaillsender}")
	public void receiveSMTPMailData(String msg) {

		try {
			sendSMTPeMail(msg);
			logger.info("Inside receiveSMTPMailData, message type is BytesMessage......" + msg);

		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@JmsListener(destination = "${jms.queue.error}")
	public void receiveErrorData(String msg) {
		logger.info("Error message received......");
		//processErrorData(msg);
	}

	@Async
	public void processEmailMessage(String msg) {
		logger.info("Start processing email data......" + msg);
		//Map<String, Object> dataToOCRMap = new HashMap<String, Object>();

		HashMap mailData = null;

		try {
			mailData = OBJECT_MAPPER_BEAN.readValue(msg, HashMap.class);
		} catch (JsonParseException e1) {
			e1.printStackTrace();
		} catch (JsonMappingException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		logger.info("Convert message String to MAP......");
		EmailMessagesDTO emailMessagesDTO = OBJECT_MAPPER_BEAN.convertValue(mailData.get("emailMsg"),
				new TypeReference<EmailMessagesDTO>() {
				});
		ArrayList<EmailAttachmentDTO> attachmentList = OBJECT_MAPPER_BEAN.convertValue(mailData.get("attachmentList"),
				new TypeReference<ArrayList<EmailAttachmentDTO>>() {
				});

		// Get All Client Rules to find Client
		List<RulesBookDTO> clientRulesList = getRulesBookList(Constants.RULE_ID_CLIENT, null);
		RulesBookDTO clientRulesBookDTO = null;
		Optional<RulesBookDTO> optClientRulesBookDTO = clientRulesList.stream().filter(rulesBook -> {
			return validateRulesBook(rulesBook, emailMessagesDTO, null, null, false);
		}).findFirst();

		if (optClientRulesBookDTO.isPresent()) {
			clientRulesBookDTO = optClientRulesBookDTO.get();
		} else {
			ErrorInProcessingDTO errorInProcessingDTO = new ErrorInProcessingDTO();
			errorInProcessingDTO.setException(Constants.EMAIL_ADDRESS_NOT_MATCH + " for MessageId : "
					+ emailMessagesDTO.getMessageId() + " eMail:" + emailMessagesDTO.getReceiveFrom());
			errorInProcessingDTO.setErrorCode(Constants.ERROR_CODE_CLIENT_NOT_MATCH);
			errorInProcessingDTO.setProcessBy(Constants.SYSTEM);
			errorInProcessingDTO.setProcessType(Constants.PROCESS_TYPE_EMAIL_PROCESSING);
			errorInProcessingDTO.setDescription(Constants.EMAIL_ADDRESS_NOT_MATCH);
			errorInProcessingDTO.setProcessID(Constants.PROCESS_ID_EMAIL_PROCESSING);
			errorInProcessingDTO.setCreateDate(Instant.now());
			errorInProcessingService.save(errorInProcessingDTO);
			logger.error("Client email address didn't match for " + emailMessagesDTO.getMessageId());
			return;
		}
		Map<String, Object> dataToOCRMap = new HashMap<String, Object>();
		// Get Client Data on eMailID
		logger.info("Fetching Client data......");
		emailMessagesDTO.setClientId(new Long(clientRulesBookDTO.getResult()));
		String emailBody = emailMessagesDTO.getEmailBody();
		emailMessagesDTO.setEmailBody(null);
		final EmailMessagesDTO emailMessagesDTOResult = emailMessagesService.save(emailMessagesDTO);
		dataToOCRMap.put("emailMsg", emailMessagesDTOResult);
		emailMessagesDTO.setEmailBody(emailBody);

		// Get All TEMPLATE Rules to find TEMPLATE-ID
		List<RulesBookDTO> templateRulesList = getRulesBookList(Constants.RULE_ID_TEMPLATE, clientRulesBookDTO.getResult());

		List<Object> emailAttachmentDataList = new ArrayList<Object>();
		Map<String, InputTemplateDTO> templateMap = new HashMap<String, InputTemplateDTO>();
		Map<String, List<TemplateFieldsDTO>> fieldsMap = new HashMap<String, List<TemplateFieldsDTO>>();
		Map<String, Map<String,Object>> awbMap = new HashMap<String, Map<String,Object>>();
		Map<String, Map<String,Object>> invMap = new HashMap<String, Map<String,Object>>();
		boolean templateDidNotMatch = false;
		for (EmailAttachmentDTO emailAttachmentDTO : attachmentList) {
			Map<String, Object> emailAttachmentDataMap = new HashMap<>();
			emailAttachmentDTO.setEmailMessagesId(emailMessagesDTOResult.getId());
			emailAttachmentDTO = emailAttachmentService.save(emailAttachmentDTO);

			final String fileName = emailAttachmentDTO.getFileName().substring(0, emailAttachmentDTO.getFileName().indexOf("."));
			Optional<RulesBookDTO> optTemplateRulesBookDTO = templateRulesList.stream().filter(rulesBook -> {
				return validateRulesBook(rulesBook, emailMessagesDTO, fileName, null, true);
			}).findFirst();
			if (optTemplateRulesBookDTO.isPresent()) {
				RulesBookDTO templateRulesBookDTO = optTemplateRulesBookDTO.get();
				Optional<InputTemplateDTO> optInputTemplateDTO = inputTemplateService
						.findOne(new Long(templateRulesBookDTO.getResult()));
				InputTemplateDTO inputTemplateDTO = optInputTemplateDTO.get();
				emailAttachmentDataMap.put("isTemplateMatch", true);
				emailAttachmentDataMap.put("templateId", inputTemplateDTO.getId());
				templateMap.put(inputTemplateDTO.getId().toString(), inputTemplateDTO);
				emailAttachmentDTO.setDocumentType(inputTemplateDTO.getTemplateType());
				logger.info("Fetching TemplateFields data......");
				List<TemplateFieldsDTO> templateFieldsDTOList = getTemplateFiledsList(inputTemplateDTO.getId());
				fieldsMap.put(inputTemplateDTO.getId().toString(), templateFieldsDTOList);
			} else {
				ErrorInProcessingDTO errorInProcessingDTO = new ErrorInProcessingDTO();
				errorInProcessingDTO.setException(Constants.TEMPLATE_NOT_MATCH + " for ClientId:"
						+ clientRulesBookDTO.getResult() + " & attachmentId:" + emailAttachmentDTO.getId());
				errorInProcessingDTO.setErrorCode(Constants.ERROR_CODE_TEMPLATE_NOT_MATCH);
				errorInProcessingDTO.setProcessBy(Constants.SYSTEM);
				errorInProcessingDTO.setProcessType(Constants.PROCESS_TYPE_TEMPLATE_IDENTIFICATION);
				errorInProcessingDTO.setDescription(Constants.TEMPLATE_NOT_MATCH);
				errorInProcessingDTO.setProcessID(Constants.PROCESS_ID_TETEMPLATE_IDENTIFICATION);
				errorInProcessingDTO.setCreateDate(Instant.now());
				errorInProcessingService.save(errorInProcessingDTO);
				logger.error("Template didn't match for attachmentId:" + emailAttachmentDTO.getId());
				emailAttachmentDataMap.put("isTemplateMatch", false);
				templateDidNotMatch = true;
			}
			emailAttachmentDataMap.put("attachmentData", emailAttachmentDTO);
			if(!templateDidNotMatch && "AWB".equalsIgnoreCase(emailAttachmentDTO.getDocumentType())) {
				awbMap.put(fileName, emailAttachmentDataMap);
			} else {
				invMap.put(fileName, emailAttachmentDataMap);
			}
			emailAttachmentDataList.add(emailAttachmentDataMap);
		}
		dataToOCRMap.put("emailAttachmentDataList", emailAttachmentDataList);
		if (templateDidNotMatch) {
			logger.info("Populating all Template and It's Fields data......");
			InputTemplateCriteria inputTemplateCriteria = new InputTemplateCriteria();
			inputTemplateCriteria
					.setClientId((LongFilter) new LongFilter().setEquals(new Long(clientRulesBookDTO.getResult()))); // TODO
			List<InputTemplateDTO> inputTemplateList = inputTemplateQueryService.findByCriteria(inputTemplateCriteria);
			logger.info("Number of Template : " + inputTemplateList.size());
			inputTemplateList.forEach(inputTemplate -> {
				if (!templateMap.containsKey(inputTemplate.getId().toString())) {
					templateMap.put(inputTemplate.getId().toString(), inputTemplate);
					List<TemplateFieldsDTO> templateFieldsDTOList = getTemplateFiledsList(inputTemplate.getId());
					fieldsMap.put(inputTemplate.getId().toString(), templateFieldsDTOList);
				}
			});
		}
		dataToOCRMap.put("allTemplate", templateMap);
		dataToOCRMap.put("allTemplateFields", fieldsMap);
		emailMessagesDTO.setEmailBody(null);
		logger.info("Data compilation complete......");
		try {
			if(templateDidNotMatch) {
				logger.info("Compiled Data Sending to INBOUNDOCRPROCESS(templateDidNotMatch) Queue......");
				//dataToOCRMap.put("allTemplate", templateMap);
				edbMessageProducer.sendMailToOCR_Q(dataToOCRMap);
			} else {
				// Get All TEMPLATE Rules to Group Files
				List<RulesBookDTO> groupFilesRulesList = getRulesBookList(Constants.RULE_ID_GROUP_FILES, clientRulesBookDTO.getResult());
				if(groupFilesRulesList.size()>=1 && awbMap.size()>1) {
					awbMap.forEach((k,v) -> {
						Map<String, Object> map = splitEmailMessageOnAWB(k, v, invMap, templateMap, fieldsMap, 
								emailMessagesDTOResult, groupFilesRulesList.get(0));
						try {
							logger.info("Split Compiled Data Sending to INBOUNDOCRPROCESS Queue......");
							edbMessageProducer.sendMailToDOC_Q(map);
						} catch (IOException e) {
							e.printStackTrace();
						}
					});
					if(!invMap.isEmpty()) {
						emailAttachmentDataList.clear();
						invMap.forEach((k,v)->{
							emailAttachmentDataList.add(v);
						});
						dataToOCRMap.put("emailAttachmentDataList", emailAttachmentDataList);
						logger.info("Rest of Split Data Sending to INBOUNDOCRPROCESS Queue......");
						edbMessageProducer.sendMailToDOC_Q(dataToOCRMap);
					}
				} else {
					//dataToOCRMap.put("allTemplate", templateMap);
					logger.info("Compiled Data Sending to INBOUNDDOCPROCESS Queue......");
					edbMessageProducer.sendMailToDOC_Q(dataToOCRMap);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		logger.info("End processing email data......");
	}
	
	private Map<String, Object> splitEmailMessageOnAWB(String awbFileName, 
			Map<String, Object> awbAttachmentMap, 
			Map<String, Map<String, Object>> invMap, 
			Map<String, InputTemplateDTO> allTemplateMMap, 
			Map<String, List<TemplateFieldsDTO>> allFieldsMap, 
			EmailMessagesDTO emailMessagesDTOResult, RulesBookDTO rulesBookDTO) {
		
		Map<String, Object> dataToOCRMap = new HashMap<String, Object>();
		List<Object> emailAttachmentDataList = new ArrayList<Object>();
		Map<String, InputTemplateDTO> templateMap = new HashMap<String, InputTemplateDTO>();
		Map<String, List<TemplateFieldsDTO>> fieldsMap = new HashMap<String, List<TemplateFieldsDTO>>();
		List<String> invList = new ArrayList<String>();
		emailAttachmentDataList.add(awbAttachmentMap);
		Long templateIdAWB = (Long) awbAttachmentMap.get("templateId");
		templateMap.put(templateIdAWB.toString(), allTemplateMMap.get(templateIdAWB.toString()));
		fieldsMap.put(templateIdAWB.toString(), allFieldsMap.get(templateIdAWB.toString()));
		invMap.forEach((key,val)->{
			boolean flag = validateRulesBook(rulesBookDTO, emailMessagesDTOResult, key, awbFileName, true);
			if(flag) {
				emailAttachmentDataList.add(val);
				Long templateId = (Long) val.get("templateId");
				templateMap.put(templateId.toString(), allTemplateMMap.get(templateId.toString()));
				fieldsMap.put(templateId.toString(), allFieldsMap.get(templateId.toString()));
				invList.add(key);
			}
		});
		dataToOCRMap.put("emailMsg", emailMessagesDTOResult);
		dataToOCRMap.put("emailAttachmentDataList", emailAttachmentDataList);
		dataToOCRMap.put("allTemplate", templateMap);
		dataToOCRMap.put("allTemplateFields", fieldsMap);
		
		invList.forEach(invKey->{
			invMap.remove(invKey);
		});
		
		return dataToOCRMap;
	}

	@SuppressWarnings("unchecked")
	@Async
	public void processOCRData(String msg) {
		try {
			final TreeMap<String, ClientDataOcrDTO> processedDataMap = new TreeMap<>();
			logger.info("inside processOCRData recieved message ......" + msg);

			Map<String, String> mailData = null;

			List list = OBJECT_MAPPER_BEAN.readValue(msg, List.class);
			LinkedHashMap map = (LinkedHashMap) list.get(0);
			list.forEach(clientDataOcrMap -> {
				LinkedHashMap clientDataOcrDTOTemp = ((LinkedHashMap) clientDataOcrMap);
				TransactionDTO transactionDTO = new TransactionDTO();
				transactionDTO.setClientEmailAddress((String) clientDataOcrDTOTemp.get(Constants.EMAIL_ADDRESS_KEY));
				transactionDTO.setCreateDate(Instant.now());
				transactionDTO.setCreatedBy(Constants.SYSTEM);
				transactionDTO.setMessageId((String) clientDataOcrDTOTemp.get(Constants.MESSAGE_ID_KEY));
				transactionDTO.setProcessType(Constants.OCR_PROCESSED);
				transactionDTO.setFileName((String) clientDataOcrDTOTemp.get(Constants.FILENAME_KEY));
				transactionDTO.setStatus(Constants.SUCCESS);
				transactionDTO = transactionService.save(transactionDTO);

				/***************************************************************************************/
				ClientDataOcrDTO clientDataOcrDTO = new ClientDataOcrDTO();
				clientDataOcrDTO.setTransactionId(transactionDTO.getId());
				Integer integer = (Integer) clientDataOcrDTOTemp.get(Constants.TEMPLATE_ID_KEY);
				Long templateID = new Long(integer);
				clientDataOcrDTO.setInputTemplateId(templateID);
				clientDataOcrDTO.setEmailMessageId((String) clientDataOcrDTOTemp.get(Constants.MESSAGE_ID_KEY));
				clientDataOcrDTO.setClientEmailAddress((String) clientDataOcrDTOTemp.get(Constants.EMAIL_ADDRESS_KEY));
				clientDataOcrDTO.setAttachmentfileName(((String) clientDataOcrDTOTemp.get(Constants.FILENAME_KEY)));
				clientDataOcrDTO.setInputTemplateTemplateName(
						(String) clientDataOcrDTOTemp.get(Constants.TEMPLATE_TEMPLATE_KEY));

				// clientDataOcrDTO.setExtractOcrDataJson((String)
				// clientDataOcrDTOTemp.get("extractOcrDataJson"));

				logger.info("inside processOCRData recieved ExtractOcrDataJson ......"
						+ clientDataOcrDTO.getExtractOcrDataJson());

				List list1 = (List) clientDataOcrDTOTemp.get(Constants.JSON_KEY);

				logger.info("inside processOCRData recieved ExtractOcrDataJson ......"
						+ clientDataOcrDTO.getExtractOcrDataJson());

				LinkedHashMap map1 = (LinkedHashMap) list1.get(0);

				JSONObject json = new JSONObject(map1);
				logger.info("JSON data  ......" + json.toString());


				clientDataOcrDTO.setExtractOcrDataJson(json.toString());
				clientDataOcrDTO.setSearchId((String) clientDataOcrDTOTemp.get(Constants.SEARCH_ID_KEY));
				logger.info("Before client data ocr saved  ......" + clientDataOcrDTO.getExtractOcrDataJson());

				ClientDataOcrDTO clientDataOcrDTOResult = clientDataOcrService.save(clientDataOcrDTO);
				logger.info("After client data ocr saved  ......" + clientDataOcrDTOResult.getSearchId());
				processedDataMap.put(clientDataOcrDTO.getInputTemplateTemplateName(), clientDataOcrDTO);

			});

			/***************************************************************************************/
			StringBuffer column = new StringBuffer();
			StringBuffer data = new StringBuffer();
			final ClientDataOcrDTO clientDataOcrDTOtemp = new ClientDataOcrDTO();
			String ocrFile = "CJ_" + getCurrentTimeStampAsString() + ".csv";// (String)fieldDataMap.get("fileName");
			try {
				processedDataMap.forEach((k, v) -> {
					ClientDataOcrDTO clientDataOcrDTO = (ClientDataOcrDTO) v;
					clientDataOcrDTOtemp.setClientEmailAddress(clientDataOcrDTO.getClientEmailAddress());
					clientDataOcrDTOtemp.setEmailMessageId(clientDataOcrDTO.getEmailMessageId());
					String jsonOcrData = clientDataOcrDTO.getExtractOcrDataJson();

					List<TemplateFieldsDTO> templateFieldsDTOList = getTemplateFiledsList(
							clientDataOcrDTO.getInputTemplateId());
					// List<TemplateFieldsDTO> templateFieldsDTOList = getTemplateFiledsList((long)
					// 1);
					Map<Integer, String> fieldMap = templateFieldsDTOList.stream()
							.sorted(Comparator.comparingInt(TemplateFieldsDTO::getSequence))
							.collect(Collectors.toMap(TemplateFieldsDTO::getSequence, TemplateFieldsDTO::getFieldName));
					Map<String, String> fieldDataMap = null;



					try {
						fieldDataMap = OBJECT_MAPPER_BEAN.readValue(jsonOcrData, HashMap.class);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					logger.info("Before creating CSV...... :" + fieldDataMap);

					final Map<String, String> fieldDataMapTemp = fieldDataMap;


					if (fieldDataMap != null) {
						fieldMap.forEach((fieldSequence, fieldName) -> {
							column.append(fieldName).append(",");
							data.append(StringEscapeUtils.escapeCsv(fieldDataMapTemp.get(fieldName))).append(",");
							logger.info("Creating CSV ...... Sequence:" + fieldSequence + "::fieldName::"+fieldName);

						});
					}

					// data.append("\n");

				});
				StringBuffer finalData = new StringBuffer();
				finalData.append(column).append("\n").append(data);
				if (!data.toString().isEmpty()) {
					File file = new File(ocrDataFileLocation + ocrFile);
					FileWriter writer = new FileWriter(file);
					writer.write(finalData.toString());
					writer.close();
					Path ocrFilePath = Paths.get(ocrDataFileLocation + ocrFile);
					Path ftpFilePath = Paths.get(ftpInbound + ocrFile);
					FileToFtpDTO fileToFtpDTO = new FileToFtpDTO();
					// fileToFtpDTO.setClientDataOcrId(clientDataOcrDTO.getId());
					fileToFtpDTO.setClientEmailAddress(clientDataOcrDTOtemp.getClientEmailAddress());
					fileToFtpDTO.setFileName(ocrFile);
					fileToFtpDTO.setMessageId(clientDataOcrDTOtemp.getEmailMessageId());
					fileToFtpDTO.setStatus(Constants.STATUS);
					fileToFtpService.save(fileToFtpDTO);
					Files.move(ocrFilePath, ftpFilePath, StandardCopyOption.REPLACE_EXISTING);
				}
			} catch (IOException e) {
				logger.error(e.getMessage());
			}

		} catch (JsonParseException e1) {
			logger.error(e1.getMessage());
		} catch (JsonMappingException e1) {
			logger.error(e1.getMessage());
		} catch (IOException e1) {
			logger.error(e1.getMessage());
		}
	}

	private List<TemplateFieldsDTO> getTemplateFiledsList(Long templateId) {
		TemplateFieldsCriteria templateFieldsCriteria = new TemplateFieldsCriteria();
		templateFieldsCriteria.setInputTemplateId((LongFilter) new LongFilter().setEquals(templateId));
		List<TemplateFieldsDTO> templateFieldsDTOList = templateFieldsQueryService
				.findByCriteria(templateFieldsCriteria);
		return templateFieldsDTOList;
	}

	private String getCurrentTimeStampAsString() {
		String pattern = "yyyyMMddHHmmss";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		String date = simpleDateFormat.format(new Date());
		return date;
	}

	@Async
	public void sendSMTPeMail(String smtpData) throws JsonParseException, JsonMappingException, IOException {
		Map emailMap = OBJECT_MAPPER_BEAN.readValue(smtpData, HashMap.class);
		System.out.println("emailMap>>>" + emailMap);
		SMTPEmailData emailData = OBJECT_MAPPER_BEAN.convertValue(emailMap, new TypeReference<SMTPEmailData>() {
		});
		System.out.println("emailData>>>" + emailData);
		emailService.sendEMail(emailData);
	}

	private List<RulesBookDTO> getRulesBookList(String ruleNumber, Integer clientId) {
		if (ruleNumber == null) {
			return null;
		}
		RulesBookCriteria rulesCriteria = new RulesBookCriteria();
		rulesCriteria.setRuleNumber((StringFilter) new StringFilter().setEquals(ruleNumber));
		if (clientId != null) {
			// rulesCriteria.setKey(new IntegerFilter().setIn(Arrays.asList(clientId)));
			rulesCriteria.setKey((IntegerFilter) new IntegerFilter().setEquals(clientId));
		}
		List<RulesBookDTO> clientRulesList = rulesBookQueryService.findByCriteria(rulesCriteria);
		return clientRulesList;
	}

	private boolean validateRulesBook(RulesBookDTO rulesBook, EmailMessagesDTO emailMessagesDTO, String fileName, String awbFileName,
			boolean isTemplateValidation) {
		
		RuleSet ruleSet1 = new RuleSet();
		ruleSet1.setLookupPlace(rulesBook.getLookupPlace1());
		ruleSet1.setOperator(rulesBook.getOperator1());
		ruleSet1.setValue(rulesBook.getValue1());
		ruleSet1.setFileName(fileName);
		ruleSet1.setAwbFileName(awbFileName);
		ruleSet1.setTemplateValidation(isTemplateValidation);
		
		RuleSet ruleSet2 = new RuleSet();
		ruleSet2.setLookupPlace(rulesBook.getLookupPlace2());
		ruleSet2.setOperator(rulesBook.getOperator2());
		ruleSet2.setValue(rulesBook.getValue2());
		ruleSet2.setFileName(fileName);
		ruleSet2.setAwbFileName(awbFileName);
		ruleSet2.setTemplateValidation(isTemplateValidation);
		
		if (rulesBook.getLookupPlace2() != null && rulesBook.getLookupPlace2().trim().length() == 0) {
			rulesBook.setLookupPlace2(null);
		}
		if (rulesBook.getLookupPlace1() != null && rulesBook.getLookupPlace2() == null) {
			return validateRuleSet(ruleSet1, emailMessagesDTO);
		} else if (rulesBook.getLookupPlace1() != null && rulesBook.getLookupPlace2() != null) {
			if ("AND".equalsIgnoreCase(rulesBook.getJoinField1())) {
				return (validateRuleSet(ruleSet1, emailMessagesDTO) && validateRuleSet(ruleSet2, emailMessagesDTO));
			} else { // JoinField1=OR
				return (validateRuleSet(ruleSet1, emailMessagesDTO) || validateRuleSet(ruleSet2, emailMessagesDTO));
			}
		}
		return false;
	}
	
	private boolean validateRuleSet(RuleSet ruleSet, EmailMessagesDTO emailMessagesDTO) {
		if ("SENDER".equalsIgnoreCase(ruleSet.getLookupPlace())) {
			if ("EQUALS".equalsIgnoreCase(ruleSet.getOperator())
					&& emailMessagesDTO.getClientEmailAddress().equalsIgnoreCase(ruleSet.getValue())) {
				return true;
			} else if ("CONTAINS".equalsIgnoreCase(ruleSet.getOperator()) && emailMessagesDTO.getClientEmailAddress()
					.toUpperCase().contains(ruleSet.getValue().toUpperCase())) {
				return true;
			}
		} else if ("SUBJECT".equalsIgnoreCase(ruleSet.getLookupPlace())
				&& emailMessagesDTO.getEmailSubject().toUpperCase().contains(ruleSet.getValue().toUpperCase())) {
			return true;
		} else if ("EMAIL_BODY".equalsIgnoreCase(ruleSet.getLookupPlace())
				&& emailMessagesDTO.getEmailBody().toUpperCase().contains(ruleSet.getValue().toUpperCase())) {
			return true;
		} else if (ruleSet.isTemplateValidation()) {
			if ("FILE_NAME".equalsIgnoreCase(ruleSet.getLookupPlace())) {
				if ("START_WITH".equalsIgnoreCase(ruleSet.getOperator())
						&& ruleSet.getFileName().toUpperCase().startsWith(ruleSet.getValue().toUpperCase())) {
					return true;
				} else if ("ENDS_WITH".equalsIgnoreCase(ruleSet.getOperator())
						&& ruleSet.getFileName().toUpperCase().endsWith(ruleSet.getValue().toUpperCase())) {
					return true;
				} else if ("CONTAINS".equalsIgnoreCase(ruleSet.getOperator())
						&& ruleSet.getFileName().toUpperCase().contains(ruleSet.getValue().toUpperCase())) {
					return true;
				} else if (ruleSet.getAwbFileName() != null) {
					return validateAwbFileWithInvoice(ruleSet);
				}
			}
		}
		return false;
	}
	
	private boolean validateAwbFileWithInvoice(RuleSet ruleSet) {
		if(StringUtils.isNumeric(ruleSet.getValue())) {
			int val = Integer.parseInt(ruleSet.getValue());
			if ("START_WITH_X_CHAR".equalsIgnoreCase(ruleSet.getOperator())) {
				int endIndex = ruleSet.getAwbFileName().length() > val ? val : ruleSet.getAwbFileName().length();
				return ruleSet.getFileName().toUpperCase().startsWith(ruleSet.getAwbFileName().substring(0, endIndex).toUpperCase());
			} else if ("ENDS_WITH_X_CHAR".equalsIgnoreCase(ruleSet.getOperator())) {
				int startIndex = ruleSet.getAwbFileName().length() > val ? ruleSet.getAwbFileName().length() - val : 0;
				return ruleSet.getFileName().toUpperCase().endsWith(ruleSet.getAwbFileName()
						.substring(startIndex, ruleSet.getAwbFileName().length()).toUpperCase());
			}
		}
		return false;
	}
}
