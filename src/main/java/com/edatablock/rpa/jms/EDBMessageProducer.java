package com.edatablock.rpa.jms;

import java.io.IOException;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.stereotype.Component;

@Component
public class EDBMessageProducer {
	
	@Value("${jms.queue.inbound.ocr}")
	private String DESTINATION_QUEUE_OCR;
	
	@Value("${jms.queue.inbound.docparser}")
	private String DESTINATION_QUEUE_DOC;
	
	@Autowired
	private JmsTemplate jmsTemplate;
	
	@Autowired
	MessageConverter jacksonJmsMessageConverter;

	public void sendMailToOCR_Q(Map<String, Object> map) throws IOException {
		this.jmsTemplate.setMessageConverter(jacksonJmsMessageConverter);
		jmsTemplate.convertAndSend(DESTINATION_QUEUE_OCR, map);
	}
	
	public void sendMailToDOC_Q(Map<String, Object> map) throws IOException {
		this.jmsTemplate.setMessageConverter(jacksonJmsMessageConverter);
		jmsTemplate.convertAndSend(DESTINATION_QUEUE_DOC, map);
	}
}
