package com.edatablock.rpa.mail;

import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Component;

import com.edatablock.rpa.model.SMTPEmailData;


@Component
public class EmailServiceImpl implements EmailService {

	@Autowired
	public JavaMailSender emailSender;

	public void sendSimpleMessage() {
		String to = "kr.manish.0505@gmail.com";
		//String to = "manish@localhost";
		String subject = "test mail.....";
		String text = "mail body";
		SimpleMailMessage message = new SimpleMailMessage();
		message.setFrom("manish@localhost");
		message.setTo(to);
		message.setSubject(subject);
		message.setText(text);
		emailSender.send(message);
		System.out.println("Mail sent......");
	}
	
	public void sendEMail(SMTPEmailData emailData) {
		emailSender.send(new MimeMessagePreparator() {
			@Override
			public void prepare(MimeMessage mimeMessage) throws Exception {
				MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true, "UTF-8");
				helper.setFrom(emailData.getFromAddress());
				helper.setTo(emailData.getToAddress().split(","));
				if(emailData.getCcAddress()!=null && emailData.getCcAddress().trim().length()>0) {
					helper.setCc(emailData.getCcAddress().split(","));
				}
				if(emailData.getBccAddress()!=null && emailData.getBccAddress().trim().length()>0) {
					helper.setBcc(emailData.getBccAddress().split(","));
				}
				helper.setSubject(emailData.getSubjct());
				helper.setText(emailData.getMailBody(), true);
			}
		});
	}
}
