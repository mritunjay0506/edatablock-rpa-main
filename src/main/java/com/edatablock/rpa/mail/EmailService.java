package com.edatablock.rpa.mail;

import com.edatablock.rpa.model.SMTPEmailData;

public interface EmailService {
	public void sendSimpleMessage();
	public void sendEMail(SMTPEmailData emailData);
}
