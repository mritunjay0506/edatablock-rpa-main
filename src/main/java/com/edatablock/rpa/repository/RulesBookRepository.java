package com.edatablock.rpa.repository;

import com.edatablock.rpa.domain.RulesBook;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the RulesBook entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RulesBookRepository extends JpaRepository<RulesBook, Long>, JpaSpecificationExecutor<RulesBook> {

}
