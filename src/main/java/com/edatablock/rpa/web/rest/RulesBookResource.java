package com.edatablock.rpa.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.edatablock.rpa.service.RulesBookService;
import com.edatablock.rpa.web.rest.errors.BadRequestAlertException;
import com.edatablock.rpa.web.rest.util.HeaderUtil;
import com.edatablock.rpa.web.rest.util.PaginationUtil;
import com.edatablock.rpa.service.dto.RulesBookDTO;
import com.edatablock.rpa.service.dto.RulesBookCriteria;
import com.edatablock.rpa.service.RulesBookQueryService;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing RulesBook.
 */
@RestController
@RequestMapping("/api")
public class RulesBookResource {

    private final Logger log = LoggerFactory.getLogger(RulesBookResource.class);

    private static final String ENTITY_NAME = "rulesBook";

    private final RulesBookService rulesBookService;

    private final RulesBookQueryService rulesBookQueryService;

    public RulesBookResource(RulesBookService rulesBookService, RulesBookQueryService rulesBookQueryService) {
        this.rulesBookService = rulesBookService;
        this.rulesBookQueryService = rulesBookQueryService;
    }

    /**
     * POST  /rules-books : Create a new rulesBook.
     *
     * @param rulesBookDTO the rulesBookDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new rulesBookDTO, or with status 400 (Bad Request) if the rulesBook has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/rules-books")
    @Timed
    public ResponseEntity<RulesBookDTO> createRulesBook(@RequestBody RulesBookDTO rulesBookDTO) throws URISyntaxException {
        log.debug("REST request to save RulesBook : {}", rulesBookDTO);
        if (rulesBookDTO.getId() != null) {
            throw new BadRequestAlertException("A new rulesBook cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RulesBookDTO result = rulesBookService.save(rulesBookDTO);
        return ResponseEntity.created(new URI("/api/rules-books/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /rules-books : Updates an existing rulesBook.
     *
     * @param rulesBookDTO the rulesBookDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated rulesBookDTO,
     * or with status 400 (Bad Request) if the rulesBookDTO is not valid,
     * or with status 500 (Internal Server Error) if the rulesBookDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/rules-books")
    @Timed
    public ResponseEntity<RulesBookDTO> updateRulesBook(@RequestBody RulesBookDTO rulesBookDTO) throws URISyntaxException {
        log.debug("REST request to update RulesBook : {}", rulesBookDTO);
        if (rulesBookDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        RulesBookDTO result = rulesBookService.save(rulesBookDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, rulesBookDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /rules-books : get all the rulesBooks.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of rulesBooks in body
     */
    @GetMapping("/rules-books")
    @Timed
    public ResponseEntity<List<RulesBookDTO>> getAllRulesBooks(RulesBookCriteria criteria, Pageable pageable) {
        log.debug("REST request to get RulesBooks by criteria: {}", criteria);
        Page<RulesBookDTO> page = rulesBookQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/rules-books");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /rules-books/:id : get the "id" rulesBook.
     *
     * @param id the id of the rulesBookDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the rulesBookDTO, or with status 404 (Not Found)
     */
    @GetMapping("/rules-books/{id}")
    @Timed
    public ResponseEntity<RulesBookDTO> getRulesBook(@PathVariable Long id) {
        log.debug("REST request to get RulesBook : {}", id);
        Optional<RulesBookDTO> rulesBookDTO = rulesBookService.findOne(id);
        return ResponseUtil.wrapOrNotFound(rulesBookDTO);
    }

    /**
     * DELETE  /rules-books/:id : delete the "id" rulesBook.
     *
     * @param id the id of the rulesBookDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/rules-books/{id}")
    @Timed
    public ResponseEntity<Void> deleteRulesBook(@PathVariable Long id) {
        log.debug("REST request to delete RulesBook : {}", id);
        rulesBookService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
