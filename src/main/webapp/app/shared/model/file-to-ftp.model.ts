import { Moment } from 'moment';

export interface IFileToFtp {
  id?: number;
  messageId?: string;
  clientEmailAddress?: string;
  status?: string;
  fileName?: string;
  fileType?: string;
  transactionId?: number;
  createDate?: Moment;
  clientDataOcrClientEmailAddress?: string;
  clientDataOcrId?: number;
}

export const defaultValue: Readonly<IFileToFtp> = {};
