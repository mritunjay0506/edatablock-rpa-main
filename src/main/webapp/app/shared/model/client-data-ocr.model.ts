export interface IClientDataOcr {
  id?: number;
  emailMessageId?: string;
  extractOcrDataJson?: string;
  searchId?: string;
  clientEmailAddress?: string;
  attachmentfileName?: string;
  transactionId?: number;
  inputTemplateTemplateName?: string;
  inputTemplateId?: number;
}

export const defaultValue: Readonly<IClientDataOcr> = {};
