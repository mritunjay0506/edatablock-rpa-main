export interface IRulesBook {
  id?: number;
  ruleNumber?: string;
  ruleSequence?: number;
  key?: number;
  lookupPlace1?: string;
  operator1?: string;
  value1?: string;
  joinField1?: string;
  lookupPlace2?: string;
  operator2?: string;
  value2?: string;
  joinField2?: string;
  lookupPlace3?: string;
  operator3?: string;
  value3?: string;
  result?: number;
  description?: string;
}

export const defaultValue: Readonly<IRulesBook> = {};
