import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import RulesBook from './rules-book';
import RulesBookDetail from './rules-book-detail';
import RulesBookUpdate from './rules-book-update';
import RulesBookDeleteDialog from './rules-book-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={RulesBookUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={RulesBookUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={RulesBookDetail} />
      <ErrorBoundaryRoute path={match.url} component={RulesBook} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={RulesBookDeleteDialog} />
  </>
);

export default Routes;
