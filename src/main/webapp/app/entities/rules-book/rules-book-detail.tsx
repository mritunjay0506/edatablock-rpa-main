import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './rules-book.reducer';
import { IRulesBook } from 'app/shared/model/rules-book.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IRulesBookDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class RulesBookDetail extends React.Component<IRulesBookDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { rulesBookEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            RulesBook [<b>{rulesBookEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="ruleNumber">Rule Number</span>
            </dt>
            <dd>{rulesBookEntity.ruleNumber}</dd>
            <dt>
              <span id="ruleSequence">Rule Sequence</span>
            </dt>
            <dd>{rulesBookEntity.ruleSequence}</dd>
            <dt>
              <span id="key">Key</span>
            </dt>
            <dd>{rulesBookEntity.key}</dd>
            <dt>
              <span id="lookupPlace1">Lookup Place 1</span>
            </dt>
            <dd>{rulesBookEntity.lookupPlace1}</dd>
            <dt>
              <span id="operator1">Operator 1</span>
            </dt>
            <dd>{rulesBookEntity.operator1}</dd>
            <dt>
              <span id="value1">Value 1</span>
            </dt>
            <dd>{rulesBookEntity.value1}</dd>
            <dt>
              <span id="joinField1">Join Field 1</span>
            </dt>
            <dd>{rulesBookEntity.joinField1}</dd>
            <dt>
              <span id="lookupPlace2">Lookup Place 2</span>
            </dt>
            <dd>{rulesBookEntity.lookupPlace2}</dd>
            <dt>
              <span id="operator2">Operator 2</span>
            </dt>
            <dd>{rulesBookEntity.operator2}</dd>
            <dt>
              <span id="value2">Value 2</span>
            </dt>
            <dd>{rulesBookEntity.value2}</dd>
            <dt>
              <span id="joinField2">Join Field 2</span>
            </dt>
            <dd>{rulesBookEntity.joinField2}</dd>
            <dt>
              <span id="lookupPlace3">Lookup Place 3</span>
            </dt>
            <dd>{rulesBookEntity.lookupPlace3}</dd>
            <dt>
              <span id="operator3">Operator 3</span>
            </dt>
            <dd>{rulesBookEntity.operator3}</dd>
            <dt>
              <span id="value3">Value 3</span>
            </dt>
            <dd>{rulesBookEntity.value3}</dd>
            <dt>
              <span id="result">Result</span>
            </dt>
            <dd>{rulesBookEntity.result}</dd>
            <dt>
              <span id="description">Description</span>
            </dt>
            <dd>{rulesBookEntity.description}</dd>
          </dl>
          <Button tag={Link} to="/entity/rules-book" replace color="info">
            <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
          </Button>&nbsp;
          <Button tag={Link} to={`/entity/rules-book/${rulesBookEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ rulesBook }: IRootState) => ({
  rulesBookEntity: rulesBook.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RulesBookDetail);
