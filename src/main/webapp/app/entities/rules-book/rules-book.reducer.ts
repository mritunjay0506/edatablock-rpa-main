import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IRulesBook, defaultValue } from 'app/shared/model/rules-book.model';

export const ACTION_TYPES = {
  FETCH_RULESBOOK_LIST: 'rulesBook/FETCH_RULESBOOK_LIST',
  FETCH_RULESBOOK: 'rulesBook/FETCH_RULESBOOK',
  CREATE_RULESBOOK: 'rulesBook/CREATE_RULESBOOK',
  UPDATE_RULESBOOK: 'rulesBook/UPDATE_RULESBOOK',
  DELETE_RULESBOOK: 'rulesBook/DELETE_RULESBOOK',
  RESET: 'rulesBook/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IRulesBook>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type RulesBookState = Readonly<typeof initialState>;

// Reducer

export default (state: RulesBookState = initialState, action): RulesBookState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_RULESBOOK_LIST):
    case REQUEST(ACTION_TYPES.FETCH_RULESBOOK):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_RULESBOOK):
    case REQUEST(ACTION_TYPES.UPDATE_RULESBOOK):
    case REQUEST(ACTION_TYPES.DELETE_RULESBOOK):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_RULESBOOK_LIST):
    case FAILURE(ACTION_TYPES.FETCH_RULESBOOK):
    case FAILURE(ACTION_TYPES.CREATE_RULESBOOK):
    case FAILURE(ACTION_TYPES.UPDATE_RULESBOOK):
    case FAILURE(ACTION_TYPES.DELETE_RULESBOOK):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_RULESBOOK_LIST):
      return {
        ...state,
        loading: false,
        totalItems: action.payload.headers['x-total-count'],
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_RULESBOOK):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_RULESBOOK):
    case SUCCESS(ACTION_TYPES.UPDATE_RULESBOOK):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_RULESBOOK):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/rules-books';

// Actions

export const getEntities: ICrudGetAllAction<IRulesBook> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_RULESBOOK_LIST,
    payload: axios.get<IRulesBook>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IRulesBook> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_RULESBOOK,
    payload: axios.get<IRulesBook>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IRulesBook> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_RULESBOOK,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IRulesBook> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_RULESBOOK,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IRulesBook> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_RULESBOOK,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
