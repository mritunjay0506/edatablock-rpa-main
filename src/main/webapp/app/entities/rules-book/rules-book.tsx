import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAllAction, getSortState, IPaginationBaseState, getPaginationItemsNumber, JhiPagination } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './rules-book.reducer';
import { IRulesBook } from 'app/shared/model/rules-book.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';

export interface IRulesBookProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export type IRulesBookState = IPaginationBaseState;

export class RulesBook extends React.Component<IRulesBookProps, IRulesBookState> {
  state: IRulesBookState = {
    ...getSortState(this.props.location, ITEMS_PER_PAGE)
  };

  componentDidMount() {
    this.getEntities();
  }

  sort = prop => () => {
    this.setState(
      {
        order: this.state.order === 'asc' ? 'desc' : 'asc',
        sort: prop
      },
      () => this.sortEntities()
    );
  };

  sortEntities() {
    this.getEntities();
    this.props.history.push(`${this.props.location.pathname}?page=${this.state.activePage}&sort=${this.state.sort},${this.state.order}`);
  }

  handlePagination = activePage => this.setState({ activePage }, () => this.sortEntities());

  getEntities = () => {
    const { activePage, itemsPerPage, sort, order } = this.state;
    this.props.getEntities(activePage - 1, itemsPerPage, `${sort},${order}`);
  };

  render() {
    const { rulesBookList, match, totalItems } = this.props;
    return (
      <div>
        <h2 id="rules-book-heading">
          Rules Books
          <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
            <FontAwesomeIcon icon="plus" />&nbsp; Create new Rules Book
          </Link>
        </h2>
        <div className="table-responsive">
          <Table responsive>
            <thead>
              <tr>
                <th className="hand" onClick={this.sort('id')}>
                  ID <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('ruleNumber')}>
                  Rule Number <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('ruleSequence')}>
                  Rule Sequence <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('key')}>
                  Key <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('lookupPlace1')}>
                  Lookup Place 1 <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('operator1')}>
                  Operator 1 <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('value1')}>
                  Value 1 <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('joinField1')}>
                  Join Field 1 <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('lookupPlace2')}>
                  Lookup Place 2 <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('operator2')}>
                  Operator 2 <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('value2')}>
                  Value 2 <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('joinField2')}>
                  Join Field 2 <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('lookupPlace3')}>
                  Lookup Place 3 <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('operator3')}>
                  Operator 3 <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('value3')}>
                  Value 3 <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('result')}>
                  Result <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('description')}>
                  Description <FontAwesomeIcon icon="sort" />
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {rulesBookList.map((rulesBook, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${rulesBook.id}`} color="link" size="sm">
                      {rulesBook.id}
                    </Button>
                  </td>
                  <td>{rulesBook.ruleNumber}</td>
                  <td>{rulesBook.ruleSequence}</td>
                  <td>{rulesBook.key}</td>
                  <td>{rulesBook.lookupPlace1}</td>
                  <td>{rulesBook.operator1}</td>
                  <td>{rulesBook.value1}</td>
                  <td>{rulesBook.joinField1}</td>
                  <td>{rulesBook.lookupPlace2}</td>
                  <td>{rulesBook.operator2}</td>
                  <td>{rulesBook.value2}</td>
                  <td>{rulesBook.joinField2}</td>
                  <td>{rulesBook.lookupPlace3}</td>
                  <td>{rulesBook.operator3}</td>
                  <td>{rulesBook.value3}</td>
                  <td>{rulesBook.result}</td>
                  <td>{rulesBook.description}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${rulesBook.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" /> <span className="d-none d-md-inline">View</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${rulesBook.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${rulesBook.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Delete</span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
        <Row className="justify-content-center">
          <JhiPagination
            items={getPaginationItemsNumber(totalItems, this.state.itemsPerPage)}
            activePage={this.state.activePage}
            onSelect={this.handlePagination}
            maxButtons={5}
          />
        </Row>
      </div>
    );
  }
}

const mapStateToProps = ({ rulesBook }: IRootState) => ({
  rulesBookList: rulesBook.entities,
  totalItems: rulesBook.totalItems
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RulesBook);
