import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './rules-book.reducer';
import { IRulesBook } from 'app/shared/model/rules-book.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IRulesBookUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IRulesBookUpdateState {
  isNew: boolean;
}

export class RulesBookUpdate extends React.Component<IRulesBookUpdateProps, IRulesBookUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { rulesBookEntity } = this.props;
      const entity = {
        ...rulesBookEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
      this.handleClose();
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/rules-book');
  };

  render() {
    const { rulesBookEntity, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="edatablockrpamainApp.rulesBook.home.createOrEditLabel">Create or edit a RulesBook</h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : rulesBookEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="id">ID</Label>
                    <AvInput id="rules-book-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="ruleNumberLabel" for="ruleNumber">
                    Rule Number
                  </Label>
                  <AvField id="rules-book-ruleNumber" type="text" name="ruleNumber" />
                </AvGroup>
                <AvGroup>
                  <Label id="ruleSequenceLabel" for="ruleSequence">
                    Rule Sequence
                  </Label>
                  <AvField id="rules-book-ruleSequence" type="string" className="form-control" name="ruleSequence" />
                </AvGroup>
                <AvGroup>
                  <Label id="keyLabel" for="key">
                    Key
                  </Label>
                  <AvField id="rules-book-key" type="string" className="form-control" name="key" />
                </AvGroup>
                <AvGroup>
                  <Label id="lookupPlace1Label" for="lookupPlace1">
                    Lookup Place 1
                  </Label>
                  <AvField id="rules-book-lookupPlace1" type="text" name="lookupPlace1" />
                </AvGroup>
                <AvGroup>
                  <Label id="operator1Label" for="operator1">
                    Operator 1
                  </Label>
                  <AvField id="rules-book-operator1" type="text" name="operator1" />
                </AvGroup>
                <AvGroup>
                  <Label id="value1Label" for="value1">
                    Value 1
                  </Label>
                  <AvField id="rules-book-value1" type="text" name="value1" />
                </AvGroup>
                <AvGroup>
                  <Label id="joinField1Label" for="joinField1">
                    Join Field 1
                  </Label>
                  <AvField id="rules-book-joinField1" type="text" name="joinField1" />
                </AvGroup>
                <AvGroup>
                  <Label id="lookupPlace2Label" for="lookupPlace2">
                    Lookup Place 2
                  </Label>
                  <AvField id="rules-book-lookupPlace2" type="text" name="lookupPlace2" />
                </AvGroup>
                <AvGroup>
                  <Label id="operator2Label" for="operator2">
                    Operator 2
                  </Label>
                  <AvField id="rules-book-operator2" type="text" name="operator2" />
                </AvGroup>
                <AvGroup>
                  <Label id="value2Label" for="value2">
                    Value 2
                  </Label>
                  <AvField id="rules-book-value2" type="text" name="value2" />
                </AvGroup>
                <AvGroup>
                  <Label id="joinField2Label" for="joinField2">
                    Join Field 2
                  </Label>
                  <AvField id="rules-book-joinField2" type="text" name="joinField2" />
                </AvGroup>
                <AvGroup>
                  <Label id="lookupPlace3Label" for="lookupPlace3">
                    Lookup Place 3
                  </Label>
                  <AvField id="rules-book-lookupPlace3" type="text" name="lookupPlace3" />
                </AvGroup>
                <AvGroup>
                  <Label id="operator3Label" for="operator3">
                    Operator 3
                  </Label>
                  <AvField id="rules-book-operator3" type="text" name="operator3" />
                </AvGroup>
                <AvGroup>
                  <Label id="value3Label" for="value3">
                    Value 3
                  </Label>
                  <AvField id="rules-book-value3" type="text" name="value3" />
                </AvGroup>
                <AvGroup>
                  <Label id="resultLabel" for="result">
                    Result
                  </Label>
                  <AvField id="rules-book-result" type="string" className="form-control" name="result" />
                </AvGroup>
                <AvGroup>
                  <Label id="descriptionLabel" for="description">
                    Description
                  </Label>
                  <AvField id="rules-book-description" type="text" name="description" />
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/rules-book" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />&nbsp;
                  <span className="d-none d-md-inline">Back</span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />&nbsp; Save
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  rulesBookEntity: storeState.rulesBook.entity,
  loading: storeState.rulesBook.loading,
  updating: storeState.rulesBook.updating
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RulesBookUpdate);
