package com.edatablock.rpa.web.rest;

import com.edatablock.rpa.EdatablockrpamainApp;

import com.edatablock.rpa.domain.TemplateFields;
import com.edatablock.rpa.domain.InputTemplate;
import com.edatablock.rpa.repository.TemplateFieldsRepository;
import com.edatablock.rpa.service.TemplateFieldsService;
import com.edatablock.rpa.service.dto.TemplateFieldsDTO;
import com.edatablock.rpa.service.mapper.TemplateFieldsMapper;
import com.edatablock.rpa.web.rest.errors.ExceptionTranslator;
import com.edatablock.rpa.service.dto.TemplateFieldsCriteria;
import com.edatablock.rpa.service.TemplateFieldsQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static com.edatablock.rpa.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the TemplateFieldsResource REST controller.
 *
 * @see TemplateFieldsResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = EdatablockrpamainApp.class)
public class TemplateFieldsResourceIntTest {

    private static final String DEFAULT_FIELD_NAME = "AAAAAAAAAA";
    private static final String UPDATED_FIELD_NAME = "BBBBBBBBBB";

    private static final Double DEFAULT_FIELD_ZONE_MIN_X = 1D;
    private static final Double UPDATED_FIELD_ZONE_MIN_X = 2D;

    private static final Double DEFAULT_FIELD_ZONE_MIN_Y = 1D;
    private static final Double UPDATED_FIELD_ZONE_MIN_Y = 2D;

    private static final Double DEFAULT_FIELD_ZONE_MAX_X = 1D;
    private static final Double UPDATED_FIELD_ZONE_MAX_X = 2D;

    private static final Double DEFAULT_FIELD_ZONE_MAX_Y = 1D;
    private static final Double UPDATED_FIELD_ZONE_MAX_Y = 2D;

    private static final Double DEFAULT_WIDTH = 1D;
    private static final Double UPDATED_WIDTH = 2D;

    private static final Double DEFAULT_HEIGHT = 1D;
    private static final Double UPDATED_HEIGHT = 2D;

    private static final Integer DEFAULT_SEQUENCE = 1;
    private static final Integer UPDATED_SEQUENCE = 2;

    private static final Integer DEFAULT_IS_TEMPLATE_IDENTIFIER = 1;
    private static final Integer UPDATED_IS_TEMPLATE_IDENTIFIER = 2;

    private static final Integer DEFAULT_IS_LABEL = 1;
    private static final Integer UPDATED_IS_LABEL = 2;

    private static final Integer DEFAULT_PAGE_NUMEBR = 1;
    private static final Integer UPDATED_PAGE_NUMEBR = 2;

    private static final Integer DEFAULT_FIELD_VALIDATION_REQUIRE = 1;
    private static final Integer UPDATED_FIELD_VALIDATION_REQUIRE = 2;

    private static final String DEFAULT_FIELD_VALIDATION_RULE = "AAAAAAAAAA";
    private static final String UPDATED_FIELD_VALIDATION_RULE = "BBBBBBBBBB";

    @Autowired
    private TemplateFieldsRepository templateFieldsRepository;

    @Autowired
    private TemplateFieldsMapper templateFieldsMapper;
    
    @Autowired
    private TemplateFieldsService templateFieldsService;

    @Autowired
    private TemplateFieldsQueryService templateFieldsQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restTemplateFieldsMockMvc;

    private TemplateFields templateFields;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TemplateFieldsResource templateFieldsResource = new TemplateFieldsResource(templateFieldsService, templateFieldsQueryService);
        this.restTemplateFieldsMockMvc = MockMvcBuilders.standaloneSetup(templateFieldsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TemplateFields createEntity(EntityManager em) {
        TemplateFields templateFields = new TemplateFields()
            .fieldName(DEFAULT_FIELD_NAME)
            .fieldZoneMinX(DEFAULT_FIELD_ZONE_MIN_X)
            .fieldZoneMinY(DEFAULT_FIELD_ZONE_MIN_Y)
            .fieldZoneMaxX(DEFAULT_FIELD_ZONE_MAX_X)
            .fieldZoneMaxY(DEFAULT_FIELD_ZONE_MAX_Y)
            .width(DEFAULT_WIDTH)
            .height(DEFAULT_HEIGHT)
            .sequence(DEFAULT_SEQUENCE)
            .isTemplateIdentifier(DEFAULT_IS_TEMPLATE_IDENTIFIER)
            .isLabel(DEFAULT_IS_LABEL)
            .pageNumebr(DEFAULT_PAGE_NUMEBR)
            .fieldValidationRequire(DEFAULT_FIELD_VALIDATION_REQUIRE)
            .fieldValidationRule(DEFAULT_FIELD_VALIDATION_RULE);
        return templateFields;
    }

    @Before
    public void initTest() {
        templateFields = createEntity(em);
    }

    @Test
    @Transactional
    public void createTemplateFields() throws Exception {
        int databaseSizeBeforeCreate = templateFieldsRepository.findAll().size();

        // Create the TemplateFields
        TemplateFieldsDTO templateFieldsDTO = templateFieldsMapper.toDto(templateFields);
        restTemplateFieldsMockMvc.perform(post("/api/template-fields")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(templateFieldsDTO)))
            .andExpect(status().isCreated());

        // Validate the TemplateFields in the database
        List<TemplateFields> templateFieldsList = templateFieldsRepository.findAll();
        assertThat(templateFieldsList).hasSize(databaseSizeBeforeCreate + 1);
        TemplateFields testTemplateFields = templateFieldsList.get(templateFieldsList.size() - 1);
        assertThat(testTemplateFields.getFieldName()).isEqualTo(DEFAULT_FIELD_NAME);
        assertThat(testTemplateFields.getFieldZoneMinX()).isEqualTo(DEFAULT_FIELD_ZONE_MIN_X);
        assertThat(testTemplateFields.getFieldZoneMinY()).isEqualTo(DEFAULT_FIELD_ZONE_MIN_Y);
        assertThat(testTemplateFields.getFieldZoneMaxX()).isEqualTo(DEFAULT_FIELD_ZONE_MAX_X);
        assertThat(testTemplateFields.getFieldZoneMaxY()).isEqualTo(DEFAULT_FIELD_ZONE_MAX_Y);
        assertThat(testTemplateFields.getWidth()).isEqualTo(DEFAULT_WIDTH);
        assertThat(testTemplateFields.getHeight()).isEqualTo(DEFAULT_HEIGHT);
        assertThat(testTemplateFields.getSequence()).isEqualTo(DEFAULT_SEQUENCE);
        assertThat(testTemplateFields.getIsTemplateIdentifier()).isEqualTo(DEFAULT_IS_TEMPLATE_IDENTIFIER);
        assertThat(testTemplateFields.getIsLabel()).isEqualTo(DEFAULT_IS_LABEL);
        assertThat(testTemplateFields.getPageNumebr()).isEqualTo(DEFAULT_PAGE_NUMEBR);
        assertThat(testTemplateFields.getFieldValidationRequire()).isEqualTo(DEFAULT_FIELD_VALIDATION_REQUIRE);
        assertThat(testTemplateFields.getFieldValidationRule()).isEqualTo(DEFAULT_FIELD_VALIDATION_RULE);
    }

    @Test
    @Transactional
    public void createTemplateFieldsWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = templateFieldsRepository.findAll().size();

        // Create the TemplateFields with an existing ID
        templateFields.setId(1L);
        TemplateFieldsDTO templateFieldsDTO = templateFieldsMapper.toDto(templateFields);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTemplateFieldsMockMvc.perform(post("/api/template-fields")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(templateFieldsDTO)))
            .andExpect(status().isBadRequest());

        // Validate the TemplateFields in the database
        List<TemplateFields> templateFieldsList = templateFieldsRepository.findAll();
        assertThat(templateFieldsList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkFieldNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = templateFieldsRepository.findAll().size();
        // set the field null
        templateFields.setFieldName(null);

        // Create the TemplateFields, which fails.
        TemplateFieldsDTO templateFieldsDTO = templateFieldsMapper.toDto(templateFields);

        restTemplateFieldsMockMvc.perform(post("/api/template-fields")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(templateFieldsDTO)))
            .andExpect(status().isBadRequest());

        List<TemplateFields> templateFieldsList = templateFieldsRepository.findAll();
        assertThat(templateFieldsList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkFieldZoneMinXIsRequired() throws Exception {
        int databaseSizeBeforeTest = templateFieldsRepository.findAll().size();
        // set the field null
        templateFields.setFieldZoneMinX(null);

        // Create the TemplateFields, which fails.
        TemplateFieldsDTO templateFieldsDTO = templateFieldsMapper.toDto(templateFields);

        restTemplateFieldsMockMvc.perform(post("/api/template-fields")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(templateFieldsDTO)))
            .andExpect(status().isBadRequest());

        List<TemplateFields> templateFieldsList = templateFieldsRepository.findAll();
        assertThat(templateFieldsList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkFieldZoneMinYIsRequired() throws Exception {
        int databaseSizeBeforeTest = templateFieldsRepository.findAll().size();
        // set the field null
        templateFields.setFieldZoneMinY(null);

        // Create the TemplateFields, which fails.
        TemplateFieldsDTO templateFieldsDTO = templateFieldsMapper.toDto(templateFields);

        restTemplateFieldsMockMvc.perform(post("/api/template-fields")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(templateFieldsDTO)))
            .andExpect(status().isBadRequest());

        List<TemplateFields> templateFieldsList = templateFieldsRepository.findAll();
        assertThat(templateFieldsList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkFieldZoneMaxXIsRequired() throws Exception {
        int databaseSizeBeforeTest = templateFieldsRepository.findAll().size();
        // set the field null
        templateFields.setFieldZoneMaxX(null);

        // Create the TemplateFields, which fails.
        TemplateFieldsDTO templateFieldsDTO = templateFieldsMapper.toDto(templateFields);

        restTemplateFieldsMockMvc.perform(post("/api/template-fields")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(templateFieldsDTO)))
            .andExpect(status().isBadRequest());

        List<TemplateFields> templateFieldsList = templateFieldsRepository.findAll();
        assertThat(templateFieldsList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkFieldZoneMaxYIsRequired() throws Exception {
        int databaseSizeBeforeTest = templateFieldsRepository.findAll().size();
        // set the field null
        templateFields.setFieldZoneMaxY(null);

        // Create the TemplateFields, which fails.
        TemplateFieldsDTO templateFieldsDTO = templateFieldsMapper.toDto(templateFields);

        restTemplateFieldsMockMvc.perform(post("/api/template-fields")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(templateFieldsDTO)))
            .andExpect(status().isBadRequest());

        List<TemplateFields> templateFieldsList = templateFieldsRepository.findAll();
        assertThat(templateFieldsList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTemplateFields() throws Exception {
        // Initialize the database
        templateFieldsRepository.saveAndFlush(templateFields);

        // Get all the templateFieldsList
        restTemplateFieldsMockMvc.perform(get("/api/template-fields?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(templateFields.getId().intValue())))
            .andExpect(jsonPath("$.[*].fieldName").value(hasItem(DEFAULT_FIELD_NAME.toString())))
            .andExpect(jsonPath("$.[*].fieldZoneMinX").value(hasItem(DEFAULT_FIELD_ZONE_MIN_X.doubleValue())))
            .andExpect(jsonPath("$.[*].fieldZoneMinY").value(hasItem(DEFAULT_FIELD_ZONE_MIN_Y.doubleValue())))
            .andExpect(jsonPath("$.[*].fieldZoneMaxX").value(hasItem(DEFAULT_FIELD_ZONE_MAX_X.doubleValue())))
            .andExpect(jsonPath("$.[*].fieldZoneMaxY").value(hasItem(DEFAULT_FIELD_ZONE_MAX_Y.doubleValue())))
            .andExpect(jsonPath("$.[*].width").value(hasItem(DEFAULT_WIDTH.doubleValue())))
            .andExpect(jsonPath("$.[*].height").value(hasItem(DEFAULT_HEIGHT.doubleValue())))
            .andExpect(jsonPath("$.[*].sequence").value(hasItem(DEFAULT_SEQUENCE)))
            .andExpect(jsonPath("$.[*].isTemplateIdentifier").value(hasItem(DEFAULT_IS_TEMPLATE_IDENTIFIER)))
            .andExpect(jsonPath("$.[*].isLabel").value(hasItem(DEFAULT_IS_LABEL)))
            .andExpect(jsonPath("$.[*].pageNumebr").value(hasItem(DEFAULT_PAGE_NUMEBR)))
            .andExpect(jsonPath("$.[*].fieldValidationRequire").value(hasItem(DEFAULT_FIELD_VALIDATION_REQUIRE)))
            .andExpect(jsonPath("$.[*].fieldValidationRule").value(hasItem(DEFAULT_FIELD_VALIDATION_RULE.toString())));
    }
    
    @Test
    @Transactional
    public void getTemplateFields() throws Exception {
        // Initialize the database
        templateFieldsRepository.saveAndFlush(templateFields);

        // Get the templateFields
        restTemplateFieldsMockMvc.perform(get("/api/template-fields/{id}", templateFields.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(templateFields.getId().intValue()))
            .andExpect(jsonPath("$.fieldName").value(DEFAULT_FIELD_NAME.toString()))
            .andExpect(jsonPath("$.fieldZoneMinX").value(DEFAULT_FIELD_ZONE_MIN_X.doubleValue()))
            .andExpect(jsonPath("$.fieldZoneMinY").value(DEFAULT_FIELD_ZONE_MIN_Y.doubleValue()))
            .andExpect(jsonPath("$.fieldZoneMaxX").value(DEFAULT_FIELD_ZONE_MAX_X.doubleValue()))
            .andExpect(jsonPath("$.fieldZoneMaxY").value(DEFAULT_FIELD_ZONE_MAX_Y.doubleValue()))
            .andExpect(jsonPath("$.width").value(DEFAULT_WIDTH.doubleValue()))
            .andExpect(jsonPath("$.height").value(DEFAULT_HEIGHT.doubleValue()))
            .andExpect(jsonPath("$.sequence").value(DEFAULT_SEQUENCE))
            .andExpect(jsonPath("$.isTemplateIdentifier").value(DEFAULT_IS_TEMPLATE_IDENTIFIER))
            .andExpect(jsonPath("$.isLabel").value(DEFAULT_IS_LABEL))
            .andExpect(jsonPath("$.pageNumebr").value(DEFAULT_PAGE_NUMEBR))
            .andExpect(jsonPath("$.fieldValidationRequire").value(DEFAULT_FIELD_VALIDATION_REQUIRE))
            .andExpect(jsonPath("$.fieldValidationRule").value(DEFAULT_FIELD_VALIDATION_RULE.toString()));
    }

    @Test
    @Transactional
    public void getAllTemplateFieldsByFieldNameIsEqualToSomething() throws Exception {
        // Initialize the database
        templateFieldsRepository.saveAndFlush(templateFields);

        // Get all the templateFieldsList where fieldName equals to DEFAULT_FIELD_NAME
        defaultTemplateFieldsShouldBeFound("fieldName.equals=" + DEFAULT_FIELD_NAME);

        // Get all the templateFieldsList where fieldName equals to UPDATED_FIELD_NAME
        defaultTemplateFieldsShouldNotBeFound("fieldName.equals=" + UPDATED_FIELD_NAME);
    }

    @Test
    @Transactional
    public void getAllTemplateFieldsByFieldNameIsInShouldWork() throws Exception {
        // Initialize the database
        templateFieldsRepository.saveAndFlush(templateFields);

        // Get all the templateFieldsList where fieldName in DEFAULT_FIELD_NAME or UPDATED_FIELD_NAME
        defaultTemplateFieldsShouldBeFound("fieldName.in=" + DEFAULT_FIELD_NAME + "," + UPDATED_FIELD_NAME);

        // Get all the templateFieldsList where fieldName equals to UPDATED_FIELD_NAME
        defaultTemplateFieldsShouldNotBeFound("fieldName.in=" + UPDATED_FIELD_NAME);
    }

    @Test
    @Transactional
    public void getAllTemplateFieldsByFieldNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        templateFieldsRepository.saveAndFlush(templateFields);

        // Get all the templateFieldsList where fieldName is not null
        defaultTemplateFieldsShouldBeFound("fieldName.specified=true");

        // Get all the templateFieldsList where fieldName is null
        defaultTemplateFieldsShouldNotBeFound("fieldName.specified=false");
    }

    @Test
    @Transactional
    public void getAllTemplateFieldsByFieldZoneMinXIsEqualToSomething() throws Exception {
        // Initialize the database
        templateFieldsRepository.saveAndFlush(templateFields);

        // Get all the templateFieldsList where fieldZoneMinX equals to DEFAULT_FIELD_ZONE_MIN_X
        defaultTemplateFieldsShouldBeFound("fieldZoneMinX.equals=" + DEFAULT_FIELD_ZONE_MIN_X);

        // Get all the templateFieldsList where fieldZoneMinX equals to UPDATED_FIELD_ZONE_MIN_X
        defaultTemplateFieldsShouldNotBeFound("fieldZoneMinX.equals=" + UPDATED_FIELD_ZONE_MIN_X);
    }

    @Test
    @Transactional
    public void getAllTemplateFieldsByFieldZoneMinXIsInShouldWork() throws Exception {
        // Initialize the database
        templateFieldsRepository.saveAndFlush(templateFields);

        // Get all the templateFieldsList where fieldZoneMinX in DEFAULT_FIELD_ZONE_MIN_X or UPDATED_FIELD_ZONE_MIN_X
        defaultTemplateFieldsShouldBeFound("fieldZoneMinX.in=" + DEFAULT_FIELD_ZONE_MIN_X + "," + UPDATED_FIELD_ZONE_MIN_X);

        // Get all the templateFieldsList where fieldZoneMinX equals to UPDATED_FIELD_ZONE_MIN_X
        defaultTemplateFieldsShouldNotBeFound("fieldZoneMinX.in=" + UPDATED_FIELD_ZONE_MIN_X);
    }

    @Test
    @Transactional
    public void getAllTemplateFieldsByFieldZoneMinXIsNullOrNotNull() throws Exception {
        // Initialize the database
        templateFieldsRepository.saveAndFlush(templateFields);

        // Get all the templateFieldsList where fieldZoneMinX is not null
        defaultTemplateFieldsShouldBeFound("fieldZoneMinX.specified=true");

        // Get all the templateFieldsList where fieldZoneMinX is null
        defaultTemplateFieldsShouldNotBeFound("fieldZoneMinX.specified=false");
    }

    @Test
    @Transactional
    public void getAllTemplateFieldsByFieldZoneMinYIsEqualToSomething() throws Exception {
        // Initialize the database
        templateFieldsRepository.saveAndFlush(templateFields);

        // Get all the templateFieldsList where fieldZoneMinY equals to DEFAULT_FIELD_ZONE_MIN_Y
        defaultTemplateFieldsShouldBeFound("fieldZoneMinY.equals=" + DEFAULT_FIELD_ZONE_MIN_Y);

        // Get all the templateFieldsList where fieldZoneMinY equals to UPDATED_FIELD_ZONE_MIN_Y
        defaultTemplateFieldsShouldNotBeFound("fieldZoneMinY.equals=" + UPDATED_FIELD_ZONE_MIN_Y);
    }

    @Test
    @Transactional
    public void getAllTemplateFieldsByFieldZoneMinYIsInShouldWork() throws Exception {
        // Initialize the database
        templateFieldsRepository.saveAndFlush(templateFields);

        // Get all the templateFieldsList where fieldZoneMinY in DEFAULT_FIELD_ZONE_MIN_Y or UPDATED_FIELD_ZONE_MIN_Y
        defaultTemplateFieldsShouldBeFound("fieldZoneMinY.in=" + DEFAULT_FIELD_ZONE_MIN_Y + "," + UPDATED_FIELD_ZONE_MIN_Y);

        // Get all the templateFieldsList where fieldZoneMinY equals to UPDATED_FIELD_ZONE_MIN_Y
        defaultTemplateFieldsShouldNotBeFound("fieldZoneMinY.in=" + UPDATED_FIELD_ZONE_MIN_Y);
    }

    @Test
    @Transactional
    public void getAllTemplateFieldsByFieldZoneMinYIsNullOrNotNull() throws Exception {
        // Initialize the database
        templateFieldsRepository.saveAndFlush(templateFields);

        // Get all the templateFieldsList where fieldZoneMinY is not null
        defaultTemplateFieldsShouldBeFound("fieldZoneMinY.specified=true");

        // Get all the templateFieldsList where fieldZoneMinY is null
        defaultTemplateFieldsShouldNotBeFound("fieldZoneMinY.specified=false");
    }

    @Test
    @Transactional
    public void getAllTemplateFieldsByFieldZoneMaxXIsEqualToSomething() throws Exception {
        // Initialize the database
        templateFieldsRepository.saveAndFlush(templateFields);

        // Get all the templateFieldsList where fieldZoneMaxX equals to DEFAULT_FIELD_ZONE_MAX_X
        defaultTemplateFieldsShouldBeFound("fieldZoneMaxX.equals=" + DEFAULT_FIELD_ZONE_MAX_X);

        // Get all the templateFieldsList where fieldZoneMaxX equals to UPDATED_FIELD_ZONE_MAX_X
        defaultTemplateFieldsShouldNotBeFound("fieldZoneMaxX.equals=" + UPDATED_FIELD_ZONE_MAX_X);
    }

    @Test
    @Transactional
    public void getAllTemplateFieldsByFieldZoneMaxXIsInShouldWork() throws Exception {
        // Initialize the database
        templateFieldsRepository.saveAndFlush(templateFields);

        // Get all the templateFieldsList where fieldZoneMaxX in DEFAULT_FIELD_ZONE_MAX_X or UPDATED_FIELD_ZONE_MAX_X
        defaultTemplateFieldsShouldBeFound("fieldZoneMaxX.in=" + DEFAULT_FIELD_ZONE_MAX_X + "," + UPDATED_FIELD_ZONE_MAX_X);

        // Get all the templateFieldsList where fieldZoneMaxX equals to UPDATED_FIELD_ZONE_MAX_X
        defaultTemplateFieldsShouldNotBeFound("fieldZoneMaxX.in=" + UPDATED_FIELD_ZONE_MAX_X);
    }

    @Test
    @Transactional
    public void getAllTemplateFieldsByFieldZoneMaxXIsNullOrNotNull() throws Exception {
        // Initialize the database
        templateFieldsRepository.saveAndFlush(templateFields);

        // Get all the templateFieldsList where fieldZoneMaxX is not null
        defaultTemplateFieldsShouldBeFound("fieldZoneMaxX.specified=true");

        // Get all the templateFieldsList where fieldZoneMaxX is null
        defaultTemplateFieldsShouldNotBeFound("fieldZoneMaxX.specified=false");
    }

    @Test
    @Transactional
    public void getAllTemplateFieldsByFieldZoneMaxYIsEqualToSomething() throws Exception {
        // Initialize the database
        templateFieldsRepository.saveAndFlush(templateFields);

        // Get all the templateFieldsList where fieldZoneMaxY equals to DEFAULT_FIELD_ZONE_MAX_Y
        defaultTemplateFieldsShouldBeFound("fieldZoneMaxY.equals=" + DEFAULT_FIELD_ZONE_MAX_Y);

        // Get all the templateFieldsList where fieldZoneMaxY equals to UPDATED_FIELD_ZONE_MAX_Y
        defaultTemplateFieldsShouldNotBeFound("fieldZoneMaxY.equals=" + UPDATED_FIELD_ZONE_MAX_Y);
    }

    @Test
    @Transactional
    public void getAllTemplateFieldsByFieldZoneMaxYIsInShouldWork() throws Exception {
        // Initialize the database
        templateFieldsRepository.saveAndFlush(templateFields);

        // Get all the templateFieldsList where fieldZoneMaxY in DEFAULT_FIELD_ZONE_MAX_Y or UPDATED_FIELD_ZONE_MAX_Y
        defaultTemplateFieldsShouldBeFound("fieldZoneMaxY.in=" + DEFAULT_FIELD_ZONE_MAX_Y + "," + UPDATED_FIELD_ZONE_MAX_Y);

        // Get all the templateFieldsList where fieldZoneMaxY equals to UPDATED_FIELD_ZONE_MAX_Y
        defaultTemplateFieldsShouldNotBeFound("fieldZoneMaxY.in=" + UPDATED_FIELD_ZONE_MAX_Y);
    }

    @Test
    @Transactional
    public void getAllTemplateFieldsByFieldZoneMaxYIsNullOrNotNull() throws Exception {
        // Initialize the database
        templateFieldsRepository.saveAndFlush(templateFields);

        // Get all the templateFieldsList where fieldZoneMaxY is not null
        defaultTemplateFieldsShouldBeFound("fieldZoneMaxY.specified=true");

        // Get all the templateFieldsList where fieldZoneMaxY is null
        defaultTemplateFieldsShouldNotBeFound("fieldZoneMaxY.specified=false");
    }

    @Test
    @Transactional
    public void getAllTemplateFieldsByWidthIsEqualToSomething() throws Exception {
        // Initialize the database
        templateFieldsRepository.saveAndFlush(templateFields);

        // Get all the templateFieldsList where width equals to DEFAULT_WIDTH
        defaultTemplateFieldsShouldBeFound("width.equals=" + DEFAULT_WIDTH);

        // Get all the templateFieldsList where width equals to UPDATED_WIDTH
        defaultTemplateFieldsShouldNotBeFound("width.equals=" + UPDATED_WIDTH);
    }

    @Test
    @Transactional
    public void getAllTemplateFieldsByWidthIsInShouldWork() throws Exception {
        // Initialize the database
        templateFieldsRepository.saveAndFlush(templateFields);

        // Get all the templateFieldsList where width in DEFAULT_WIDTH or UPDATED_WIDTH
        defaultTemplateFieldsShouldBeFound("width.in=" + DEFAULT_WIDTH + "," + UPDATED_WIDTH);

        // Get all the templateFieldsList where width equals to UPDATED_WIDTH
        defaultTemplateFieldsShouldNotBeFound("width.in=" + UPDATED_WIDTH);
    }

    @Test
    @Transactional
    public void getAllTemplateFieldsByWidthIsNullOrNotNull() throws Exception {
        // Initialize the database
        templateFieldsRepository.saveAndFlush(templateFields);

        // Get all the templateFieldsList where width is not null
        defaultTemplateFieldsShouldBeFound("width.specified=true");

        // Get all the templateFieldsList where width is null
        defaultTemplateFieldsShouldNotBeFound("width.specified=false");
    }

    @Test
    @Transactional
    public void getAllTemplateFieldsByHeightIsEqualToSomething() throws Exception {
        // Initialize the database
        templateFieldsRepository.saveAndFlush(templateFields);

        // Get all the templateFieldsList where height equals to DEFAULT_HEIGHT
        defaultTemplateFieldsShouldBeFound("height.equals=" + DEFAULT_HEIGHT);

        // Get all the templateFieldsList where height equals to UPDATED_HEIGHT
        defaultTemplateFieldsShouldNotBeFound("height.equals=" + UPDATED_HEIGHT);
    }

    @Test
    @Transactional
    public void getAllTemplateFieldsByHeightIsInShouldWork() throws Exception {
        // Initialize the database
        templateFieldsRepository.saveAndFlush(templateFields);

        // Get all the templateFieldsList where height in DEFAULT_HEIGHT or UPDATED_HEIGHT
        defaultTemplateFieldsShouldBeFound("height.in=" + DEFAULT_HEIGHT + "," + UPDATED_HEIGHT);

        // Get all the templateFieldsList where height equals to UPDATED_HEIGHT
        defaultTemplateFieldsShouldNotBeFound("height.in=" + UPDATED_HEIGHT);
    }

    @Test
    @Transactional
    public void getAllTemplateFieldsByHeightIsNullOrNotNull() throws Exception {
        // Initialize the database
        templateFieldsRepository.saveAndFlush(templateFields);

        // Get all the templateFieldsList where height is not null
        defaultTemplateFieldsShouldBeFound("height.specified=true");

        // Get all the templateFieldsList where height is null
        defaultTemplateFieldsShouldNotBeFound("height.specified=false");
    }

    @Test
    @Transactional
    public void getAllTemplateFieldsBySequenceIsEqualToSomething() throws Exception {
        // Initialize the database
        templateFieldsRepository.saveAndFlush(templateFields);

        // Get all the templateFieldsList where sequence equals to DEFAULT_SEQUENCE
        defaultTemplateFieldsShouldBeFound("sequence.equals=" + DEFAULT_SEQUENCE);

        // Get all the templateFieldsList where sequence equals to UPDATED_SEQUENCE
        defaultTemplateFieldsShouldNotBeFound("sequence.equals=" + UPDATED_SEQUENCE);
    }

    @Test
    @Transactional
    public void getAllTemplateFieldsBySequenceIsInShouldWork() throws Exception {
        // Initialize the database
        templateFieldsRepository.saveAndFlush(templateFields);

        // Get all the templateFieldsList where sequence in DEFAULT_SEQUENCE or UPDATED_SEQUENCE
        defaultTemplateFieldsShouldBeFound("sequence.in=" + DEFAULT_SEQUENCE + "," + UPDATED_SEQUENCE);

        // Get all the templateFieldsList where sequence equals to UPDATED_SEQUENCE
        defaultTemplateFieldsShouldNotBeFound("sequence.in=" + UPDATED_SEQUENCE);
    }

    @Test
    @Transactional
    public void getAllTemplateFieldsBySequenceIsNullOrNotNull() throws Exception {
        // Initialize the database
        templateFieldsRepository.saveAndFlush(templateFields);

        // Get all the templateFieldsList where sequence is not null
        defaultTemplateFieldsShouldBeFound("sequence.specified=true");

        // Get all the templateFieldsList where sequence is null
        defaultTemplateFieldsShouldNotBeFound("sequence.specified=false");
    }

    @Test
    @Transactional
    public void getAllTemplateFieldsBySequenceIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        templateFieldsRepository.saveAndFlush(templateFields);

        // Get all the templateFieldsList where sequence greater than or equals to DEFAULT_SEQUENCE
        defaultTemplateFieldsShouldBeFound("sequence.greaterOrEqualThan=" + DEFAULT_SEQUENCE);

        // Get all the templateFieldsList where sequence greater than or equals to UPDATED_SEQUENCE
        defaultTemplateFieldsShouldNotBeFound("sequence.greaterOrEqualThan=" + UPDATED_SEQUENCE);
    }

    @Test
    @Transactional
    public void getAllTemplateFieldsBySequenceIsLessThanSomething() throws Exception {
        // Initialize the database
        templateFieldsRepository.saveAndFlush(templateFields);

        // Get all the templateFieldsList where sequence less than or equals to DEFAULT_SEQUENCE
        defaultTemplateFieldsShouldNotBeFound("sequence.lessThan=" + DEFAULT_SEQUENCE);

        // Get all the templateFieldsList where sequence less than or equals to UPDATED_SEQUENCE
        defaultTemplateFieldsShouldBeFound("sequence.lessThan=" + UPDATED_SEQUENCE);
    }


    @Test
    @Transactional
    public void getAllTemplateFieldsByIsTemplateIdentifierIsEqualToSomething() throws Exception {
        // Initialize the database
        templateFieldsRepository.saveAndFlush(templateFields);

        // Get all the templateFieldsList where isTemplateIdentifier equals to DEFAULT_IS_TEMPLATE_IDENTIFIER
        defaultTemplateFieldsShouldBeFound("isTemplateIdentifier.equals=" + DEFAULT_IS_TEMPLATE_IDENTIFIER);

        // Get all the templateFieldsList where isTemplateIdentifier equals to UPDATED_IS_TEMPLATE_IDENTIFIER
        defaultTemplateFieldsShouldNotBeFound("isTemplateIdentifier.equals=" + UPDATED_IS_TEMPLATE_IDENTIFIER);
    }

    @Test
    @Transactional
    public void getAllTemplateFieldsByIsTemplateIdentifierIsInShouldWork() throws Exception {
        // Initialize the database
        templateFieldsRepository.saveAndFlush(templateFields);

        // Get all the templateFieldsList where isTemplateIdentifier in DEFAULT_IS_TEMPLATE_IDENTIFIER or UPDATED_IS_TEMPLATE_IDENTIFIER
        defaultTemplateFieldsShouldBeFound("isTemplateIdentifier.in=" + DEFAULT_IS_TEMPLATE_IDENTIFIER + "," + UPDATED_IS_TEMPLATE_IDENTIFIER);

        // Get all the templateFieldsList where isTemplateIdentifier equals to UPDATED_IS_TEMPLATE_IDENTIFIER
        defaultTemplateFieldsShouldNotBeFound("isTemplateIdentifier.in=" + UPDATED_IS_TEMPLATE_IDENTIFIER);
    }

    @Test
    @Transactional
    public void getAllTemplateFieldsByIsTemplateIdentifierIsNullOrNotNull() throws Exception {
        // Initialize the database
        templateFieldsRepository.saveAndFlush(templateFields);

        // Get all the templateFieldsList where isTemplateIdentifier is not null
        defaultTemplateFieldsShouldBeFound("isTemplateIdentifier.specified=true");

        // Get all the templateFieldsList where isTemplateIdentifier is null
        defaultTemplateFieldsShouldNotBeFound("isTemplateIdentifier.specified=false");
    }

    @Test
    @Transactional
    public void getAllTemplateFieldsByIsTemplateIdentifierIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        templateFieldsRepository.saveAndFlush(templateFields);

        // Get all the templateFieldsList where isTemplateIdentifier greater than or equals to DEFAULT_IS_TEMPLATE_IDENTIFIER
        defaultTemplateFieldsShouldBeFound("isTemplateIdentifier.greaterOrEqualThan=" + DEFAULT_IS_TEMPLATE_IDENTIFIER);

        // Get all the templateFieldsList where isTemplateIdentifier greater than or equals to UPDATED_IS_TEMPLATE_IDENTIFIER
        defaultTemplateFieldsShouldNotBeFound("isTemplateIdentifier.greaterOrEqualThan=" + UPDATED_IS_TEMPLATE_IDENTIFIER);
    }

    @Test
    @Transactional
    public void getAllTemplateFieldsByIsTemplateIdentifierIsLessThanSomething() throws Exception {
        // Initialize the database
        templateFieldsRepository.saveAndFlush(templateFields);

        // Get all the templateFieldsList where isTemplateIdentifier less than or equals to DEFAULT_IS_TEMPLATE_IDENTIFIER
        defaultTemplateFieldsShouldNotBeFound("isTemplateIdentifier.lessThan=" + DEFAULT_IS_TEMPLATE_IDENTIFIER);

        // Get all the templateFieldsList where isTemplateIdentifier less than or equals to UPDATED_IS_TEMPLATE_IDENTIFIER
        defaultTemplateFieldsShouldBeFound("isTemplateIdentifier.lessThan=" + UPDATED_IS_TEMPLATE_IDENTIFIER);
    }


    @Test
    @Transactional
    public void getAllTemplateFieldsByIsLabelIsEqualToSomething() throws Exception {
        // Initialize the database
        templateFieldsRepository.saveAndFlush(templateFields);

        // Get all the templateFieldsList where isLabel equals to DEFAULT_IS_LABEL
        defaultTemplateFieldsShouldBeFound("isLabel.equals=" + DEFAULT_IS_LABEL);

        // Get all the templateFieldsList where isLabel equals to UPDATED_IS_LABEL
        defaultTemplateFieldsShouldNotBeFound("isLabel.equals=" + UPDATED_IS_LABEL);
    }

    @Test
    @Transactional
    public void getAllTemplateFieldsByIsLabelIsInShouldWork() throws Exception {
        // Initialize the database
        templateFieldsRepository.saveAndFlush(templateFields);

        // Get all the templateFieldsList where isLabel in DEFAULT_IS_LABEL or UPDATED_IS_LABEL
        defaultTemplateFieldsShouldBeFound("isLabel.in=" + DEFAULT_IS_LABEL + "," + UPDATED_IS_LABEL);

        // Get all the templateFieldsList where isLabel equals to UPDATED_IS_LABEL
        defaultTemplateFieldsShouldNotBeFound("isLabel.in=" + UPDATED_IS_LABEL);
    }

    @Test
    @Transactional
    public void getAllTemplateFieldsByIsLabelIsNullOrNotNull() throws Exception {
        // Initialize the database
        templateFieldsRepository.saveAndFlush(templateFields);

        // Get all the templateFieldsList where isLabel is not null
        defaultTemplateFieldsShouldBeFound("isLabel.specified=true");

        // Get all the templateFieldsList where isLabel is null
        defaultTemplateFieldsShouldNotBeFound("isLabel.specified=false");
    }

    @Test
    @Transactional
    public void getAllTemplateFieldsByIsLabelIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        templateFieldsRepository.saveAndFlush(templateFields);

        // Get all the templateFieldsList where isLabel greater than or equals to DEFAULT_IS_LABEL
        defaultTemplateFieldsShouldBeFound("isLabel.greaterOrEqualThan=" + DEFAULT_IS_LABEL);

        // Get all the templateFieldsList where isLabel greater than or equals to UPDATED_IS_LABEL
        defaultTemplateFieldsShouldNotBeFound("isLabel.greaterOrEqualThan=" + UPDATED_IS_LABEL);
    }

    @Test
    @Transactional
    public void getAllTemplateFieldsByIsLabelIsLessThanSomething() throws Exception {
        // Initialize the database
        templateFieldsRepository.saveAndFlush(templateFields);

        // Get all the templateFieldsList where isLabel less than or equals to DEFAULT_IS_LABEL
        defaultTemplateFieldsShouldNotBeFound("isLabel.lessThan=" + DEFAULT_IS_LABEL);

        // Get all the templateFieldsList where isLabel less than or equals to UPDATED_IS_LABEL
        defaultTemplateFieldsShouldBeFound("isLabel.lessThan=" + UPDATED_IS_LABEL);
    }


    @Test
    @Transactional
    public void getAllTemplateFieldsByPageNumebrIsEqualToSomething() throws Exception {
        // Initialize the database
        templateFieldsRepository.saveAndFlush(templateFields);

        // Get all the templateFieldsList where pageNumebr equals to DEFAULT_PAGE_NUMEBR
        defaultTemplateFieldsShouldBeFound("pageNumebr.equals=" + DEFAULT_PAGE_NUMEBR);

        // Get all the templateFieldsList where pageNumebr equals to UPDATED_PAGE_NUMEBR
        defaultTemplateFieldsShouldNotBeFound("pageNumebr.equals=" + UPDATED_PAGE_NUMEBR);
    }

    @Test
    @Transactional
    public void getAllTemplateFieldsByPageNumebrIsInShouldWork() throws Exception {
        // Initialize the database
        templateFieldsRepository.saveAndFlush(templateFields);

        // Get all the templateFieldsList where pageNumebr in DEFAULT_PAGE_NUMEBR or UPDATED_PAGE_NUMEBR
        defaultTemplateFieldsShouldBeFound("pageNumebr.in=" + DEFAULT_PAGE_NUMEBR + "," + UPDATED_PAGE_NUMEBR);

        // Get all the templateFieldsList where pageNumebr equals to UPDATED_PAGE_NUMEBR
        defaultTemplateFieldsShouldNotBeFound("pageNumebr.in=" + UPDATED_PAGE_NUMEBR);
    }

    @Test
    @Transactional
    public void getAllTemplateFieldsByPageNumebrIsNullOrNotNull() throws Exception {
        // Initialize the database
        templateFieldsRepository.saveAndFlush(templateFields);

        // Get all the templateFieldsList where pageNumebr is not null
        defaultTemplateFieldsShouldBeFound("pageNumebr.specified=true");

        // Get all the templateFieldsList where pageNumebr is null
        defaultTemplateFieldsShouldNotBeFound("pageNumebr.specified=false");
    }

    @Test
    @Transactional
    public void getAllTemplateFieldsByPageNumebrIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        templateFieldsRepository.saveAndFlush(templateFields);

        // Get all the templateFieldsList where pageNumebr greater than or equals to DEFAULT_PAGE_NUMEBR
        defaultTemplateFieldsShouldBeFound("pageNumebr.greaterOrEqualThan=" + DEFAULT_PAGE_NUMEBR);

        // Get all the templateFieldsList where pageNumebr greater than or equals to UPDATED_PAGE_NUMEBR
        defaultTemplateFieldsShouldNotBeFound("pageNumebr.greaterOrEqualThan=" + UPDATED_PAGE_NUMEBR);
    }

    @Test
    @Transactional
    public void getAllTemplateFieldsByPageNumebrIsLessThanSomething() throws Exception {
        // Initialize the database
        templateFieldsRepository.saveAndFlush(templateFields);

        // Get all the templateFieldsList where pageNumebr less than or equals to DEFAULT_PAGE_NUMEBR
        defaultTemplateFieldsShouldNotBeFound("pageNumebr.lessThan=" + DEFAULT_PAGE_NUMEBR);

        // Get all the templateFieldsList where pageNumebr less than or equals to UPDATED_PAGE_NUMEBR
        defaultTemplateFieldsShouldBeFound("pageNumebr.lessThan=" + UPDATED_PAGE_NUMEBR);
    }


    @Test
    @Transactional
    public void getAllTemplateFieldsByFieldValidationRequireIsEqualToSomething() throws Exception {
        // Initialize the database
        templateFieldsRepository.saveAndFlush(templateFields);

        // Get all the templateFieldsList where fieldValidationRequire equals to DEFAULT_FIELD_VALIDATION_REQUIRE
        defaultTemplateFieldsShouldBeFound("fieldValidationRequire.equals=" + DEFAULT_FIELD_VALIDATION_REQUIRE);

        // Get all the templateFieldsList where fieldValidationRequire equals to UPDATED_FIELD_VALIDATION_REQUIRE
        defaultTemplateFieldsShouldNotBeFound("fieldValidationRequire.equals=" + UPDATED_FIELD_VALIDATION_REQUIRE);
    }

    @Test
    @Transactional
    public void getAllTemplateFieldsByFieldValidationRequireIsInShouldWork() throws Exception {
        // Initialize the database
        templateFieldsRepository.saveAndFlush(templateFields);

        // Get all the templateFieldsList where fieldValidationRequire in DEFAULT_FIELD_VALIDATION_REQUIRE or UPDATED_FIELD_VALIDATION_REQUIRE
        defaultTemplateFieldsShouldBeFound("fieldValidationRequire.in=" + DEFAULT_FIELD_VALIDATION_REQUIRE + "," + UPDATED_FIELD_VALIDATION_REQUIRE);

        // Get all the templateFieldsList where fieldValidationRequire equals to UPDATED_FIELD_VALIDATION_REQUIRE
        defaultTemplateFieldsShouldNotBeFound("fieldValidationRequire.in=" + UPDATED_FIELD_VALIDATION_REQUIRE);
    }

    @Test
    @Transactional
    public void getAllTemplateFieldsByFieldValidationRequireIsNullOrNotNull() throws Exception {
        // Initialize the database
        templateFieldsRepository.saveAndFlush(templateFields);

        // Get all the templateFieldsList where fieldValidationRequire is not null
        defaultTemplateFieldsShouldBeFound("fieldValidationRequire.specified=true");

        // Get all the templateFieldsList where fieldValidationRequire is null
        defaultTemplateFieldsShouldNotBeFound("fieldValidationRequire.specified=false");
    }

    @Test
    @Transactional
    public void getAllTemplateFieldsByFieldValidationRequireIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        templateFieldsRepository.saveAndFlush(templateFields);

        // Get all the templateFieldsList where fieldValidationRequire greater than or equals to DEFAULT_FIELD_VALIDATION_REQUIRE
        defaultTemplateFieldsShouldBeFound("fieldValidationRequire.greaterOrEqualThan=" + DEFAULT_FIELD_VALIDATION_REQUIRE);

        // Get all the templateFieldsList where fieldValidationRequire greater than or equals to UPDATED_FIELD_VALIDATION_REQUIRE
        defaultTemplateFieldsShouldNotBeFound("fieldValidationRequire.greaterOrEqualThan=" + UPDATED_FIELD_VALIDATION_REQUIRE);
    }

    @Test
    @Transactional
    public void getAllTemplateFieldsByFieldValidationRequireIsLessThanSomething() throws Exception {
        // Initialize the database
        templateFieldsRepository.saveAndFlush(templateFields);

        // Get all the templateFieldsList where fieldValidationRequire less than or equals to DEFAULT_FIELD_VALIDATION_REQUIRE
        defaultTemplateFieldsShouldNotBeFound("fieldValidationRequire.lessThan=" + DEFAULT_FIELD_VALIDATION_REQUIRE);

        // Get all the templateFieldsList where fieldValidationRequire less than or equals to UPDATED_FIELD_VALIDATION_REQUIRE
        defaultTemplateFieldsShouldBeFound("fieldValidationRequire.lessThan=" + UPDATED_FIELD_VALIDATION_REQUIRE);
    }


    @Test
    @Transactional
    public void getAllTemplateFieldsByFieldValidationRuleIsEqualToSomething() throws Exception {
        // Initialize the database
        templateFieldsRepository.saveAndFlush(templateFields);

        // Get all the templateFieldsList where fieldValidationRule equals to DEFAULT_FIELD_VALIDATION_RULE
        defaultTemplateFieldsShouldBeFound("fieldValidationRule.equals=" + DEFAULT_FIELD_VALIDATION_RULE);

        // Get all the templateFieldsList where fieldValidationRule equals to UPDATED_FIELD_VALIDATION_RULE
        defaultTemplateFieldsShouldNotBeFound("fieldValidationRule.equals=" + UPDATED_FIELD_VALIDATION_RULE);
    }

    @Test
    @Transactional
    public void getAllTemplateFieldsByFieldValidationRuleIsInShouldWork() throws Exception {
        // Initialize the database
        templateFieldsRepository.saveAndFlush(templateFields);

        // Get all the templateFieldsList where fieldValidationRule in DEFAULT_FIELD_VALIDATION_RULE or UPDATED_FIELD_VALIDATION_RULE
        defaultTemplateFieldsShouldBeFound("fieldValidationRule.in=" + DEFAULT_FIELD_VALIDATION_RULE + "," + UPDATED_FIELD_VALIDATION_RULE);

        // Get all the templateFieldsList where fieldValidationRule equals to UPDATED_FIELD_VALIDATION_RULE
        defaultTemplateFieldsShouldNotBeFound("fieldValidationRule.in=" + UPDATED_FIELD_VALIDATION_RULE);
    }

    @Test
    @Transactional
    public void getAllTemplateFieldsByFieldValidationRuleIsNullOrNotNull() throws Exception {
        // Initialize the database
        templateFieldsRepository.saveAndFlush(templateFields);

        // Get all the templateFieldsList where fieldValidationRule is not null
        defaultTemplateFieldsShouldBeFound("fieldValidationRule.specified=true");

        // Get all the templateFieldsList where fieldValidationRule is null
        defaultTemplateFieldsShouldNotBeFound("fieldValidationRule.specified=false");
    }

    @Test
    @Transactional
    public void getAllTemplateFieldsByInputTemplateIsEqualToSomething() throws Exception {
        // Initialize the database
        InputTemplate inputTemplate = InputTemplateResourceIntTest.createEntity(em);
        em.persist(inputTemplate);
        em.flush();
        templateFields.setInputTemplate(inputTemplate);
        templateFieldsRepository.saveAndFlush(templateFields);
        Long inputTemplateId = inputTemplate.getId();

        // Get all the templateFieldsList where inputTemplate equals to inputTemplateId
        defaultTemplateFieldsShouldBeFound("inputTemplateId.equals=" + inputTemplateId);

        // Get all the templateFieldsList where inputTemplate equals to inputTemplateId + 1
        defaultTemplateFieldsShouldNotBeFound("inputTemplateId.equals=" + (inputTemplateId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultTemplateFieldsShouldBeFound(String filter) throws Exception {
        restTemplateFieldsMockMvc.perform(get("/api/template-fields?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(templateFields.getId().intValue())))
            .andExpect(jsonPath("$.[*].fieldName").value(hasItem(DEFAULT_FIELD_NAME.toString())))
            .andExpect(jsonPath("$.[*].fieldZoneMinX").value(hasItem(DEFAULT_FIELD_ZONE_MIN_X.doubleValue())))
            .andExpect(jsonPath("$.[*].fieldZoneMinY").value(hasItem(DEFAULT_FIELD_ZONE_MIN_Y.doubleValue())))
            .andExpect(jsonPath("$.[*].fieldZoneMaxX").value(hasItem(DEFAULT_FIELD_ZONE_MAX_X.doubleValue())))
            .andExpect(jsonPath("$.[*].fieldZoneMaxY").value(hasItem(DEFAULT_FIELD_ZONE_MAX_Y.doubleValue())))
            .andExpect(jsonPath("$.[*].width").value(hasItem(DEFAULT_WIDTH.doubleValue())))
            .andExpect(jsonPath("$.[*].height").value(hasItem(DEFAULT_HEIGHT.doubleValue())))
            .andExpect(jsonPath("$.[*].sequence").value(hasItem(DEFAULT_SEQUENCE)))
            .andExpect(jsonPath("$.[*].isTemplateIdentifier").value(hasItem(DEFAULT_IS_TEMPLATE_IDENTIFIER)))
            .andExpect(jsonPath("$.[*].isLabel").value(hasItem(DEFAULT_IS_LABEL)))
            .andExpect(jsonPath("$.[*].pageNumebr").value(hasItem(DEFAULT_PAGE_NUMEBR)))
            .andExpect(jsonPath("$.[*].fieldValidationRequire").value(hasItem(DEFAULT_FIELD_VALIDATION_REQUIRE)))
            .andExpect(jsonPath("$.[*].fieldValidationRule").value(hasItem(DEFAULT_FIELD_VALIDATION_RULE.toString())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultTemplateFieldsShouldNotBeFound(String filter) throws Exception {
        restTemplateFieldsMockMvc.perform(get("/api/template-fields?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingTemplateFields() throws Exception {
        // Get the templateFields
        restTemplateFieldsMockMvc.perform(get("/api/template-fields/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTemplateFields() throws Exception {
        // Initialize the database
        templateFieldsRepository.saveAndFlush(templateFields);

        int databaseSizeBeforeUpdate = templateFieldsRepository.findAll().size();

        // Update the templateFields
        TemplateFields updatedTemplateFields = templateFieldsRepository.findById(templateFields.getId()).get();
        // Disconnect from session so that the updates on updatedTemplateFields are not directly saved in db
        em.detach(updatedTemplateFields);
        updatedTemplateFields
            .fieldName(UPDATED_FIELD_NAME)
            .fieldZoneMinX(UPDATED_FIELD_ZONE_MIN_X)
            .fieldZoneMinY(UPDATED_FIELD_ZONE_MIN_Y)
            .fieldZoneMaxX(UPDATED_FIELD_ZONE_MAX_X)
            .fieldZoneMaxY(UPDATED_FIELD_ZONE_MAX_Y)
            .width(UPDATED_WIDTH)
            .height(UPDATED_HEIGHT)
            .sequence(UPDATED_SEQUENCE)
            .isTemplateIdentifier(UPDATED_IS_TEMPLATE_IDENTIFIER)
            .isLabel(UPDATED_IS_LABEL)
            .pageNumebr(UPDATED_PAGE_NUMEBR)
            .fieldValidationRequire(UPDATED_FIELD_VALIDATION_REQUIRE)
            .fieldValidationRule(UPDATED_FIELD_VALIDATION_RULE);
        TemplateFieldsDTO templateFieldsDTO = templateFieldsMapper.toDto(updatedTemplateFields);

        restTemplateFieldsMockMvc.perform(put("/api/template-fields")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(templateFieldsDTO)))
            .andExpect(status().isOk());

        // Validate the TemplateFields in the database
        List<TemplateFields> templateFieldsList = templateFieldsRepository.findAll();
        assertThat(templateFieldsList).hasSize(databaseSizeBeforeUpdate);
        TemplateFields testTemplateFields = templateFieldsList.get(templateFieldsList.size() - 1);
        assertThat(testTemplateFields.getFieldName()).isEqualTo(UPDATED_FIELD_NAME);
        assertThat(testTemplateFields.getFieldZoneMinX()).isEqualTo(UPDATED_FIELD_ZONE_MIN_X);
        assertThat(testTemplateFields.getFieldZoneMinY()).isEqualTo(UPDATED_FIELD_ZONE_MIN_Y);
        assertThat(testTemplateFields.getFieldZoneMaxX()).isEqualTo(UPDATED_FIELD_ZONE_MAX_X);
        assertThat(testTemplateFields.getFieldZoneMaxY()).isEqualTo(UPDATED_FIELD_ZONE_MAX_Y);
        assertThat(testTemplateFields.getWidth()).isEqualTo(UPDATED_WIDTH);
        assertThat(testTemplateFields.getHeight()).isEqualTo(UPDATED_HEIGHT);
        assertThat(testTemplateFields.getSequence()).isEqualTo(UPDATED_SEQUENCE);
        assertThat(testTemplateFields.getIsTemplateIdentifier()).isEqualTo(UPDATED_IS_TEMPLATE_IDENTIFIER);
        assertThat(testTemplateFields.getIsLabel()).isEqualTo(UPDATED_IS_LABEL);
        assertThat(testTemplateFields.getPageNumebr()).isEqualTo(UPDATED_PAGE_NUMEBR);
        assertThat(testTemplateFields.getFieldValidationRequire()).isEqualTo(UPDATED_FIELD_VALIDATION_REQUIRE);
        assertThat(testTemplateFields.getFieldValidationRule()).isEqualTo(UPDATED_FIELD_VALIDATION_RULE);
    }

    @Test
    @Transactional
    public void updateNonExistingTemplateFields() throws Exception {
        int databaseSizeBeforeUpdate = templateFieldsRepository.findAll().size();

        // Create the TemplateFields
        TemplateFieldsDTO templateFieldsDTO = templateFieldsMapper.toDto(templateFields);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTemplateFieldsMockMvc.perform(put("/api/template-fields")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(templateFieldsDTO)))
            .andExpect(status().isBadRequest());

        // Validate the TemplateFields in the database
        List<TemplateFields> templateFieldsList = templateFieldsRepository.findAll();
        assertThat(templateFieldsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteTemplateFields() throws Exception {
        // Initialize the database
        templateFieldsRepository.saveAndFlush(templateFields);

        int databaseSizeBeforeDelete = templateFieldsRepository.findAll().size();

        // Get the templateFields
        restTemplateFieldsMockMvc.perform(delete("/api/template-fields/{id}", templateFields.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<TemplateFields> templateFieldsList = templateFieldsRepository.findAll();
        assertThat(templateFieldsList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TemplateFields.class);
        TemplateFields templateFields1 = new TemplateFields();
        templateFields1.setId(1L);
        TemplateFields templateFields2 = new TemplateFields();
        templateFields2.setId(templateFields1.getId());
        assertThat(templateFields1).isEqualTo(templateFields2);
        templateFields2.setId(2L);
        assertThat(templateFields1).isNotEqualTo(templateFields2);
        templateFields1.setId(null);
        assertThat(templateFields1).isNotEqualTo(templateFields2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(TemplateFieldsDTO.class);
        TemplateFieldsDTO templateFieldsDTO1 = new TemplateFieldsDTO();
        templateFieldsDTO1.setId(1L);
        TemplateFieldsDTO templateFieldsDTO2 = new TemplateFieldsDTO();
        assertThat(templateFieldsDTO1).isNotEqualTo(templateFieldsDTO2);
        templateFieldsDTO2.setId(templateFieldsDTO1.getId());
        assertThat(templateFieldsDTO1).isEqualTo(templateFieldsDTO2);
        templateFieldsDTO2.setId(2L);
        assertThat(templateFieldsDTO1).isNotEqualTo(templateFieldsDTO2);
        templateFieldsDTO1.setId(null);
        assertThat(templateFieldsDTO1).isNotEqualTo(templateFieldsDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(templateFieldsMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(templateFieldsMapper.fromId(null)).isNull();
    }
}
