package com.edatablock.rpa.web.rest;

import com.edatablock.rpa.EdatablockrpamainApp;

import com.edatablock.rpa.domain.InputTemplate;
import com.edatablock.rpa.domain.Client;
import com.edatablock.rpa.repository.InputTemplateRepository;
import com.edatablock.rpa.service.InputTemplateService;
import com.edatablock.rpa.service.dto.InputTemplateDTO;
import com.edatablock.rpa.service.mapper.InputTemplateMapper;
import com.edatablock.rpa.web.rest.errors.ExceptionTranslator;
import com.edatablock.rpa.service.dto.InputTemplateCriteria;
import com.edatablock.rpa.service.InputTemplateQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;


import static com.edatablock.rpa.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the InputTemplateResource REST controller.
 *
 * @see InputTemplateResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = EdatablockrpamainApp.class)
public class InputTemplateResourceIntTest {

    private static final String DEFAULT_TEMPLATE_NAME = "AAAAAAAAAA";
    private static final String UPDATED_TEMPLATE_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_TEMPLATE_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_TEMPLATE_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_TEMPLATE_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_TEMPLATE_TYPE = "BBBBBBBBBB";

    private static final Boolean DEFAULT_IS_STANDARD_TEMPLATE = false;
    private static final Boolean UPDATED_IS_STANDARD_TEMPLATE = true;

    private static final Integer DEFAULT_IS_ACTIVE = 1;
    private static final Integer UPDATED_IS_ACTIVE = 2;

    private static final Instant DEFAULT_CREATE_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATE_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_UPDATE_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_UPDATE_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_TEMPLATE_IDENTIFIER = "AAAAAAAAAA";
    private static final String UPDATED_TEMPLATE_IDENTIFIER = "BBBBBBBBBB";

    private static final Integer DEFAULT_NUMBER_OF_PAGES = 1;
    private static final Integer UPDATED_NUMBER_OF_PAGES = 2;

    private static final String DEFAULT_UPDATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_UPDATED_BY = "BBBBBBBBBB";

    @Autowired
    private InputTemplateRepository inputTemplateRepository;

    @Autowired
    private InputTemplateMapper inputTemplateMapper;
    
    @Autowired
    private InputTemplateService inputTemplateService;

    @Autowired
    private InputTemplateQueryService inputTemplateQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restInputTemplateMockMvc;

    private InputTemplate inputTemplate;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final InputTemplateResource inputTemplateResource = new InputTemplateResource(inputTemplateService, inputTemplateQueryService);
        this.restInputTemplateMockMvc = MockMvcBuilders.standaloneSetup(inputTemplateResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static InputTemplate createEntity(EntityManager em) {
        InputTemplate inputTemplate = new InputTemplate()
            .templateName(DEFAULT_TEMPLATE_NAME)
            .templateDescription(DEFAULT_TEMPLATE_DESCRIPTION)
            .templateType(DEFAULT_TEMPLATE_TYPE)
            .isStandardTemplate(DEFAULT_IS_STANDARD_TEMPLATE)
            .isActive(DEFAULT_IS_ACTIVE)
            .createDate(DEFAULT_CREATE_DATE)
            .createdBy(DEFAULT_CREATED_BY)
            .updateDate(DEFAULT_UPDATE_DATE)
            .templateIdentifier(DEFAULT_TEMPLATE_IDENTIFIER)
            .numberOfPages(DEFAULT_NUMBER_OF_PAGES)
            .updatedBy(DEFAULT_UPDATED_BY);
        return inputTemplate;
    }

    @Before
    public void initTest() {
        inputTemplate = createEntity(em);
    }

    @Test
    @Transactional
    public void createInputTemplate() throws Exception {
        int databaseSizeBeforeCreate = inputTemplateRepository.findAll().size();

        // Create the InputTemplate
        InputTemplateDTO inputTemplateDTO = inputTemplateMapper.toDto(inputTemplate);
        restInputTemplateMockMvc.perform(post("/api/input-templates")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(inputTemplateDTO)))
            .andExpect(status().isCreated());

        // Validate the InputTemplate in the database
        List<InputTemplate> inputTemplateList = inputTemplateRepository.findAll();
        assertThat(inputTemplateList).hasSize(databaseSizeBeforeCreate + 1);
        InputTemplate testInputTemplate = inputTemplateList.get(inputTemplateList.size() - 1);
        assertThat(testInputTemplate.getTemplateName()).isEqualTo(DEFAULT_TEMPLATE_NAME);
        assertThat(testInputTemplate.getTemplateDescription()).isEqualTo(DEFAULT_TEMPLATE_DESCRIPTION);
        assertThat(testInputTemplate.getTemplateType()).isEqualTo(DEFAULT_TEMPLATE_TYPE);
        assertThat(testInputTemplate.isIsStandardTemplate()).isEqualTo(DEFAULT_IS_STANDARD_TEMPLATE);
        assertThat(testInputTemplate.getIsActive()).isEqualTo(DEFAULT_IS_ACTIVE);
        assertThat(testInputTemplate.getCreateDate()).isEqualTo(DEFAULT_CREATE_DATE);
        assertThat(testInputTemplate.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testInputTemplate.getUpdateDate()).isEqualTo(DEFAULT_UPDATE_DATE);
        assertThat(testInputTemplate.getTemplateIdentifier()).isEqualTo(DEFAULT_TEMPLATE_IDENTIFIER);
        assertThat(testInputTemplate.getNumberOfPages()).isEqualTo(DEFAULT_NUMBER_OF_PAGES);
        assertThat(testInputTemplate.getUpdatedBy()).isEqualTo(DEFAULT_UPDATED_BY);
    }

    @Test
    @Transactional
    public void createInputTemplateWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = inputTemplateRepository.findAll().size();

        // Create the InputTemplate with an existing ID
        inputTemplate.setId(1L);
        InputTemplateDTO inputTemplateDTO = inputTemplateMapper.toDto(inputTemplate);

        // An entity with an existing ID cannot be created, so this API call must fail
        restInputTemplateMockMvc.perform(post("/api/input-templates")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(inputTemplateDTO)))
            .andExpect(status().isBadRequest());

        // Validate the InputTemplate in the database
        List<InputTemplate> inputTemplateList = inputTemplateRepository.findAll();
        assertThat(inputTemplateList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkTemplateNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = inputTemplateRepository.findAll().size();
        // set the field null
        inputTemplate.setTemplateName(null);

        // Create the InputTemplate, which fails.
        InputTemplateDTO inputTemplateDTO = inputTemplateMapper.toDto(inputTemplate);

        restInputTemplateMockMvc.perform(post("/api/input-templates")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(inputTemplateDTO)))
            .andExpect(status().isBadRequest());

        List<InputTemplate> inputTemplateList = inputTemplateRepository.findAll();
        assertThat(inputTemplateList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTemplateTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = inputTemplateRepository.findAll().size();
        // set the field null
        inputTemplate.setTemplateType(null);

        // Create the InputTemplate, which fails.
        InputTemplateDTO inputTemplateDTO = inputTemplateMapper.toDto(inputTemplate);

        restInputTemplateMockMvc.perform(post("/api/input-templates")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(inputTemplateDTO)))
            .andExpect(status().isBadRequest());

        List<InputTemplate> inputTemplateList = inputTemplateRepository.findAll();
        assertThat(inputTemplateList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkIsStandardTemplateIsRequired() throws Exception {
        int databaseSizeBeforeTest = inputTemplateRepository.findAll().size();
        // set the field null
        inputTemplate.setIsStandardTemplate(null);

        // Create the InputTemplate, which fails.
        InputTemplateDTO inputTemplateDTO = inputTemplateMapper.toDto(inputTemplate);

        restInputTemplateMockMvc.perform(post("/api/input-templates")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(inputTemplateDTO)))
            .andExpect(status().isBadRequest());

        List<InputTemplate> inputTemplateList = inputTemplateRepository.findAll();
        assertThat(inputTemplateList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllInputTemplates() throws Exception {
        // Initialize the database
        inputTemplateRepository.saveAndFlush(inputTemplate);

        // Get all the inputTemplateList
        restInputTemplateMockMvc.perform(get("/api/input-templates?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(inputTemplate.getId().intValue())))
            .andExpect(jsonPath("$.[*].templateName").value(hasItem(DEFAULT_TEMPLATE_NAME.toString())))
            .andExpect(jsonPath("$.[*].templateDescription").value(hasItem(DEFAULT_TEMPLATE_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].templateType").value(hasItem(DEFAULT_TEMPLATE_TYPE.toString())))
            .andExpect(jsonPath("$.[*].isStandardTemplate").value(hasItem(DEFAULT_IS_STANDARD_TEMPLATE.booleanValue())))
            .andExpect(jsonPath("$.[*].isActive").value(hasItem(DEFAULT_IS_ACTIVE)))
            .andExpect(jsonPath("$.[*].createDate").value(hasItem(DEFAULT_CREATE_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY.toString())))
            .andExpect(jsonPath("$.[*].updateDate").value(hasItem(DEFAULT_UPDATE_DATE.toString())))
            .andExpect(jsonPath("$.[*].templateIdentifier").value(hasItem(DEFAULT_TEMPLATE_IDENTIFIER.toString())))
            .andExpect(jsonPath("$.[*].numberOfPages").value(hasItem(DEFAULT_NUMBER_OF_PAGES)))
            .andExpect(jsonPath("$.[*].updatedBy").value(hasItem(DEFAULT_UPDATED_BY.toString())));
    }
    
    @Test
    @Transactional
    public void getInputTemplate() throws Exception {
        // Initialize the database
        inputTemplateRepository.saveAndFlush(inputTemplate);

        // Get the inputTemplate
        restInputTemplateMockMvc.perform(get("/api/input-templates/{id}", inputTemplate.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(inputTemplate.getId().intValue()))
            .andExpect(jsonPath("$.templateName").value(DEFAULT_TEMPLATE_NAME.toString()))
            .andExpect(jsonPath("$.templateDescription").value(DEFAULT_TEMPLATE_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.templateType").value(DEFAULT_TEMPLATE_TYPE.toString()))
            .andExpect(jsonPath("$.isStandardTemplate").value(DEFAULT_IS_STANDARD_TEMPLATE.booleanValue()))
            .andExpect(jsonPath("$.isActive").value(DEFAULT_IS_ACTIVE))
            .andExpect(jsonPath("$.createDate").value(DEFAULT_CREATE_DATE.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY.toString()))
            .andExpect(jsonPath("$.updateDate").value(DEFAULT_UPDATE_DATE.toString()))
            .andExpect(jsonPath("$.templateIdentifier").value(DEFAULT_TEMPLATE_IDENTIFIER.toString()))
            .andExpect(jsonPath("$.numberOfPages").value(DEFAULT_NUMBER_OF_PAGES))
            .andExpect(jsonPath("$.updatedBy").value(DEFAULT_UPDATED_BY.toString()));
    }

    @Test
    @Transactional
    public void getAllInputTemplatesByTemplateNameIsEqualToSomething() throws Exception {
        // Initialize the database
        inputTemplateRepository.saveAndFlush(inputTemplate);

        // Get all the inputTemplateList where templateName equals to DEFAULT_TEMPLATE_NAME
        defaultInputTemplateShouldBeFound("templateName.equals=" + DEFAULT_TEMPLATE_NAME);

        // Get all the inputTemplateList where templateName equals to UPDATED_TEMPLATE_NAME
        defaultInputTemplateShouldNotBeFound("templateName.equals=" + UPDATED_TEMPLATE_NAME);
    }

    @Test
    @Transactional
    public void getAllInputTemplatesByTemplateNameIsInShouldWork() throws Exception {
        // Initialize the database
        inputTemplateRepository.saveAndFlush(inputTemplate);

        // Get all the inputTemplateList where templateName in DEFAULT_TEMPLATE_NAME or UPDATED_TEMPLATE_NAME
        defaultInputTemplateShouldBeFound("templateName.in=" + DEFAULT_TEMPLATE_NAME + "," + UPDATED_TEMPLATE_NAME);

        // Get all the inputTemplateList where templateName equals to UPDATED_TEMPLATE_NAME
        defaultInputTemplateShouldNotBeFound("templateName.in=" + UPDATED_TEMPLATE_NAME);
    }

    @Test
    @Transactional
    public void getAllInputTemplatesByTemplateNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        inputTemplateRepository.saveAndFlush(inputTemplate);

        // Get all the inputTemplateList where templateName is not null
        defaultInputTemplateShouldBeFound("templateName.specified=true");

        // Get all the inputTemplateList where templateName is null
        defaultInputTemplateShouldNotBeFound("templateName.specified=false");
    }

    @Test
    @Transactional
    public void getAllInputTemplatesByTemplateDescriptionIsEqualToSomething() throws Exception {
        // Initialize the database
        inputTemplateRepository.saveAndFlush(inputTemplate);

        // Get all the inputTemplateList where templateDescription equals to DEFAULT_TEMPLATE_DESCRIPTION
        defaultInputTemplateShouldBeFound("templateDescription.equals=" + DEFAULT_TEMPLATE_DESCRIPTION);

        // Get all the inputTemplateList where templateDescription equals to UPDATED_TEMPLATE_DESCRIPTION
        defaultInputTemplateShouldNotBeFound("templateDescription.equals=" + UPDATED_TEMPLATE_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllInputTemplatesByTemplateDescriptionIsInShouldWork() throws Exception {
        // Initialize the database
        inputTemplateRepository.saveAndFlush(inputTemplate);

        // Get all the inputTemplateList where templateDescription in DEFAULT_TEMPLATE_DESCRIPTION or UPDATED_TEMPLATE_DESCRIPTION
        defaultInputTemplateShouldBeFound("templateDescription.in=" + DEFAULT_TEMPLATE_DESCRIPTION + "," + UPDATED_TEMPLATE_DESCRIPTION);

        // Get all the inputTemplateList where templateDescription equals to UPDATED_TEMPLATE_DESCRIPTION
        defaultInputTemplateShouldNotBeFound("templateDescription.in=" + UPDATED_TEMPLATE_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllInputTemplatesByTemplateDescriptionIsNullOrNotNull() throws Exception {
        // Initialize the database
        inputTemplateRepository.saveAndFlush(inputTemplate);

        // Get all the inputTemplateList where templateDescription is not null
        defaultInputTemplateShouldBeFound("templateDescription.specified=true");

        // Get all the inputTemplateList where templateDescription is null
        defaultInputTemplateShouldNotBeFound("templateDescription.specified=false");
    }

    @Test
    @Transactional
    public void getAllInputTemplatesByTemplateTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        inputTemplateRepository.saveAndFlush(inputTemplate);

        // Get all the inputTemplateList where templateType equals to DEFAULT_TEMPLATE_TYPE
        defaultInputTemplateShouldBeFound("templateType.equals=" + DEFAULT_TEMPLATE_TYPE);

        // Get all the inputTemplateList where templateType equals to UPDATED_TEMPLATE_TYPE
        defaultInputTemplateShouldNotBeFound("templateType.equals=" + UPDATED_TEMPLATE_TYPE);
    }

    @Test
    @Transactional
    public void getAllInputTemplatesByTemplateTypeIsInShouldWork() throws Exception {
        // Initialize the database
        inputTemplateRepository.saveAndFlush(inputTemplate);

        // Get all the inputTemplateList where templateType in DEFAULT_TEMPLATE_TYPE or UPDATED_TEMPLATE_TYPE
        defaultInputTemplateShouldBeFound("templateType.in=" + DEFAULT_TEMPLATE_TYPE + "," + UPDATED_TEMPLATE_TYPE);

        // Get all the inputTemplateList where templateType equals to UPDATED_TEMPLATE_TYPE
        defaultInputTemplateShouldNotBeFound("templateType.in=" + UPDATED_TEMPLATE_TYPE);
    }

    @Test
    @Transactional
    public void getAllInputTemplatesByTemplateTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        inputTemplateRepository.saveAndFlush(inputTemplate);

        // Get all the inputTemplateList where templateType is not null
        defaultInputTemplateShouldBeFound("templateType.specified=true");

        // Get all the inputTemplateList where templateType is null
        defaultInputTemplateShouldNotBeFound("templateType.specified=false");
    }

    @Test
    @Transactional
    public void getAllInputTemplatesByIsStandardTemplateIsEqualToSomething() throws Exception {
        // Initialize the database
        inputTemplateRepository.saveAndFlush(inputTemplate);

        // Get all the inputTemplateList where isStandardTemplate equals to DEFAULT_IS_STANDARD_TEMPLATE
        defaultInputTemplateShouldBeFound("isStandardTemplate.equals=" + DEFAULT_IS_STANDARD_TEMPLATE);

        // Get all the inputTemplateList where isStandardTemplate equals to UPDATED_IS_STANDARD_TEMPLATE
        defaultInputTemplateShouldNotBeFound("isStandardTemplate.equals=" + UPDATED_IS_STANDARD_TEMPLATE);
    }

    @Test
    @Transactional
    public void getAllInputTemplatesByIsStandardTemplateIsInShouldWork() throws Exception {
        // Initialize the database
        inputTemplateRepository.saveAndFlush(inputTemplate);

        // Get all the inputTemplateList where isStandardTemplate in DEFAULT_IS_STANDARD_TEMPLATE or UPDATED_IS_STANDARD_TEMPLATE
        defaultInputTemplateShouldBeFound("isStandardTemplate.in=" + DEFAULT_IS_STANDARD_TEMPLATE + "," + UPDATED_IS_STANDARD_TEMPLATE);

        // Get all the inputTemplateList where isStandardTemplate equals to UPDATED_IS_STANDARD_TEMPLATE
        defaultInputTemplateShouldNotBeFound("isStandardTemplate.in=" + UPDATED_IS_STANDARD_TEMPLATE);
    }

    @Test
    @Transactional
    public void getAllInputTemplatesByIsStandardTemplateIsNullOrNotNull() throws Exception {
        // Initialize the database
        inputTemplateRepository.saveAndFlush(inputTemplate);

        // Get all the inputTemplateList where isStandardTemplate is not null
        defaultInputTemplateShouldBeFound("isStandardTemplate.specified=true");

        // Get all the inputTemplateList where isStandardTemplate is null
        defaultInputTemplateShouldNotBeFound("isStandardTemplate.specified=false");
    }

    @Test
    @Transactional
    public void getAllInputTemplatesByIsActiveIsEqualToSomething() throws Exception {
        // Initialize the database
        inputTemplateRepository.saveAndFlush(inputTemplate);

        // Get all the inputTemplateList where isActive equals to DEFAULT_IS_ACTIVE
        defaultInputTemplateShouldBeFound("isActive.equals=" + DEFAULT_IS_ACTIVE);

        // Get all the inputTemplateList where isActive equals to UPDATED_IS_ACTIVE
        defaultInputTemplateShouldNotBeFound("isActive.equals=" + UPDATED_IS_ACTIVE);
    }

    @Test
    @Transactional
    public void getAllInputTemplatesByIsActiveIsInShouldWork() throws Exception {
        // Initialize the database
        inputTemplateRepository.saveAndFlush(inputTemplate);

        // Get all the inputTemplateList where isActive in DEFAULT_IS_ACTIVE or UPDATED_IS_ACTIVE
        defaultInputTemplateShouldBeFound("isActive.in=" + DEFAULT_IS_ACTIVE + "," + UPDATED_IS_ACTIVE);

        // Get all the inputTemplateList where isActive equals to UPDATED_IS_ACTIVE
        defaultInputTemplateShouldNotBeFound("isActive.in=" + UPDATED_IS_ACTIVE);
    }

    @Test
    @Transactional
    public void getAllInputTemplatesByIsActiveIsNullOrNotNull() throws Exception {
        // Initialize the database
        inputTemplateRepository.saveAndFlush(inputTemplate);

        // Get all the inputTemplateList where isActive is not null
        defaultInputTemplateShouldBeFound("isActive.specified=true");

        // Get all the inputTemplateList where isActive is null
        defaultInputTemplateShouldNotBeFound("isActive.specified=false");
    }

    @Test
    @Transactional
    public void getAllInputTemplatesByIsActiveIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        inputTemplateRepository.saveAndFlush(inputTemplate);

        // Get all the inputTemplateList where isActive greater than or equals to DEFAULT_IS_ACTIVE
        defaultInputTemplateShouldBeFound("isActive.greaterOrEqualThan=" + DEFAULT_IS_ACTIVE);

        // Get all the inputTemplateList where isActive greater than or equals to UPDATED_IS_ACTIVE
        defaultInputTemplateShouldNotBeFound("isActive.greaterOrEqualThan=" + UPDATED_IS_ACTIVE);
    }

    @Test
    @Transactional
    public void getAllInputTemplatesByIsActiveIsLessThanSomething() throws Exception {
        // Initialize the database
        inputTemplateRepository.saveAndFlush(inputTemplate);

        // Get all the inputTemplateList where isActive less than or equals to DEFAULT_IS_ACTIVE
        defaultInputTemplateShouldNotBeFound("isActive.lessThan=" + DEFAULT_IS_ACTIVE);

        // Get all the inputTemplateList where isActive less than or equals to UPDATED_IS_ACTIVE
        defaultInputTemplateShouldBeFound("isActive.lessThan=" + UPDATED_IS_ACTIVE);
    }


    @Test
    @Transactional
    public void getAllInputTemplatesByCreateDateIsEqualToSomething() throws Exception {
        // Initialize the database
        inputTemplateRepository.saveAndFlush(inputTemplate);

        // Get all the inputTemplateList where createDate equals to DEFAULT_CREATE_DATE
        defaultInputTemplateShouldBeFound("createDate.equals=" + DEFAULT_CREATE_DATE);

        // Get all the inputTemplateList where createDate equals to UPDATED_CREATE_DATE
        defaultInputTemplateShouldNotBeFound("createDate.equals=" + UPDATED_CREATE_DATE);
    }

    @Test
    @Transactional
    public void getAllInputTemplatesByCreateDateIsInShouldWork() throws Exception {
        // Initialize the database
        inputTemplateRepository.saveAndFlush(inputTemplate);

        // Get all the inputTemplateList where createDate in DEFAULT_CREATE_DATE or UPDATED_CREATE_DATE
        defaultInputTemplateShouldBeFound("createDate.in=" + DEFAULT_CREATE_DATE + "," + UPDATED_CREATE_DATE);

        // Get all the inputTemplateList where createDate equals to UPDATED_CREATE_DATE
        defaultInputTemplateShouldNotBeFound("createDate.in=" + UPDATED_CREATE_DATE);
    }

    @Test
    @Transactional
    public void getAllInputTemplatesByCreateDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        inputTemplateRepository.saveAndFlush(inputTemplate);

        // Get all the inputTemplateList where createDate is not null
        defaultInputTemplateShouldBeFound("createDate.specified=true");

        // Get all the inputTemplateList where createDate is null
        defaultInputTemplateShouldNotBeFound("createDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllInputTemplatesByCreatedByIsEqualToSomething() throws Exception {
        // Initialize the database
        inputTemplateRepository.saveAndFlush(inputTemplate);

        // Get all the inputTemplateList where createdBy equals to DEFAULT_CREATED_BY
        defaultInputTemplateShouldBeFound("createdBy.equals=" + DEFAULT_CREATED_BY);

        // Get all the inputTemplateList where createdBy equals to UPDATED_CREATED_BY
        defaultInputTemplateShouldNotBeFound("createdBy.equals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllInputTemplatesByCreatedByIsInShouldWork() throws Exception {
        // Initialize the database
        inputTemplateRepository.saveAndFlush(inputTemplate);

        // Get all the inputTemplateList where createdBy in DEFAULT_CREATED_BY or UPDATED_CREATED_BY
        defaultInputTemplateShouldBeFound("createdBy.in=" + DEFAULT_CREATED_BY + "," + UPDATED_CREATED_BY);

        // Get all the inputTemplateList where createdBy equals to UPDATED_CREATED_BY
        defaultInputTemplateShouldNotBeFound("createdBy.in=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllInputTemplatesByCreatedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        inputTemplateRepository.saveAndFlush(inputTemplate);

        // Get all the inputTemplateList where createdBy is not null
        defaultInputTemplateShouldBeFound("createdBy.specified=true");

        // Get all the inputTemplateList where createdBy is null
        defaultInputTemplateShouldNotBeFound("createdBy.specified=false");
    }

    @Test
    @Transactional
    public void getAllInputTemplatesByUpdateDateIsEqualToSomething() throws Exception {
        // Initialize the database
        inputTemplateRepository.saveAndFlush(inputTemplate);

        // Get all the inputTemplateList where updateDate equals to DEFAULT_UPDATE_DATE
        defaultInputTemplateShouldBeFound("updateDate.equals=" + DEFAULT_UPDATE_DATE);

        // Get all the inputTemplateList where updateDate equals to UPDATED_UPDATE_DATE
        defaultInputTemplateShouldNotBeFound("updateDate.equals=" + UPDATED_UPDATE_DATE);
    }

    @Test
    @Transactional
    public void getAllInputTemplatesByUpdateDateIsInShouldWork() throws Exception {
        // Initialize the database
        inputTemplateRepository.saveAndFlush(inputTemplate);

        // Get all the inputTemplateList where updateDate in DEFAULT_UPDATE_DATE or UPDATED_UPDATE_DATE
        defaultInputTemplateShouldBeFound("updateDate.in=" + DEFAULT_UPDATE_DATE + "," + UPDATED_UPDATE_DATE);

        // Get all the inputTemplateList where updateDate equals to UPDATED_UPDATE_DATE
        defaultInputTemplateShouldNotBeFound("updateDate.in=" + UPDATED_UPDATE_DATE);
    }

    @Test
    @Transactional
    public void getAllInputTemplatesByUpdateDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        inputTemplateRepository.saveAndFlush(inputTemplate);

        // Get all the inputTemplateList where updateDate is not null
        defaultInputTemplateShouldBeFound("updateDate.specified=true");

        // Get all the inputTemplateList where updateDate is null
        defaultInputTemplateShouldNotBeFound("updateDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllInputTemplatesByTemplateIdentifierIsEqualToSomething() throws Exception {
        // Initialize the database
        inputTemplateRepository.saveAndFlush(inputTemplate);

        // Get all the inputTemplateList where templateIdentifier equals to DEFAULT_TEMPLATE_IDENTIFIER
        defaultInputTemplateShouldBeFound("templateIdentifier.equals=" + DEFAULT_TEMPLATE_IDENTIFIER);

        // Get all the inputTemplateList where templateIdentifier equals to UPDATED_TEMPLATE_IDENTIFIER
        defaultInputTemplateShouldNotBeFound("templateIdentifier.equals=" + UPDATED_TEMPLATE_IDENTIFIER);
    }

    @Test
    @Transactional
    public void getAllInputTemplatesByTemplateIdentifierIsInShouldWork() throws Exception {
        // Initialize the database
        inputTemplateRepository.saveAndFlush(inputTemplate);

        // Get all the inputTemplateList where templateIdentifier in DEFAULT_TEMPLATE_IDENTIFIER or UPDATED_TEMPLATE_IDENTIFIER
        defaultInputTemplateShouldBeFound("templateIdentifier.in=" + DEFAULT_TEMPLATE_IDENTIFIER + "," + UPDATED_TEMPLATE_IDENTIFIER);

        // Get all the inputTemplateList where templateIdentifier equals to UPDATED_TEMPLATE_IDENTIFIER
        defaultInputTemplateShouldNotBeFound("templateIdentifier.in=" + UPDATED_TEMPLATE_IDENTIFIER);
    }

    @Test
    @Transactional
    public void getAllInputTemplatesByTemplateIdentifierIsNullOrNotNull() throws Exception {
        // Initialize the database
        inputTemplateRepository.saveAndFlush(inputTemplate);

        // Get all the inputTemplateList where templateIdentifier is not null
        defaultInputTemplateShouldBeFound("templateIdentifier.specified=true");

        // Get all the inputTemplateList where templateIdentifier is null
        defaultInputTemplateShouldNotBeFound("templateIdentifier.specified=false");
    }

    @Test
    @Transactional
    public void getAllInputTemplatesByNumberOfPagesIsEqualToSomething() throws Exception {
        // Initialize the database
        inputTemplateRepository.saveAndFlush(inputTemplate);

        // Get all the inputTemplateList where numberOfPages equals to DEFAULT_NUMBER_OF_PAGES
        defaultInputTemplateShouldBeFound("numberOfPages.equals=" + DEFAULT_NUMBER_OF_PAGES);

        // Get all the inputTemplateList where numberOfPages equals to UPDATED_NUMBER_OF_PAGES
        defaultInputTemplateShouldNotBeFound("numberOfPages.equals=" + UPDATED_NUMBER_OF_PAGES);
    }

    @Test
    @Transactional
    public void getAllInputTemplatesByNumberOfPagesIsInShouldWork() throws Exception {
        // Initialize the database
        inputTemplateRepository.saveAndFlush(inputTemplate);

        // Get all the inputTemplateList where numberOfPages in DEFAULT_NUMBER_OF_PAGES or UPDATED_NUMBER_OF_PAGES
        defaultInputTemplateShouldBeFound("numberOfPages.in=" + DEFAULT_NUMBER_OF_PAGES + "," + UPDATED_NUMBER_OF_PAGES);

        // Get all the inputTemplateList where numberOfPages equals to UPDATED_NUMBER_OF_PAGES
        defaultInputTemplateShouldNotBeFound("numberOfPages.in=" + UPDATED_NUMBER_OF_PAGES);
    }

    @Test
    @Transactional
    public void getAllInputTemplatesByNumberOfPagesIsNullOrNotNull() throws Exception {
        // Initialize the database
        inputTemplateRepository.saveAndFlush(inputTemplate);

        // Get all the inputTemplateList where numberOfPages is not null
        defaultInputTemplateShouldBeFound("numberOfPages.specified=true");

        // Get all the inputTemplateList where numberOfPages is null
        defaultInputTemplateShouldNotBeFound("numberOfPages.specified=false");
    }

    @Test
    @Transactional
    public void getAllInputTemplatesByNumberOfPagesIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        inputTemplateRepository.saveAndFlush(inputTemplate);

        // Get all the inputTemplateList where numberOfPages greater than or equals to DEFAULT_NUMBER_OF_PAGES
        defaultInputTemplateShouldBeFound("numberOfPages.greaterOrEqualThan=" + DEFAULT_NUMBER_OF_PAGES);

        // Get all the inputTemplateList where numberOfPages greater than or equals to UPDATED_NUMBER_OF_PAGES
        defaultInputTemplateShouldNotBeFound("numberOfPages.greaterOrEqualThan=" + UPDATED_NUMBER_OF_PAGES);
    }

    @Test
    @Transactional
    public void getAllInputTemplatesByNumberOfPagesIsLessThanSomething() throws Exception {
        // Initialize the database
        inputTemplateRepository.saveAndFlush(inputTemplate);

        // Get all the inputTemplateList where numberOfPages less than or equals to DEFAULT_NUMBER_OF_PAGES
        defaultInputTemplateShouldNotBeFound("numberOfPages.lessThan=" + DEFAULT_NUMBER_OF_PAGES);

        // Get all the inputTemplateList where numberOfPages less than or equals to UPDATED_NUMBER_OF_PAGES
        defaultInputTemplateShouldBeFound("numberOfPages.lessThan=" + UPDATED_NUMBER_OF_PAGES);
    }


    @Test
    @Transactional
    public void getAllInputTemplatesByUpdatedByIsEqualToSomething() throws Exception {
        // Initialize the database
        inputTemplateRepository.saveAndFlush(inputTemplate);

        // Get all the inputTemplateList where updatedBy equals to DEFAULT_UPDATED_BY
        defaultInputTemplateShouldBeFound("updatedBy.equals=" + DEFAULT_UPDATED_BY);

        // Get all the inputTemplateList where updatedBy equals to UPDATED_UPDATED_BY
        defaultInputTemplateShouldNotBeFound("updatedBy.equals=" + UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    public void getAllInputTemplatesByUpdatedByIsInShouldWork() throws Exception {
        // Initialize the database
        inputTemplateRepository.saveAndFlush(inputTemplate);

        // Get all the inputTemplateList where updatedBy in DEFAULT_UPDATED_BY or UPDATED_UPDATED_BY
        defaultInputTemplateShouldBeFound("updatedBy.in=" + DEFAULT_UPDATED_BY + "," + UPDATED_UPDATED_BY);

        // Get all the inputTemplateList where updatedBy equals to UPDATED_UPDATED_BY
        defaultInputTemplateShouldNotBeFound("updatedBy.in=" + UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    public void getAllInputTemplatesByUpdatedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        inputTemplateRepository.saveAndFlush(inputTemplate);

        // Get all the inputTemplateList where updatedBy is not null
        defaultInputTemplateShouldBeFound("updatedBy.specified=true");

        // Get all the inputTemplateList where updatedBy is null
        defaultInputTemplateShouldNotBeFound("updatedBy.specified=false");
    }

    @Test
    @Transactional
    public void getAllInputTemplatesByClientIsEqualToSomething() throws Exception {
        // Initialize the database
        Client client = ClientResourceIntTest.createEntity(em);
        em.persist(client);
        em.flush();
        inputTemplate.setClient(client);
        inputTemplateRepository.saveAndFlush(inputTemplate);
        Long clientId = client.getId();

        // Get all the inputTemplateList where client equals to clientId
        defaultInputTemplateShouldBeFound("clientId.equals=" + clientId);

        // Get all the inputTemplateList where client equals to clientId + 1
        defaultInputTemplateShouldNotBeFound("clientId.equals=" + (clientId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultInputTemplateShouldBeFound(String filter) throws Exception {
        restInputTemplateMockMvc.perform(get("/api/input-templates?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(inputTemplate.getId().intValue())))
            .andExpect(jsonPath("$.[*].templateName").value(hasItem(DEFAULT_TEMPLATE_NAME.toString())))
            .andExpect(jsonPath("$.[*].templateDescription").value(hasItem(DEFAULT_TEMPLATE_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].templateType").value(hasItem(DEFAULT_TEMPLATE_TYPE.toString())))
            .andExpect(jsonPath("$.[*].isStandardTemplate").value(hasItem(DEFAULT_IS_STANDARD_TEMPLATE.booleanValue())))
            .andExpect(jsonPath("$.[*].isActive").value(hasItem(DEFAULT_IS_ACTIVE)))
            .andExpect(jsonPath("$.[*].createDate").value(hasItem(DEFAULT_CREATE_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY.toString())))
            .andExpect(jsonPath("$.[*].updateDate").value(hasItem(DEFAULT_UPDATE_DATE.toString())))
            .andExpect(jsonPath("$.[*].templateIdentifier").value(hasItem(DEFAULT_TEMPLATE_IDENTIFIER.toString())))
            .andExpect(jsonPath("$.[*].numberOfPages").value(hasItem(DEFAULT_NUMBER_OF_PAGES)))
            .andExpect(jsonPath("$.[*].updatedBy").value(hasItem(DEFAULT_UPDATED_BY.toString())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultInputTemplateShouldNotBeFound(String filter) throws Exception {
        restInputTemplateMockMvc.perform(get("/api/input-templates?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingInputTemplate() throws Exception {
        // Get the inputTemplate
        restInputTemplateMockMvc.perform(get("/api/input-templates/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateInputTemplate() throws Exception {
        // Initialize the database
        inputTemplateRepository.saveAndFlush(inputTemplate);

        int databaseSizeBeforeUpdate = inputTemplateRepository.findAll().size();

        // Update the inputTemplate
        InputTemplate updatedInputTemplate = inputTemplateRepository.findById(inputTemplate.getId()).get();
        // Disconnect from session so that the updates on updatedInputTemplate are not directly saved in db
        em.detach(updatedInputTemplate);
        updatedInputTemplate
            .templateName(UPDATED_TEMPLATE_NAME)
            .templateDescription(UPDATED_TEMPLATE_DESCRIPTION)
            .templateType(UPDATED_TEMPLATE_TYPE)
            .isStandardTemplate(UPDATED_IS_STANDARD_TEMPLATE)
            .isActive(UPDATED_IS_ACTIVE)
            .createDate(UPDATED_CREATE_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .updateDate(UPDATED_UPDATE_DATE)
            .templateIdentifier(UPDATED_TEMPLATE_IDENTIFIER)
            .numberOfPages(UPDATED_NUMBER_OF_PAGES)
            .updatedBy(UPDATED_UPDATED_BY);
        InputTemplateDTO inputTemplateDTO = inputTemplateMapper.toDto(updatedInputTemplate);

        restInputTemplateMockMvc.perform(put("/api/input-templates")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(inputTemplateDTO)))
            .andExpect(status().isOk());

        // Validate the InputTemplate in the database
        List<InputTemplate> inputTemplateList = inputTemplateRepository.findAll();
        assertThat(inputTemplateList).hasSize(databaseSizeBeforeUpdate);
        InputTemplate testInputTemplate = inputTemplateList.get(inputTemplateList.size() - 1);
        assertThat(testInputTemplate.getTemplateName()).isEqualTo(UPDATED_TEMPLATE_NAME);
        assertThat(testInputTemplate.getTemplateDescription()).isEqualTo(UPDATED_TEMPLATE_DESCRIPTION);
        assertThat(testInputTemplate.getTemplateType()).isEqualTo(UPDATED_TEMPLATE_TYPE);
        assertThat(testInputTemplate.isIsStandardTemplate()).isEqualTo(UPDATED_IS_STANDARD_TEMPLATE);
        assertThat(testInputTemplate.getIsActive()).isEqualTo(UPDATED_IS_ACTIVE);
        assertThat(testInputTemplate.getCreateDate()).isEqualTo(UPDATED_CREATE_DATE);
        assertThat(testInputTemplate.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testInputTemplate.getUpdateDate()).isEqualTo(UPDATED_UPDATE_DATE);
        assertThat(testInputTemplate.getTemplateIdentifier()).isEqualTo(UPDATED_TEMPLATE_IDENTIFIER);
        assertThat(testInputTemplate.getNumberOfPages()).isEqualTo(UPDATED_NUMBER_OF_PAGES);
        assertThat(testInputTemplate.getUpdatedBy()).isEqualTo(UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    public void updateNonExistingInputTemplate() throws Exception {
        int databaseSizeBeforeUpdate = inputTemplateRepository.findAll().size();

        // Create the InputTemplate
        InputTemplateDTO inputTemplateDTO = inputTemplateMapper.toDto(inputTemplate);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restInputTemplateMockMvc.perform(put("/api/input-templates")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(inputTemplateDTO)))
            .andExpect(status().isBadRequest());

        // Validate the InputTemplate in the database
        List<InputTemplate> inputTemplateList = inputTemplateRepository.findAll();
        assertThat(inputTemplateList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteInputTemplate() throws Exception {
        // Initialize the database
        inputTemplateRepository.saveAndFlush(inputTemplate);

        int databaseSizeBeforeDelete = inputTemplateRepository.findAll().size();

        // Get the inputTemplate
        restInputTemplateMockMvc.perform(delete("/api/input-templates/{id}", inputTemplate.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<InputTemplate> inputTemplateList = inputTemplateRepository.findAll();
        assertThat(inputTemplateList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(InputTemplate.class);
        InputTemplate inputTemplate1 = new InputTemplate();
        inputTemplate1.setId(1L);
        InputTemplate inputTemplate2 = new InputTemplate();
        inputTemplate2.setId(inputTemplate1.getId());
        assertThat(inputTemplate1).isEqualTo(inputTemplate2);
        inputTemplate2.setId(2L);
        assertThat(inputTemplate1).isNotEqualTo(inputTemplate2);
        inputTemplate1.setId(null);
        assertThat(inputTemplate1).isNotEqualTo(inputTemplate2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(InputTemplateDTO.class);
        InputTemplateDTO inputTemplateDTO1 = new InputTemplateDTO();
        inputTemplateDTO1.setId(1L);
        InputTemplateDTO inputTemplateDTO2 = new InputTemplateDTO();
        assertThat(inputTemplateDTO1).isNotEqualTo(inputTemplateDTO2);
        inputTemplateDTO2.setId(inputTemplateDTO1.getId());
        assertThat(inputTemplateDTO1).isEqualTo(inputTemplateDTO2);
        inputTemplateDTO2.setId(2L);
        assertThat(inputTemplateDTO1).isNotEqualTo(inputTemplateDTO2);
        inputTemplateDTO1.setId(null);
        assertThat(inputTemplateDTO1).isNotEqualTo(inputTemplateDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(inputTemplateMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(inputTemplateMapper.fromId(null)).isNull();
    }
}
