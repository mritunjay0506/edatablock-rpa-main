package com.edatablock.rpa.web.rest;

import com.edatablock.rpa.EdatablockrpamainApp;

import com.edatablock.rpa.domain.FileToFtp;
import com.edatablock.rpa.domain.ClientDataOcr;
import com.edatablock.rpa.repository.FileToFtpRepository;
import com.edatablock.rpa.service.FileToFtpService;
import com.edatablock.rpa.service.dto.FileToFtpDTO;
import com.edatablock.rpa.service.mapper.FileToFtpMapper;
import com.edatablock.rpa.web.rest.errors.ExceptionTranslator;
import com.edatablock.rpa.service.dto.FileToFtpCriteria;
import com.edatablock.rpa.service.FileToFtpQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;


import static com.edatablock.rpa.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the FileToFtpResource REST controller.
 *
 * @see FileToFtpResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = EdatablockrpamainApp.class)
public class FileToFtpResourceIntTest {

    private static final String DEFAULT_MESSAGE_ID = "AAAAAAAAAA";
    private static final String UPDATED_MESSAGE_ID = "BBBBBBBBBB";

    private static final String DEFAULT_CLIENT_EMAIL_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_CLIENT_EMAIL_ADDRESS = "BBBBBBBBBB";

    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    private static final String DEFAULT_FILE_NAME = "AAAAAAAAAA";
    private static final String UPDATED_FILE_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_FILE_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_FILE_TYPE = "BBBBBBBBBB";

    private static final Integer DEFAULT_TRANSACTION_ID = 1;
    private static final Integer UPDATED_TRANSACTION_ID = 2;

    private static final Instant DEFAULT_CREATE_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATE_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    @Autowired
    private FileToFtpRepository fileToFtpRepository;

    @Autowired
    private FileToFtpMapper fileToFtpMapper;
    
    @Autowired
    private FileToFtpService fileToFtpService;

    @Autowired
    private FileToFtpQueryService fileToFtpQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restFileToFtpMockMvc;

    private FileToFtp fileToFtp;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final FileToFtpResource fileToFtpResource = new FileToFtpResource(fileToFtpService, fileToFtpQueryService);
        this.restFileToFtpMockMvc = MockMvcBuilders.standaloneSetup(fileToFtpResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FileToFtp createEntity(EntityManager em) {
        FileToFtp fileToFtp = new FileToFtp()
            .messageId(DEFAULT_MESSAGE_ID)
            .clientEmailAddress(DEFAULT_CLIENT_EMAIL_ADDRESS)
            .status(DEFAULT_STATUS)
            .fileName(DEFAULT_FILE_NAME)
            .fileType(DEFAULT_FILE_TYPE)
            .transactionId(DEFAULT_TRANSACTION_ID)
            .createDate(DEFAULT_CREATE_DATE);
        return fileToFtp;
    }

    @Before
    public void initTest() {
        fileToFtp = createEntity(em);
    }

    @Test
    @Transactional
    public void createFileToFtp() throws Exception {
        int databaseSizeBeforeCreate = fileToFtpRepository.findAll().size();

        // Create the FileToFtp
        FileToFtpDTO fileToFtpDTO = fileToFtpMapper.toDto(fileToFtp);
        restFileToFtpMockMvc.perform(post("/api/file-to-ftps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(fileToFtpDTO)))
            .andExpect(status().isCreated());

        // Validate the FileToFtp in the database
        List<FileToFtp> fileToFtpList = fileToFtpRepository.findAll();
        assertThat(fileToFtpList).hasSize(databaseSizeBeforeCreate + 1);
        FileToFtp testFileToFtp = fileToFtpList.get(fileToFtpList.size() - 1);
        assertThat(testFileToFtp.getMessageId()).isEqualTo(DEFAULT_MESSAGE_ID);
        assertThat(testFileToFtp.getClientEmailAddress()).isEqualTo(DEFAULT_CLIENT_EMAIL_ADDRESS);
        assertThat(testFileToFtp.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testFileToFtp.getFileName()).isEqualTo(DEFAULT_FILE_NAME);
        assertThat(testFileToFtp.getFileType()).isEqualTo(DEFAULT_FILE_TYPE);
        assertThat(testFileToFtp.getTransactionId()).isEqualTo(DEFAULT_TRANSACTION_ID);
        assertThat(testFileToFtp.getCreateDate()).isEqualTo(DEFAULT_CREATE_DATE);
    }

    @Test
    @Transactional
    public void createFileToFtpWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = fileToFtpRepository.findAll().size();

        // Create the FileToFtp with an existing ID
        fileToFtp.setId(1L);
        FileToFtpDTO fileToFtpDTO = fileToFtpMapper.toDto(fileToFtp);

        // An entity with an existing ID cannot be created, so this API call must fail
        restFileToFtpMockMvc.perform(post("/api/file-to-ftps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(fileToFtpDTO)))
            .andExpect(status().isBadRequest());

        // Validate the FileToFtp in the database
        List<FileToFtp> fileToFtpList = fileToFtpRepository.findAll();
        assertThat(fileToFtpList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllFileToFtps() throws Exception {
        // Initialize the database
        fileToFtpRepository.saveAndFlush(fileToFtp);

        // Get all the fileToFtpList
        restFileToFtpMockMvc.perform(get("/api/file-to-ftps?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(fileToFtp.getId().intValue())))
            .andExpect(jsonPath("$.[*].messageId").value(hasItem(DEFAULT_MESSAGE_ID.toString())))
            .andExpect(jsonPath("$.[*].clientEmailAddress").value(hasItem(DEFAULT_CLIENT_EMAIL_ADDRESS.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].fileName").value(hasItem(DEFAULT_FILE_NAME.toString())))
            .andExpect(jsonPath("$.[*].fileType").value(hasItem(DEFAULT_FILE_TYPE.toString())))
            .andExpect(jsonPath("$.[*].transactionId").value(hasItem(DEFAULT_TRANSACTION_ID)))
            .andExpect(jsonPath("$.[*].createDate").value(hasItem(DEFAULT_CREATE_DATE.toString())));
    }
    
    @Test
    @Transactional
    public void getFileToFtp() throws Exception {
        // Initialize the database
        fileToFtpRepository.saveAndFlush(fileToFtp);

        // Get the fileToFtp
        restFileToFtpMockMvc.perform(get("/api/file-to-ftps/{id}", fileToFtp.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(fileToFtp.getId().intValue()))
            .andExpect(jsonPath("$.messageId").value(DEFAULT_MESSAGE_ID.toString()))
            .andExpect(jsonPath("$.clientEmailAddress").value(DEFAULT_CLIENT_EMAIL_ADDRESS.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.fileName").value(DEFAULT_FILE_NAME.toString()))
            .andExpect(jsonPath("$.fileType").value(DEFAULT_FILE_TYPE.toString()))
            .andExpect(jsonPath("$.transactionId").value(DEFAULT_TRANSACTION_ID))
            .andExpect(jsonPath("$.createDate").value(DEFAULT_CREATE_DATE.toString()));
    }

    @Test
    @Transactional
    public void getAllFileToFtpsByMessageIdIsEqualToSomething() throws Exception {
        // Initialize the database
        fileToFtpRepository.saveAndFlush(fileToFtp);

        // Get all the fileToFtpList where messageId equals to DEFAULT_MESSAGE_ID
        defaultFileToFtpShouldBeFound("messageId.equals=" + DEFAULT_MESSAGE_ID);

        // Get all the fileToFtpList where messageId equals to UPDATED_MESSAGE_ID
        defaultFileToFtpShouldNotBeFound("messageId.equals=" + UPDATED_MESSAGE_ID);
    }

    @Test
    @Transactional
    public void getAllFileToFtpsByMessageIdIsInShouldWork() throws Exception {
        // Initialize the database
        fileToFtpRepository.saveAndFlush(fileToFtp);

        // Get all the fileToFtpList where messageId in DEFAULT_MESSAGE_ID or UPDATED_MESSAGE_ID
        defaultFileToFtpShouldBeFound("messageId.in=" + DEFAULT_MESSAGE_ID + "," + UPDATED_MESSAGE_ID);

        // Get all the fileToFtpList where messageId equals to UPDATED_MESSAGE_ID
        defaultFileToFtpShouldNotBeFound("messageId.in=" + UPDATED_MESSAGE_ID);
    }

    @Test
    @Transactional
    public void getAllFileToFtpsByMessageIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        fileToFtpRepository.saveAndFlush(fileToFtp);

        // Get all the fileToFtpList where messageId is not null
        defaultFileToFtpShouldBeFound("messageId.specified=true");

        // Get all the fileToFtpList where messageId is null
        defaultFileToFtpShouldNotBeFound("messageId.specified=false");
    }

    @Test
    @Transactional
    public void getAllFileToFtpsByClientEmailAddressIsEqualToSomething() throws Exception {
        // Initialize the database
        fileToFtpRepository.saveAndFlush(fileToFtp);

        // Get all the fileToFtpList where clientEmailAddress equals to DEFAULT_CLIENT_EMAIL_ADDRESS
        defaultFileToFtpShouldBeFound("clientEmailAddress.equals=" + DEFAULT_CLIENT_EMAIL_ADDRESS);

        // Get all the fileToFtpList where clientEmailAddress equals to UPDATED_CLIENT_EMAIL_ADDRESS
        defaultFileToFtpShouldNotBeFound("clientEmailAddress.equals=" + UPDATED_CLIENT_EMAIL_ADDRESS);
    }

    @Test
    @Transactional
    public void getAllFileToFtpsByClientEmailAddressIsInShouldWork() throws Exception {
        // Initialize the database
        fileToFtpRepository.saveAndFlush(fileToFtp);

        // Get all the fileToFtpList where clientEmailAddress in DEFAULT_CLIENT_EMAIL_ADDRESS or UPDATED_CLIENT_EMAIL_ADDRESS
        defaultFileToFtpShouldBeFound("clientEmailAddress.in=" + DEFAULT_CLIENT_EMAIL_ADDRESS + "," + UPDATED_CLIENT_EMAIL_ADDRESS);

        // Get all the fileToFtpList where clientEmailAddress equals to UPDATED_CLIENT_EMAIL_ADDRESS
        defaultFileToFtpShouldNotBeFound("clientEmailAddress.in=" + UPDATED_CLIENT_EMAIL_ADDRESS);
    }

    @Test
    @Transactional
    public void getAllFileToFtpsByClientEmailAddressIsNullOrNotNull() throws Exception {
        // Initialize the database
        fileToFtpRepository.saveAndFlush(fileToFtp);

        // Get all the fileToFtpList where clientEmailAddress is not null
        defaultFileToFtpShouldBeFound("clientEmailAddress.specified=true");

        // Get all the fileToFtpList where clientEmailAddress is null
        defaultFileToFtpShouldNotBeFound("clientEmailAddress.specified=false");
    }

    @Test
    @Transactional
    public void getAllFileToFtpsByStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        fileToFtpRepository.saveAndFlush(fileToFtp);

        // Get all the fileToFtpList where status equals to DEFAULT_STATUS
        defaultFileToFtpShouldBeFound("status.equals=" + DEFAULT_STATUS);

        // Get all the fileToFtpList where status equals to UPDATED_STATUS
        defaultFileToFtpShouldNotBeFound("status.equals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllFileToFtpsByStatusIsInShouldWork() throws Exception {
        // Initialize the database
        fileToFtpRepository.saveAndFlush(fileToFtp);

        // Get all the fileToFtpList where status in DEFAULT_STATUS or UPDATED_STATUS
        defaultFileToFtpShouldBeFound("status.in=" + DEFAULT_STATUS + "," + UPDATED_STATUS);

        // Get all the fileToFtpList where status equals to UPDATED_STATUS
        defaultFileToFtpShouldNotBeFound("status.in=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllFileToFtpsByStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        fileToFtpRepository.saveAndFlush(fileToFtp);

        // Get all the fileToFtpList where status is not null
        defaultFileToFtpShouldBeFound("status.specified=true");

        // Get all the fileToFtpList where status is null
        defaultFileToFtpShouldNotBeFound("status.specified=false");
    }

    @Test
    @Transactional
    public void getAllFileToFtpsByFileNameIsEqualToSomething() throws Exception {
        // Initialize the database
        fileToFtpRepository.saveAndFlush(fileToFtp);

        // Get all the fileToFtpList where fileName equals to DEFAULT_FILE_NAME
        defaultFileToFtpShouldBeFound("fileName.equals=" + DEFAULT_FILE_NAME);

        // Get all the fileToFtpList where fileName equals to UPDATED_FILE_NAME
        defaultFileToFtpShouldNotBeFound("fileName.equals=" + UPDATED_FILE_NAME);
    }

    @Test
    @Transactional
    public void getAllFileToFtpsByFileNameIsInShouldWork() throws Exception {
        // Initialize the database
        fileToFtpRepository.saveAndFlush(fileToFtp);

        // Get all the fileToFtpList where fileName in DEFAULT_FILE_NAME or UPDATED_FILE_NAME
        defaultFileToFtpShouldBeFound("fileName.in=" + DEFAULT_FILE_NAME + "," + UPDATED_FILE_NAME);

        // Get all the fileToFtpList where fileName equals to UPDATED_FILE_NAME
        defaultFileToFtpShouldNotBeFound("fileName.in=" + UPDATED_FILE_NAME);
    }

    @Test
    @Transactional
    public void getAllFileToFtpsByFileNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        fileToFtpRepository.saveAndFlush(fileToFtp);

        // Get all the fileToFtpList where fileName is not null
        defaultFileToFtpShouldBeFound("fileName.specified=true");

        // Get all the fileToFtpList where fileName is null
        defaultFileToFtpShouldNotBeFound("fileName.specified=false");
    }

    @Test
    @Transactional
    public void getAllFileToFtpsByFileTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        fileToFtpRepository.saveAndFlush(fileToFtp);

        // Get all the fileToFtpList where fileType equals to DEFAULT_FILE_TYPE
        defaultFileToFtpShouldBeFound("fileType.equals=" + DEFAULT_FILE_TYPE);

        // Get all the fileToFtpList where fileType equals to UPDATED_FILE_TYPE
        defaultFileToFtpShouldNotBeFound("fileType.equals=" + UPDATED_FILE_TYPE);
    }

    @Test
    @Transactional
    public void getAllFileToFtpsByFileTypeIsInShouldWork() throws Exception {
        // Initialize the database
        fileToFtpRepository.saveAndFlush(fileToFtp);

        // Get all the fileToFtpList where fileType in DEFAULT_FILE_TYPE or UPDATED_FILE_TYPE
        defaultFileToFtpShouldBeFound("fileType.in=" + DEFAULT_FILE_TYPE + "," + UPDATED_FILE_TYPE);

        // Get all the fileToFtpList where fileType equals to UPDATED_FILE_TYPE
        defaultFileToFtpShouldNotBeFound("fileType.in=" + UPDATED_FILE_TYPE);
    }

    @Test
    @Transactional
    public void getAllFileToFtpsByFileTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        fileToFtpRepository.saveAndFlush(fileToFtp);

        // Get all the fileToFtpList where fileType is not null
        defaultFileToFtpShouldBeFound("fileType.specified=true");

        // Get all the fileToFtpList where fileType is null
        defaultFileToFtpShouldNotBeFound("fileType.specified=false");
    }

    @Test
    @Transactional
    public void getAllFileToFtpsByTransactionIdIsEqualToSomething() throws Exception {
        // Initialize the database
        fileToFtpRepository.saveAndFlush(fileToFtp);

        // Get all the fileToFtpList where transactionId equals to DEFAULT_TRANSACTION_ID
        defaultFileToFtpShouldBeFound("transactionId.equals=" + DEFAULT_TRANSACTION_ID);

        // Get all the fileToFtpList where transactionId equals to UPDATED_TRANSACTION_ID
        defaultFileToFtpShouldNotBeFound("transactionId.equals=" + UPDATED_TRANSACTION_ID);
    }

    @Test
    @Transactional
    public void getAllFileToFtpsByTransactionIdIsInShouldWork() throws Exception {
        // Initialize the database
        fileToFtpRepository.saveAndFlush(fileToFtp);

        // Get all the fileToFtpList where transactionId in DEFAULT_TRANSACTION_ID or UPDATED_TRANSACTION_ID
        defaultFileToFtpShouldBeFound("transactionId.in=" + DEFAULT_TRANSACTION_ID + "," + UPDATED_TRANSACTION_ID);

        // Get all the fileToFtpList where transactionId equals to UPDATED_TRANSACTION_ID
        defaultFileToFtpShouldNotBeFound("transactionId.in=" + UPDATED_TRANSACTION_ID);
    }

    @Test
    @Transactional
    public void getAllFileToFtpsByTransactionIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        fileToFtpRepository.saveAndFlush(fileToFtp);

        // Get all the fileToFtpList where transactionId is not null
        defaultFileToFtpShouldBeFound("transactionId.specified=true");

        // Get all the fileToFtpList where transactionId is null
        defaultFileToFtpShouldNotBeFound("transactionId.specified=false");
    }

    @Test
    @Transactional
    public void getAllFileToFtpsByTransactionIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        fileToFtpRepository.saveAndFlush(fileToFtp);

        // Get all the fileToFtpList where transactionId greater than or equals to DEFAULT_TRANSACTION_ID
        defaultFileToFtpShouldBeFound("transactionId.greaterOrEqualThan=" + DEFAULT_TRANSACTION_ID);

        // Get all the fileToFtpList where transactionId greater than or equals to UPDATED_TRANSACTION_ID
        defaultFileToFtpShouldNotBeFound("transactionId.greaterOrEqualThan=" + UPDATED_TRANSACTION_ID);
    }

    @Test
    @Transactional
    public void getAllFileToFtpsByTransactionIdIsLessThanSomething() throws Exception {
        // Initialize the database
        fileToFtpRepository.saveAndFlush(fileToFtp);

        // Get all the fileToFtpList where transactionId less than or equals to DEFAULT_TRANSACTION_ID
        defaultFileToFtpShouldNotBeFound("transactionId.lessThan=" + DEFAULT_TRANSACTION_ID);

        // Get all the fileToFtpList where transactionId less than or equals to UPDATED_TRANSACTION_ID
        defaultFileToFtpShouldBeFound("transactionId.lessThan=" + UPDATED_TRANSACTION_ID);
    }


    @Test
    @Transactional
    public void getAllFileToFtpsByCreateDateIsEqualToSomething() throws Exception {
        // Initialize the database
        fileToFtpRepository.saveAndFlush(fileToFtp);

        // Get all the fileToFtpList where createDate equals to DEFAULT_CREATE_DATE
        defaultFileToFtpShouldBeFound("createDate.equals=" + DEFAULT_CREATE_DATE);

        // Get all the fileToFtpList where createDate equals to UPDATED_CREATE_DATE
        defaultFileToFtpShouldNotBeFound("createDate.equals=" + UPDATED_CREATE_DATE);
    }

    @Test
    @Transactional
    public void getAllFileToFtpsByCreateDateIsInShouldWork() throws Exception {
        // Initialize the database
        fileToFtpRepository.saveAndFlush(fileToFtp);

        // Get all the fileToFtpList where createDate in DEFAULT_CREATE_DATE or UPDATED_CREATE_DATE
        defaultFileToFtpShouldBeFound("createDate.in=" + DEFAULT_CREATE_DATE + "," + UPDATED_CREATE_DATE);

        // Get all the fileToFtpList where createDate equals to UPDATED_CREATE_DATE
        defaultFileToFtpShouldNotBeFound("createDate.in=" + UPDATED_CREATE_DATE);
    }

    @Test
    @Transactional
    public void getAllFileToFtpsByCreateDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        fileToFtpRepository.saveAndFlush(fileToFtp);

        // Get all the fileToFtpList where createDate is not null
        defaultFileToFtpShouldBeFound("createDate.specified=true");

        // Get all the fileToFtpList where createDate is null
        defaultFileToFtpShouldNotBeFound("createDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllFileToFtpsByClientDataOcrIsEqualToSomething() throws Exception {
        // Initialize the database
        ClientDataOcr clientDataOcr = ClientDataOcrResourceIntTest.createEntity(em);
        em.persist(clientDataOcr);
        em.flush();
        fileToFtp.setClientDataOcr(clientDataOcr);
        fileToFtpRepository.saveAndFlush(fileToFtp);
        Long clientDataOcrId = clientDataOcr.getId();

        // Get all the fileToFtpList where clientDataOcr equals to clientDataOcrId
        defaultFileToFtpShouldBeFound("clientDataOcrId.equals=" + clientDataOcrId);

        // Get all the fileToFtpList where clientDataOcr equals to clientDataOcrId + 1
        defaultFileToFtpShouldNotBeFound("clientDataOcrId.equals=" + (clientDataOcrId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultFileToFtpShouldBeFound(String filter) throws Exception {
        restFileToFtpMockMvc.perform(get("/api/file-to-ftps?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(fileToFtp.getId().intValue())))
            .andExpect(jsonPath("$.[*].messageId").value(hasItem(DEFAULT_MESSAGE_ID.toString())))
            .andExpect(jsonPath("$.[*].clientEmailAddress").value(hasItem(DEFAULT_CLIENT_EMAIL_ADDRESS.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].fileName").value(hasItem(DEFAULT_FILE_NAME.toString())))
            .andExpect(jsonPath("$.[*].fileType").value(hasItem(DEFAULT_FILE_TYPE.toString())))
            .andExpect(jsonPath("$.[*].transactionId").value(hasItem(DEFAULT_TRANSACTION_ID)))
            .andExpect(jsonPath("$.[*].createDate").value(hasItem(DEFAULT_CREATE_DATE.toString())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultFileToFtpShouldNotBeFound(String filter) throws Exception {
        restFileToFtpMockMvc.perform(get("/api/file-to-ftps?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingFileToFtp() throws Exception {
        // Get the fileToFtp
        restFileToFtpMockMvc.perform(get("/api/file-to-ftps/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateFileToFtp() throws Exception {
        // Initialize the database
        fileToFtpRepository.saveAndFlush(fileToFtp);

        int databaseSizeBeforeUpdate = fileToFtpRepository.findAll().size();

        // Update the fileToFtp
        FileToFtp updatedFileToFtp = fileToFtpRepository.findById(fileToFtp.getId()).get();
        // Disconnect from session so that the updates on updatedFileToFtp are not directly saved in db
        em.detach(updatedFileToFtp);
        updatedFileToFtp
            .messageId(UPDATED_MESSAGE_ID)
            .clientEmailAddress(UPDATED_CLIENT_EMAIL_ADDRESS)
            .status(UPDATED_STATUS)
            .fileName(UPDATED_FILE_NAME)
            .fileType(UPDATED_FILE_TYPE)
            .transactionId(UPDATED_TRANSACTION_ID)
            .createDate(UPDATED_CREATE_DATE);
        FileToFtpDTO fileToFtpDTO = fileToFtpMapper.toDto(updatedFileToFtp);

        restFileToFtpMockMvc.perform(put("/api/file-to-ftps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(fileToFtpDTO)))
            .andExpect(status().isOk());

        // Validate the FileToFtp in the database
        List<FileToFtp> fileToFtpList = fileToFtpRepository.findAll();
        assertThat(fileToFtpList).hasSize(databaseSizeBeforeUpdate);
        FileToFtp testFileToFtp = fileToFtpList.get(fileToFtpList.size() - 1);
        assertThat(testFileToFtp.getMessageId()).isEqualTo(UPDATED_MESSAGE_ID);
        assertThat(testFileToFtp.getClientEmailAddress()).isEqualTo(UPDATED_CLIENT_EMAIL_ADDRESS);
        assertThat(testFileToFtp.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testFileToFtp.getFileName()).isEqualTo(UPDATED_FILE_NAME);
        assertThat(testFileToFtp.getFileType()).isEqualTo(UPDATED_FILE_TYPE);
        assertThat(testFileToFtp.getTransactionId()).isEqualTo(UPDATED_TRANSACTION_ID);
        assertThat(testFileToFtp.getCreateDate()).isEqualTo(UPDATED_CREATE_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingFileToFtp() throws Exception {
        int databaseSizeBeforeUpdate = fileToFtpRepository.findAll().size();

        // Create the FileToFtp
        FileToFtpDTO fileToFtpDTO = fileToFtpMapper.toDto(fileToFtp);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFileToFtpMockMvc.perform(put("/api/file-to-ftps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(fileToFtpDTO)))
            .andExpect(status().isBadRequest());

        // Validate the FileToFtp in the database
        List<FileToFtp> fileToFtpList = fileToFtpRepository.findAll();
        assertThat(fileToFtpList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteFileToFtp() throws Exception {
        // Initialize the database
        fileToFtpRepository.saveAndFlush(fileToFtp);

        int databaseSizeBeforeDelete = fileToFtpRepository.findAll().size();

        // Get the fileToFtp
        restFileToFtpMockMvc.perform(delete("/api/file-to-ftps/{id}", fileToFtp.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<FileToFtp> fileToFtpList = fileToFtpRepository.findAll();
        assertThat(fileToFtpList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(FileToFtp.class);
        FileToFtp fileToFtp1 = new FileToFtp();
        fileToFtp1.setId(1L);
        FileToFtp fileToFtp2 = new FileToFtp();
        fileToFtp2.setId(fileToFtp1.getId());
        assertThat(fileToFtp1).isEqualTo(fileToFtp2);
        fileToFtp2.setId(2L);
        assertThat(fileToFtp1).isNotEqualTo(fileToFtp2);
        fileToFtp1.setId(null);
        assertThat(fileToFtp1).isNotEqualTo(fileToFtp2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(FileToFtpDTO.class);
        FileToFtpDTO fileToFtpDTO1 = new FileToFtpDTO();
        fileToFtpDTO1.setId(1L);
        FileToFtpDTO fileToFtpDTO2 = new FileToFtpDTO();
        assertThat(fileToFtpDTO1).isNotEqualTo(fileToFtpDTO2);
        fileToFtpDTO2.setId(fileToFtpDTO1.getId());
        assertThat(fileToFtpDTO1).isEqualTo(fileToFtpDTO2);
        fileToFtpDTO2.setId(2L);
        assertThat(fileToFtpDTO1).isNotEqualTo(fileToFtpDTO2);
        fileToFtpDTO1.setId(null);
        assertThat(fileToFtpDTO1).isNotEqualTo(fileToFtpDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(fileToFtpMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(fileToFtpMapper.fromId(null)).isNull();
    }
}
