package com.edatablock.rpa.web.rest;

import com.edatablock.rpa.EdatablockrpamainApp;

import com.edatablock.rpa.domain.EmailMessages;
import com.edatablock.rpa.domain.EmailAttachment;
import com.edatablock.rpa.domain.Client;
import com.edatablock.rpa.repository.EmailMessagesRepository;
import com.edatablock.rpa.service.EmailMessagesService;
import com.edatablock.rpa.service.dto.EmailMessagesDTO;
import com.edatablock.rpa.service.mapper.EmailMessagesMapper;
import com.edatablock.rpa.web.rest.errors.ExceptionTranslator;
import com.edatablock.rpa.service.dto.EmailMessagesCriteria;
import com.edatablock.rpa.service.EmailMessagesQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;


import static com.edatablock.rpa.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the EmailMessagesResource REST controller.
 *
 * @see EmailMessagesResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = EdatablockrpamainApp.class)
public class EmailMessagesResourceIntTest {

    private static final String DEFAULT_MESSAGE_ID = "AAAAAAAAAA";
    private static final String UPDATED_MESSAGE_ID = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL_SUBJECT = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL_SUBJECT = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL_BODY = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL_BODY = "BBBBBBBBBB";

    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    private static final String DEFAULT_CLIENT_EMAIL_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_CLIENT_EMAIL_ADDRESS = "BBBBBBBBBB";

    private static final String DEFAULT_RECEIVE_FROM = "AAAAAAAAAA";
    private static final String UPDATED_RECEIVE_FROM = "BBBBBBBBBB";

    private static final Instant DEFAULT_RECEIVED_TIME = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_RECEIVED_TIME = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_NUMBER_OF_ATTACHMENTS = 1;
    private static final Integer UPDATED_NUMBER_OF_ATTACHMENTS = 2;

    private static final String DEFAULT_ATTACHMENTS = "AAAAAAAAAA";
    private static final String UPDATED_ATTACHMENTS = "BBBBBBBBBB";

    @Autowired
    private EmailMessagesRepository emailMessagesRepository;

    @Autowired
    private EmailMessagesMapper emailMessagesMapper;
    
    @Autowired
    private EmailMessagesService emailMessagesService;

    @Autowired
    private EmailMessagesQueryService emailMessagesQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restEmailMessagesMockMvc;

    private EmailMessages emailMessages;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final EmailMessagesResource emailMessagesResource = new EmailMessagesResource(emailMessagesService, emailMessagesQueryService);
        this.restEmailMessagesMockMvc = MockMvcBuilders.standaloneSetup(emailMessagesResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static EmailMessages createEntity(EntityManager em) {
        EmailMessages emailMessages = new EmailMessages()
            .messageId(DEFAULT_MESSAGE_ID)
            .emailSubject(DEFAULT_EMAIL_SUBJECT)
            .emailBody(DEFAULT_EMAIL_BODY)
            .status(DEFAULT_STATUS)
            .clientEmailAddress(DEFAULT_CLIENT_EMAIL_ADDRESS)
            .receiveFrom(DEFAULT_RECEIVE_FROM)
            .receivedTime(DEFAULT_RECEIVED_TIME)
            .numberOfAttachments(DEFAULT_NUMBER_OF_ATTACHMENTS)
            .attachments(DEFAULT_ATTACHMENTS);
        return emailMessages;
    }

    @Before
    public void initTest() {
        emailMessages = createEntity(em);
    }

    @Test
    @Transactional
    public void createEmailMessages() throws Exception {
        int databaseSizeBeforeCreate = emailMessagesRepository.findAll().size();

        // Create the EmailMessages
        EmailMessagesDTO emailMessagesDTO = emailMessagesMapper.toDto(emailMessages);
        restEmailMessagesMockMvc.perform(post("/api/email-messages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(emailMessagesDTO)))
            .andExpect(status().isCreated());

        // Validate the EmailMessages in the database
        List<EmailMessages> emailMessagesList = emailMessagesRepository.findAll();
        assertThat(emailMessagesList).hasSize(databaseSizeBeforeCreate + 1);
        EmailMessages testEmailMessages = emailMessagesList.get(emailMessagesList.size() - 1);
        assertThat(testEmailMessages.getMessageId()).isEqualTo(DEFAULT_MESSAGE_ID);
        assertThat(testEmailMessages.getEmailSubject()).isEqualTo(DEFAULT_EMAIL_SUBJECT);
        assertThat(testEmailMessages.getEmailBody()).isEqualTo(DEFAULT_EMAIL_BODY);
        assertThat(testEmailMessages.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testEmailMessages.getClientEmailAddress()).isEqualTo(DEFAULT_CLIENT_EMAIL_ADDRESS);
        assertThat(testEmailMessages.getReceiveFrom()).isEqualTo(DEFAULT_RECEIVE_FROM);
        assertThat(testEmailMessages.getReceivedTime()).isEqualTo(DEFAULT_RECEIVED_TIME);
        assertThat(testEmailMessages.getNumberOfAttachments()).isEqualTo(DEFAULT_NUMBER_OF_ATTACHMENTS);
        assertThat(testEmailMessages.getAttachments()).isEqualTo(DEFAULT_ATTACHMENTS);
    }

    @Test
    @Transactional
    public void createEmailMessagesWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = emailMessagesRepository.findAll().size();

        // Create the EmailMessages with an existing ID
        emailMessages.setId(1L);
        EmailMessagesDTO emailMessagesDTO = emailMessagesMapper.toDto(emailMessages);

        // An entity with an existing ID cannot be created, so this API call must fail
        restEmailMessagesMockMvc.perform(post("/api/email-messages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(emailMessagesDTO)))
            .andExpect(status().isBadRequest());

        // Validate the EmailMessages in the database
        List<EmailMessages> emailMessagesList = emailMessagesRepository.findAll();
        assertThat(emailMessagesList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkMessageIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = emailMessagesRepository.findAll().size();
        // set the field null
        emailMessages.setMessageId(null);

        // Create the EmailMessages, which fails.
        EmailMessagesDTO emailMessagesDTO = emailMessagesMapper.toDto(emailMessages);

        restEmailMessagesMockMvc.perform(post("/api/email-messages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(emailMessagesDTO)))
            .andExpect(status().isBadRequest());

        List<EmailMessages> emailMessagesList = emailMessagesRepository.findAll();
        assertThat(emailMessagesList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEmailSubjectIsRequired() throws Exception {
        int databaseSizeBeforeTest = emailMessagesRepository.findAll().size();
        // set the field null
        emailMessages.setEmailSubject(null);

        // Create the EmailMessages, which fails.
        EmailMessagesDTO emailMessagesDTO = emailMessagesMapper.toDto(emailMessages);

        restEmailMessagesMockMvc.perform(post("/api/email-messages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(emailMessagesDTO)))
            .andExpect(status().isBadRequest());

        List<EmailMessages> emailMessagesList = emailMessagesRepository.findAll();
        assertThat(emailMessagesList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkReceiveFromIsRequired() throws Exception {
        int databaseSizeBeforeTest = emailMessagesRepository.findAll().size();
        // set the field null
        emailMessages.setReceiveFrom(null);

        // Create the EmailMessages, which fails.
        EmailMessagesDTO emailMessagesDTO = emailMessagesMapper.toDto(emailMessages);

        restEmailMessagesMockMvc.perform(post("/api/email-messages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(emailMessagesDTO)))
            .andExpect(status().isBadRequest());

        List<EmailMessages> emailMessagesList = emailMessagesRepository.findAll();
        assertThat(emailMessagesList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkReceivedTimeIsRequired() throws Exception {
        int databaseSizeBeforeTest = emailMessagesRepository.findAll().size();
        // set the field null
        emailMessages.setReceivedTime(null);

        // Create the EmailMessages, which fails.
        EmailMessagesDTO emailMessagesDTO = emailMessagesMapper.toDto(emailMessages);

        restEmailMessagesMockMvc.perform(post("/api/email-messages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(emailMessagesDTO)))
            .andExpect(status().isBadRequest());

        List<EmailMessages> emailMessagesList = emailMessagesRepository.findAll();
        assertThat(emailMessagesList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNumberOfAttachmentsIsRequired() throws Exception {
        int databaseSizeBeforeTest = emailMessagesRepository.findAll().size();
        // set the field null
        emailMessages.setNumberOfAttachments(null);

        // Create the EmailMessages, which fails.
        EmailMessagesDTO emailMessagesDTO = emailMessagesMapper.toDto(emailMessages);

        restEmailMessagesMockMvc.perform(post("/api/email-messages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(emailMessagesDTO)))
            .andExpect(status().isBadRequest());

        List<EmailMessages> emailMessagesList = emailMessagesRepository.findAll();
        assertThat(emailMessagesList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkAttachmentsIsRequired() throws Exception {
        int databaseSizeBeforeTest = emailMessagesRepository.findAll().size();
        // set the field null
        emailMessages.setAttachments(null);

        // Create the EmailMessages, which fails.
        EmailMessagesDTO emailMessagesDTO = emailMessagesMapper.toDto(emailMessages);

        restEmailMessagesMockMvc.perform(post("/api/email-messages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(emailMessagesDTO)))
            .andExpect(status().isBadRequest());

        List<EmailMessages> emailMessagesList = emailMessagesRepository.findAll();
        assertThat(emailMessagesList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllEmailMessages() throws Exception {
        // Initialize the database
        emailMessagesRepository.saveAndFlush(emailMessages);

        // Get all the emailMessagesList
        restEmailMessagesMockMvc.perform(get("/api/email-messages?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(emailMessages.getId().intValue())))
            .andExpect(jsonPath("$.[*].messageId").value(hasItem(DEFAULT_MESSAGE_ID.toString())))
            .andExpect(jsonPath("$.[*].emailSubject").value(hasItem(DEFAULT_EMAIL_SUBJECT.toString())))
            .andExpect(jsonPath("$.[*].emailBody").value(hasItem(DEFAULT_EMAIL_BODY.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].clientEmailAddress").value(hasItem(DEFAULT_CLIENT_EMAIL_ADDRESS.toString())))
            .andExpect(jsonPath("$.[*].receiveFrom").value(hasItem(DEFAULT_RECEIVE_FROM.toString())))
            .andExpect(jsonPath("$.[*].receivedTime").value(hasItem(DEFAULT_RECEIVED_TIME.toString())))
            .andExpect(jsonPath("$.[*].numberOfAttachments").value(hasItem(DEFAULT_NUMBER_OF_ATTACHMENTS)))
            .andExpect(jsonPath("$.[*].attachments").value(hasItem(DEFAULT_ATTACHMENTS.toString())));
    }
    
    @Test
    @Transactional
    public void getEmailMessages() throws Exception {
        // Initialize the database
        emailMessagesRepository.saveAndFlush(emailMessages);

        // Get the emailMessages
        restEmailMessagesMockMvc.perform(get("/api/email-messages/{id}", emailMessages.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(emailMessages.getId().intValue()))
            .andExpect(jsonPath("$.messageId").value(DEFAULT_MESSAGE_ID.toString()))
            .andExpect(jsonPath("$.emailSubject").value(DEFAULT_EMAIL_SUBJECT.toString()))
            .andExpect(jsonPath("$.emailBody").value(DEFAULT_EMAIL_BODY.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.clientEmailAddress").value(DEFAULT_CLIENT_EMAIL_ADDRESS.toString()))
            .andExpect(jsonPath("$.receiveFrom").value(DEFAULT_RECEIVE_FROM.toString()))
            .andExpect(jsonPath("$.receivedTime").value(DEFAULT_RECEIVED_TIME.toString()))
            .andExpect(jsonPath("$.numberOfAttachments").value(DEFAULT_NUMBER_OF_ATTACHMENTS))
            .andExpect(jsonPath("$.attachments").value(DEFAULT_ATTACHMENTS.toString()));
    }

    @Test
    @Transactional
    public void getAllEmailMessagesByMessageIdIsEqualToSomething() throws Exception {
        // Initialize the database
        emailMessagesRepository.saveAndFlush(emailMessages);

        // Get all the emailMessagesList where messageId equals to DEFAULT_MESSAGE_ID
        defaultEmailMessagesShouldBeFound("messageId.equals=" + DEFAULT_MESSAGE_ID);

        // Get all the emailMessagesList where messageId equals to UPDATED_MESSAGE_ID
        defaultEmailMessagesShouldNotBeFound("messageId.equals=" + UPDATED_MESSAGE_ID);
    }

    @Test
    @Transactional
    public void getAllEmailMessagesByMessageIdIsInShouldWork() throws Exception {
        // Initialize the database
        emailMessagesRepository.saveAndFlush(emailMessages);

        // Get all the emailMessagesList where messageId in DEFAULT_MESSAGE_ID or UPDATED_MESSAGE_ID
        defaultEmailMessagesShouldBeFound("messageId.in=" + DEFAULT_MESSAGE_ID + "," + UPDATED_MESSAGE_ID);

        // Get all the emailMessagesList where messageId equals to UPDATED_MESSAGE_ID
        defaultEmailMessagesShouldNotBeFound("messageId.in=" + UPDATED_MESSAGE_ID);
    }

    @Test
    @Transactional
    public void getAllEmailMessagesByMessageIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        emailMessagesRepository.saveAndFlush(emailMessages);

        // Get all the emailMessagesList where messageId is not null
        defaultEmailMessagesShouldBeFound("messageId.specified=true");

        // Get all the emailMessagesList where messageId is null
        defaultEmailMessagesShouldNotBeFound("messageId.specified=false");
    }

    @Test
    @Transactional
    public void getAllEmailMessagesByEmailSubjectIsEqualToSomething() throws Exception {
        // Initialize the database
        emailMessagesRepository.saveAndFlush(emailMessages);

        // Get all the emailMessagesList where emailSubject equals to DEFAULT_EMAIL_SUBJECT
        defaultEmailMessagesShouldBeFound("emailSubject.equals=" + DEFAULT_EMAIL_SUBJECT);

        // Get all the emailMessagesList where emailSubject equals to UPDATED_EMAIL_SUBJECT
        defaultEmailMessagesShouldNotBeFound("emailSubject.equals=" + UPDATED_EMAIL_SUBJECT);
    }

    @Test
    @Transactional
    public void getAllEmailMessagesByEmailSubjectIsInShouldWork() throws Exception {
        // Initialize the database
        emailMessagesRepository.saveAndFlush(emailMessages);

        // Get all the emailMessagesList where emailSubject in DEFAULT_EMAIL_SUBJECT or UPDATED_EMAIL_SUBJECT
        defaultEmailMessagesShouldBeFound("emailSubject.in=" + DEFAULT_EMAIL_SUBJECT + "," + UPDATED_EMAIL_SUBJECT);

        // Get all the emailMessagesList where emailSubject equals to UPDATED_EMAIL_SUBJECT
        defaultEmailMessagesShouldNotBeFound("emailSubject.in=" + UPDATED_EMAIL_SUBJECT);
    }

    @Test
    @Transactional
    public void getAllEmailMessagesByEmailSubjectIsNullOrNotNull() throws Exception {
        // Initialize the database
        emailMessagesRepository.saveAndFlush(emailMessages);

        // Get all the emailMessagesList where emailSubject is not null
        defaultEmailMessagesShouldBeFound("emailSubject.specified=true");

        // Get all the emailMessagesList where emailSubject is null
        defaultEmailMessagesShouldNotBeFound("emailSubject.specified=false");
    }

    @Test
    @Transactional
    public void getAllEmailMessagesByEmailBodyIsEqualToSomething() throws Exception {
        // Initialize the database
        emailMessagesRepository.saveAndFlush(emailMessages);

        // Get all the emailMessagesList where emailBody equals to DEFAULT_EMAIL_BODY
        defaultEmailMessagesShouldBeFound("emailBody.equals=" + DEFAULT_EMAIL_BODY);

        // Get all the emailMessagesList where emailBody equals to UPDATED_EMAIL_BODY
        defaultEmailMessagesShouldNotBeFound("emailBody.equals=" + UPDATED_EMAIL_BODY);
    }

    @Test
    @Transactional
    public void getAllEmailMessagesByEmailBodyIsInShouldWork() throws Exception {
        // Initialize the database
        emailMessagesRepository.saveAndFlush(emailMessages);

        // Get all the emailMessagesList where emailBody in DEFAULT_EMAIL_BODY or UPDATED_EMAIL_BODY
        defaultEmailMessagesShouldBeFound("emailBody.in=" + DEFAULT_EMAIL_BODY + "," + UPDATED_EMAIL_BODY);

        // Get all the emailMessagesList where emailBody equals to UPDATED_EMAIL_BODY
        defaultEmailMessagesShouldNotBeFound("emailBody.in=" + UPDATED_EMAIL_BODY);
    }

    @Test
    @Transactional
    public void getAllEmailMessagesByEmailBodyIsNullOrNotNull() throws Exception {
        // Initialize the database
        emailMessagesRepository.saveAndFlush(emailMessages);

        // Get all the emailMessagesList where emailBody is not null
        defaultEmailMessagesShouldBeFound("emailBody.specified=true");

        // Get all the emailMessagesList where emailBody is null
        defaultEmailMessagesShouldNotBeFound("emailBody.specified=false");
    }

    @Test
    @Transactional
    public void getAllEmailMessagesByStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        emailMessagesRepository.saveAndFlush(emailMessages);

        // Get all the emailMessagesList where status equals to DEFAULT_STATUS
        defaultEmailMessagesShouldBeFound("status.equals=" + DEFAULT_STATUS);

        // Get all the emailMessagesList where status equals to UPDATED_STATUS
        defaultEmailMessagesShouldNotBeFound("status.equals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllEmailMessagesByStatusIsInShouldWork() throws Exception {
        // Initialize the database
        emailMessagesRepository.saveAndFlush(emailMessages);

        // Get all the emailMessagesList where status in DEFAULT_STATUS or UPDATED_STATUS
        defaultEmailMessagesShouldBeFound("status.in=" + DEFAULT_STATUS + "," + UPDATED_STATUS);

        // Get all the emailMessagesList where status equals to UPDATED_STATUS
        defaultEmailMessagesShouldNotBeFound("status.in=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllEmailMessagesByStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        emailMessagesRepository.saveAndFlush(emailMessages);

        // Get all the emailMessagesList where status is not null
        defaultEmailMessagesShouldBeFound("status.specified=true");

        // Get all the emailMessagesList where status is null
        defaultEmailMessagesShouldNotBeFound("status.specified=false");
    }

    @Test
    @Transactional
    public void getAllEmailMessagesByClientEmailAddressIsEqualToSomething() throws Exception {
        // Initialize the database
        emailMessagesRepository.saveAndFlush(emailMessages);

        // Get all the emailMessagesList where clientEmailAddress equals to DEFAULT_CLIENT_EMAIL_ADDRESS
        defaultEmailMessagesShouldBeFound("clientEmailAddress.equals=" + DEFAULT_CLIENT_EMAIL_ADDRESS);

        // Get all the emailMessagesList where clientEmailAddress equals to UPDATED_CLIENT_EMAIL_ADDRESS
        defaultEmailMessagesShouldNotBeFound("clientEmailAddress.equals=" + UPDATED_CLIENT_EMAIL_ADDRESS);
    }

    @Test
    @Transactional
    public void getAllEmailMessagesByClientEmailAddressIsInShouldWork() throws Exception {
        // Initialize the database
        emailMessagesRepository.saveAndFlush(emailMessages);

        // Get all the emailMessagesList where clientEmailAddress in DEFAULT_CLIENT_EMAIL_ADDRESS or UPDATED_CLIENT_EMAIL_ADDRESS
        defaultEmailMessagesShouldBeFound("clientEmailAddress.in=" + DEFAULT_CLIENT_EMAIL_ADDRESS + "," + UPDATED_CLIENT_EMAIL_ADDRESS);

        // Get all the emailMessagesList where clientEmailAddress equals to UPDATED_CLIENT_EMAIL_ADDRESS
        defaultEmailMessagesShouldNotBeFound("clientEmailAddress.in=" + UPDATED_CLIENT_EMAIL_ADDRESS);
    }

    @Test
    @Transactional
    public void getAllEmailMessagesByClientEmailAddressIsNullOrNotNull() throws Exception {
        // Initialize the database
        emailMessagesRepository.saveAndFlush(emailMessages);

        // Get all the emailMessagesList where clientEmailAddress is not null
        defaultEmailMessagesShouldBeFound("clientEmailAddress.specified=true");

        // Get all the emailMessagesList where clientEmailAddress is null
        defaultEmailMessagesShouldNotBeFound("clientEmailAddress.specified=false");
    }

    @Test
    @Transactional
    public void getAllEmailMessagesByReceiveFromIsEqualToSomething() throws Exception {
        // Initialize the database
        emailMessagesRepository.saveAndFlush(emailMessages);

        // Get all the emailMessagesList where receiveFrom equals to DEFAULT_RECEIVE_FROM
        defaultEmailMessagesShouldBeFound("receiveFrom.equals=" + DEFAULT_RECEIVE_FROM);

        // Get all the emailMessagesList where receiveFrom equals to UPDATED_RECEIVE_FROM
        defaultEmailMessagesShouldNotBeFound("receiveFrom.equals=" + UPDATED_RECEIVE_FROM);
    }

    @Test
    @Transactional
    public void getAllEmailMessagesByReceiveFromIsInShouldWork() throws Exception {
        // Initialize the database
        emailMessagesRepository.saveAndFlush(emailMessages);

        // Get all the emailMessagesList where receiveFrom in DEFAULT_RECEIVE_FROM or UPDATED_RECEIVE_FROM
        defaultEmailMessagesShouldBeFound("receiveFrom.in=" + DEFAULT_RECEIVE_FROM + "," + UPDATED_RECEIVE_FROM);

        // Get all the emailMessagesList where receiveFrom equals to UPDATED_RECEIVE_FROM
        defaultEmailMessagesShouldNotBeFound("receiveFrom.in=" + UPDATED_RECEIVE_FROM);
    }

    @Test
    @Transactional
    public void getAllEmailMessagesByReceiveFromIsNullOrNotNull() throws Exception {
        // Initialize the database
        emailMessagesRepository.saveAndFlush(emailMessages);

        // Get all the emailMessagesList where receiveFrom is not null
        defaultEmailMessagesShouldBeFound("receiveFrom.specified=true");

        // Get all the emailMessagesList where receiveFrom is null
        defaultEmailMessagesShouldNotBeFound("receiveFrom.specified=false");
    }

    @Test
    @Transactional
    public void getAllEmailMessagesByReceivedTimeIsEqualToSomething() throws Exception {
        // Initialize the database
        emailMessagesRepository.saveAndFlush(emailMessages);

        // Get all the emailMessagesList where receivedTime equals to DEFAULT_RECEIVED_TIME
        defaultEmailMessagesShouldBeFound("receivedTime.equals=" + DEFAULT_RECEIVED_TIME);

        // Get all the emailMessagesList where receivedTime equals to UPDATED_RECEIVED_TIME
        defaultEmailMessagesShouldNotBeFound("receivedTime.equals=" + UPDATED_RECEIVED_TIME);
    }

    @Test
    @Transactional
    public void getAllEmailMessagesByReceivedTimeIsInShouldWork() throws Exception {
        // Initialize the database
        emailMessagesRepository.saveAndFlush(emailMessages);

        // Get all the emailMessagesList where receivedTime in DEFAULT_RECEIVED_TIME or UPDATED_RECEIVED_TIME
        defaultEmailMessagesShouldBeFound("receivedTime.in=" + DEFAULT_RECEIVED_TIME + "," + UPDATED_RECEIVED_TIME);

        // Get all the emailMessagesList where receivedTime equals to UPDATED_RECEIVED_TIME
        defaultEmailMessagesShouldNotBeFound("receivedTime.in=" + UPDATED_RECEIVED_TIME);
    }

    @Test
    @Transactional
    public void getAllEmailMessagesByReceivedTimeIsNullOrNotNull() throws Exception {
        // Initialize the database
        emailMessagesRepository.saveAndFlush(emailMessages);

        // Get all the emailMessagesList where receivedTime is not null
        defaultEmailMessagesShouldBeFound("receivedTime.specified=true");

        // Get all the emailMessagesList where receivedTime is null
        defaultEmailMessagesShouldNotBeFound("receivedTime.specified=false");
    }

    @Test
    @Transactional
    public void getAllEmailMessagesByNumberOfAttachmentsIsEqualToSomething() throws Exception {
        // Initialize the database
        emailMessagesRepository.saveAndFlush(emailMessages);

        // Get all the emailMessagesList where numberOfAttachments equals to DEFAULT_NUMBER_OF_ATTACHMENTS
        defaultEmailMessagesShouldBeFound("numberOfAttachments.equals=" + DEFAULT_NUMBER_OF_ATTACHMENTS);

        // Get all the emailMessagesList where numberOfAttachments equals to UPDATED_NUMBER_OF_ATTACHMENTS
        defaultEmailMessagesShouldNotBeFound("numberOfAttachments.equals=" + UPDATED_NUMBER_OF_ATTACHMENTS);
    }

    @Test
    @Transactional
    public void getAllEmailMessagesByNumberOfAttachmentsIsInShouldWork() throws Exception {
        // Initialize the database
        emailMessagesRepository.saveAndFlush(emailMessages);

        // Get all the emailMessagesList where numberOfAttachments in DEFAULT_NUMBER_OF_ATTACHMENTS or UPDATED_NUMBER_OF_ATTACHMENTS
        defaultEmailMessagesShouldBeFound("numberOfAttachments.in=" + DEFAULT_NUMBER_OF_ATTACHMENTS + "," + UPDATED_NUMBER_OF_ATTACHMENTS);

        // Get all the emailMessagesList where numberOfAttachments equals to UPDATED_NUMBER_OF_ATTACHMENTS
        defaultEmailMessagesShouldNotBeFound("numberOfAttachments.in=" + UPDATED_NUMBER_OF_ATTACHMENTS);
    }

    @Test
    @Transactional
    public void getAllEmailMessagesByNumberOfAttachmentsIsNullOrNotNull() throws Exception {
        // Initialize the database
        emailMessagesRepository.saveAndFlush(emailMessages);

        // Get all the emailMessagesList where numberOfAttachments is not null
        defaultEmailMessagesShouldBeFound("numberOfAttachments.specified=true");

        // Get all the emailMessagesList where numberOfAttachments is null
        defaultEmailMessagesShouldNotBeFound("numberOfAttachments.specified=false");
    }

    @Test
    @Transactional
    public void getAllEmailMessagesByNumberOfAttachmentsIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        emailMessagesRepository.saveAndFlush(emailMessages);

        // Get all the emailMessagesList where numberOfAttachments greater than or equals to DEFAULT_NUMBER_OF_ATTACHMENTS
        defaultEmailMessagesShouldBeFound("numberOfAttachments.greaterOrEqualThan=" + DEFAULT_NUMBER_OF_ATTACHMENTS);

        // Get all the emailMessagesList where numberOfAttachments greater than or equals to UPDATED_NUMBER_OF_ATTACHMENTS
        defaultEmailMessagesShouldNotBeFound("numberOfAttachments.greaterOrEqualThan=" + UPDATED_NUMBER_OF_ATTACHMENTS);
    }

    @Test
    @Transactional
    public void getAllEmailMessagesByNumberOfAttachmentsIsLessThanSomething() throws Exception {
        // Initialize the database
        emailMessagesRepository.saveAndFlush(emailMessages);

        // Get all the emailMessagesList where numberOfAttachments less than or equals to DEFAULT_NUMBER_OF_ATTACHMENTS
        defaultEmailMessagesShouldNotBeFound("numberOfAttachments.lessThan=" + DEFAULT_NUMBER_OF_ATTACHMENTS);

        // Get all the emailMessagesList where numberOfAttachments less than or equals to UPDATED_NUMBER_OF_ATTACHMENTS
        defaultEmailMessagesShouldBeFound("numberOfAttachments.lessThan=" + UPDATED_NUMBER_OF_ATTACHMENTS);
    }


    @Test
    @Transactional
    public void getAllEmailMessagesByAttachmentsIsEqualToSomething() throws Exception {
        // Initialize the database
        emailMessagesRepository.saveAndFlush(emailMessages);

        // Get all the emailMessagesList where attachments equals to DEFAULT_ATTACHMENTS
        defaultEmailMessagesShouldBeFound("attachments.equals=" + DEFAULT_ATTACHMENTS);

        // Get all the emailMessagesList where attachments equals to UPDATED_ATTACHMENTS
        defaultEmailMessagesShouldNotBeFound("attachments.equals=" + UPDATED_ATTACHMENTS);
    }

    @Test
    @Transactional
    public void getAllEmailMessagesByAttachmentsIsInShouldWork() throws Exception {
        // Initialize the database
        emailMessagesRepository.saveAndFlush(emailMessages);

        // Get all the emailMessagesList where attachments in DEFAULT_ATTACHMENTS or UPDATED_ATTACHMENTS
        defaultEmailMessagesShouldBeFound("attachments.in=" + DEFAULT_ATTACHMENTS + "," + UPDATED_ATTACHMENTS);

        // Get all the emailMessagesList where attachments equals to UPDATED_ATTACHMENTS
        defaultEmailMessagesShouldNotBeFound("attachments.in=" + UPDATED_ATTACHMENTS);
    }

    @Test
    @Transactional
    public void getAllEmailMessagesByAttachmentsIsNullOrNotNull() throws Exception {
        // Initialize the database
        emailMessagesRepository.saveAndFlush(emailMessages);

        // Get all the emailMessagesList where attachments is not null
        defaultEmailMessagesShouldBeFound("attachments.specified=true");

        // Get all the emailMessagesList where attachments is null
        defaultEmailMessagesShouldNotBeFound("attachments.specified=false");
    }

    @Test
    @Transactional
    public void getAllEmailMessagesByEmailAttachmentIsEqualToSomething() throws Exception {
        // Initialize the database
        EmailAttachment emailAttachment = EmailAttachmentResourceIntTest.createEntity(em);
        em.persist(emailAttachment);
        em.flush();
        emailMessages.addEmailAttachment(emailAttachment);
        emailMessagesRepository.saveAndFlush(emailMessages);
        Long emailAttachmentId = emailAttachment.getId();

        // Get all the emailMessagesList where emailAttachment equals to emailAttachmentId
        defaultEmailMessagesShouldBeFound("emailAttachmentId.equals=" + emailAttachmentId);

        // Get all the emailMessagesList where emailAttachment equals to emailAttachmentId + 1
        defaultEmailMessagesShouldNotBeFound("emailAttachmentId.equals=" + (emailAttachmentId + 1));
    }


    @Test
    @Transactional
    public void getAllEmailMessagesByClientIsEqualToSomething() throws Exception {
        // Initialize the database
        Client client = ClientResourceIntTest.createEntity(em);
        em.persist(client);
        em.flush();
        emailMessages.setClient(client);
        emailMessagesRepository.saveAndFlush(emailMessages);
        Long clientId = client.getId();

        // Get all the emailMessagesList where client equals to clientId
        defaultEmailMessagesShouldBeFound("clientId.equals=" + clientId);

        // Get all the emailMessagesList where client equals to clientId + 1
        defaultEmailMessagesShouldNotBeFound("clientId.equals=" + (clientId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultEmailMessagesShouldBeFound(String filter) throws Exception {
        restEmailMessagesMockMvc.perform(get("/api/email-messages?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(emailMessages.getId().intValue())))
            .andExpect(jsonPath("$.[*].messageId").value(hasItem(DEFAULT_MESSAGE_ID.toString())))
            .andExpect(jsonPath("$.[*].emailSubject").value(hasItem(DEFAULT_EMAIL_SUBJECT.toString())))
            .andExpect(jsonPath("$.[*].emailBody").value(hasItem(DEFAULT_EMAIL_BODY.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].clientEmailAddress").value(hasItem(DEFAULT_CLIENT_EMAIL_ADDRESS.toString())))
            .andExpect(jsonPath("$.[*].receiveFrom").value(hasItem(DEFAULT_RECEIVE_FROM.toString())))
            .andExpect(jsonPath("$.[*].receivedTime").value(hasItem(DEFAULT_RECEIVED_TIME.toString())))
            .andExpect(jsonPath("$.[*].numberOfAttachments").value(hasItem(DEFAULT_NUMBER_OF_ATTACHMENTS)))
            .andExpect(jsonPath("$.[*].attachments").value(hasItem(DEFAULT_ATTACHMENTS.toString())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultEmailMessagesShouldNotBeFound(String filter) throws Exception {
        restEmailMessagesMockMvc.perform(get("/api/email-messages?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingEmailMessages() throws Exception {
        // Get the emailMessages
        restEmailMessagesMockMvc.perform(get("/api/email-messages/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateEmailMessages() throws Exception {
        // Initialize the database
        emailMessagesRepository.saveAndFlush(emailMessages);

        int databaseSizeBeforeUpdate = emailMessagesRepository.findAll().size();

        // Update the emailMessages
        EmailMessages updatedEmailMessages = emailMessagesRepository.findById(emailMessages.getId()).get();
        // Disconnect from session so that the updates on updatedEmailMessages are not directly saved in db
        em.detach(updatedEmailMessages);
        updatedEmailMessages
            .messageId(UPDATED_MESSAGE_ID)
            .emailSubject(UPDATED_EMAIL_SUBJECT)
            .emailBody(UPDATED_EMAIL_BODY)
            .status(UPDATED_STATUS)
            .clientEmailAddress(UPDATED_CLIENT_EMAIL_ADDRESS)
            .receiveFrom(UPDATED_RECEIVE_FROM)
            .receivedTime(UPDATED_RECEIVED_TIME)
            .numberOfAttachments(UPDATED_NUMBER_OF_ATTACHMENTS)
            .attachments(UPDATED_ATTACHMENTS);
        EmailMessagesDTO emailMessagesDTO = emailMessagesMapper.toDto(updatedEmailMessages);

        restEmailMessagesMockMvc.perform(put("/api/email-messages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(emailMessagesDTO)))
            .andExpect(status().isOk());

        // Validate the EmailMessages in the database
        List<EmailMessages> emailMessagesList = emailMessagesRepository.findAll();
        assertThat(emailMessagesList).hasSize(databaseSizeBeforeUpdate);
        EmailMessages testEmailMessages = emailMessagesList.get(emailMessagesList.size() - 1);
        assertThat(testEmailMessages.getMessageId()).isEqualTo(UPDATED_MESSAGE_ID);
        assertThat(testEmailMessages.getEmailSubject()).isEqualTo(UPDATED_EMAIL_SUBJECT);
        assertThat(testEmailMessages.getEmailBody()).isEqualTo(UPDATED_EMAIL_BODY);
        assertThat(testEmailMessages.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testEmailMessages.getClientEmailAddress()).isEqualTo(UPDATED_CLIENT_EMAIL_ADDRESS);
        assertThat(testEmailMessages.getReceiveFrom()).isEqualTo(UPDATED_RECEIVE_FROM);
        assertThat(testEmailMessages.getReceivedTime()).isEqualTo(UPDATED_RECEIVED_TIME);
        assertThat(testEmailMessages.getNumberOfAttachments()).isEqualTo(UPDATED_NUMBER_OF_ATTACHMENTS);
        assertThat(testEmailMessages.getAttachments()).isEqualTo(UPDATED_ATTACHMENTS);
    }

    @Test
    @Transactional
    public void updateNonExistingEmailMessages() throws Exception {
        int databaseSizeBeforeUpdate = emailMessagesRepository.findAll().size();

        // Create the EmailMessages
        EmailMessagesDTO emailMessagesDTO = emailMessagesMapper.toDto(emailMessages);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restEmailMessagesMockMvc.perform(put("/api/email-messages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(emailMessagesDTO)))
            .andExpect(status().isBadRequest());

        // Validate the EmailMessages in the database
        List<EmailMessages> emailMessagesList = emailMessagesRepository.findAll();
        assertThat(emailMessagesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteEmailMessages() throws Exception {
        // Initialize the database
        emailMessagesRepository.saveAndFlush(emailMessages);

        int databaseSizeBeforeDelete = emailMessagesRepository.findAll().size();

        // Get the emailMessages
        restEmailMessagesMockMvc.perform(delete("/api/email-messages/{id}", emailMessages.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<EmailMessages> emailMessagesList = emailMessagesRepository.findAll();
        assertThat(emailMessagesList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(EmailMessages.class);
        EmailMessages emailMessages1 = new EmailMessages();
        emailMessages1.setId(1L);
        EmailMessages emailMessages2 = new EmailMessages();
        emailMessages2.setId(emailMessages1.getId());
        assertThat(emailMessages1).isEqualTo(emailMessages2);
        emailMessages2.setId(2L);
        assertThat(emailMessages1).isNotEqualTo(emailMessages2);
        emailMessages1.setId(null);
        assertThat(emailMessages1).isNotEqualTo(emailMessages2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(EmailMessagesDTO.class);
        EmailMessagesDTO emailMessagesDTO1 = new EmailMessagesDTO();
        emailMessagesDTO1.setId(1L);
        EmailMessagesDTO emailMessagesDTO2 = new EmailMessagesDTO();
        assertThat(emailMessagesDTO1).isNotEqualTo(emailMessagesDTO2);
        emailMessagesDTO2.setId(emailMessagesDTO1.getId());
        assertThat(emailMessagesDTO1).isEqualTo(emailMessagesDTO2);
        emailMessagesDTO2.setId(2L);
        assertThat(emailMessagesDTO1).isNotEqualTo(emailMessagesDTO2);
        emailMessagesDTO1.setId(null);
        assertThat(emailMessagesDTO1).isNotEqualTo(emailMessagesDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(emailMessagesMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(emailMessagesMapper.fromId(null)).isNull();
    }
}
