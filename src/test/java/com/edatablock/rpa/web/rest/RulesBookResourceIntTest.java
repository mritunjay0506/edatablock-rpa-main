package com.edatablock.rpa.web.rest;

import com.edatablock.rpa.EdatablockrpamainApp;

import com.edatablock.rpa.domain.RulesBook;
import com.edatablock.rpa.repository.RulesBookRepository;
import com.edatablock.rpa.service.RulesBookService;
import com.edatablock.rpa.service.dto.RulesBookDTO;
import com.edatablock.rpa.service.mapper.RulesBookMapper;
import com.edatablock.rpa.web.rest.errors.ExceptionTranslator;
import com.edatablock.rpa.service.dto.RulesBookCriteria;
import com.edatablock.rpa.service.RulesBookQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static com.edatablock.rpa.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the RulesBookResource REST controller.
 *
 * @see RulesBookResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = EdatablockrpamainApp.class)
public class RulesBookResourceIntTest {

    private static final String DEFAULT_RULE_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_RULE_NUMBER = "BBBBBBBBBB";

    private static final Integer DEFAULT_RULE_SEQUENCE = 1;
    private static final Integer UPDATED_RULE_SEQUENCE = 2;

    private static final Integer DEFAULT_KEY = 1;
    private static final Integer UPDATED_KEY = 2;

    private static final String DEFAULT_LOOKUP_PLACE_1 = "AAAAAAAAAA";
    private static final String UPDATED_LOOKUP_PLACE_1 = "BBBBBBBBBB";

    private static final String DEFAULT_OPERATOR_1 = "AAAAAAAAAA";
    private static final String UPDATED_OPERATOR_1 = "BBBBBBBBBB";

    private static final String DEFAULT_VALUE_1 = "AAAAAAAAAA";
    private static final String UPDATED_VALUE_1 = "BBBBBBBBBB";

    private static final String DEFAULT_JOIN_FIELD_1 = "AAAAAAAAAA";
    private static final String UPDATED_JOIN_FIELD_1 = "BBBBBBBBBB";

    private static final String DEFAULT_LOOKUP_PLACE_2 = "AAAAAAAAAA";
    private static final String UPDATED_LOOKUP_PLACE_2 = "BBBBBBBBBB";

    private static final String DEFAULT_OPERATOR_2 = "AAAAAAAAAA";
    private static final String UPDATED_OPERATOR_2 = "BBBBBBBBBB";

    private static final String DEFAULT_VALUE_2 = "AAAAAAAAAA";
    private static final String UPDATED_VALUE_2 = "BBBBBBBBBB";

    private static final String DEFAULT_JOIN_FIELD_2 = "AAAAAAAAAA";
    private static final String UPDATED_JOIN_FIELD_2 = "BBBBBBBBBB";

    private static final String DEFAULT_LOOKUP_PLACE_3 = "AAAAAAAAAA";
    private static final String UPDATED_LOOKUP_PLACE_3 = "BBBBBBBBBB";

    private static final String DEFAULT_OPERATOR_3 = "AAAAAAAAAA";
    private static final String UPDATED_OPERATOR_3 = "BBBBBBBBBB";

    private static final String DEFAULT_VALUE_3 = "AAAAAAAAAA";
    private static final String UPDATED_VALUE_3 = "BBBBBBBBBB";

    private static final Integer DEFAULT_RESULT = 1;
    private static final Integer UPDATED_RESULT = 2;

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private RulesBookRepository rulesBookRepository;

    @Autowired
    private RulesBookMapper rulesBookMapper;
    
    @Autowired
    private RulesBookService rulesBookService;

    @Autowired
    private RulesBookQueryService rulesBookQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restRulesBookMockMvc;

    private RulesBook rulesBook;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RulesBookResource rulesBookResource = new RulesBookResource(rulesBookService, rulesBookQueryService);
        this.restRulesBookMockMvc = MockMvcBuilders.standaloneSetup(rulesBookResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RulesBook createEntity(EntityManager em) {
        RulesBook rulesBook = new RulesBook()
            .ruleNumber(DEFAULT_RULE_NUMBER)
            .ruleSequence(DEFAULT_RULE_SEQUENCE)
            .key(DEFAULT_KEY)
            .lookupPlace1(DEFAULT_LOOKUP_PLACE_1)
            .operator1(DEFAULT_OPERATOR_1)
            .value1(DEFAULT_VALUE_1)
            .joinField1(DEFAULT_JOIN_FIELD_1)
            .lookupPlace2(DEFAULT_LOOKUP_PLACE_2)
            .operator2(DEFAULT_OPERATOR_2)
            .value2(DEFAULT_VALUE_2)
            .joinField2(DEFAULT_JOIN_FIELD_2)
            .lookupPlace3(DEFAULT_LOOKUP_PLACE_3)
            .operator3(DEFAULT_OPERATOR_3)
            .value3(DEFAULT_VALUE_3)
            .result(DEFAULT_RESULT)
            .description(DEFAULT_DESCRIPTION);
        return rulesBook;
    }

    @Before
    public void initTest() {
        rulesBook = createEntity(em);
    }

    @Test
    @Transactional
    public void createRulesBook() throws Exception {
        int databaseSizeBeforeCreate = rulesBookRepository.findAll().size();

        // Create the RulesBook
        RulesBookDTO rulesBookDTO = rulesBookMapper.toDto(rulesBook);
        restRulesBookMockMvc.perform(post("/api/rules-books")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(rulesBookDTO)))
            .andExpect(status().isCreated());

        // Validate the RulesBook in the database
        List<RulesBook> rulesBookList = rulesBookRepository.findAll();
        assertThat(rulesBookList).hasSize(databaseSizeBeforeCreate + 1);
        RulesBook testRulesBook = rulesBookList.get(rulesBookList.size() - 1);
        assertThat(testRulesBook.getRuleNumber()).isEqualTo(DEFAULT_RULE_NUMBER);
        assertThat(testRulesBook.getRuleSequence()).isEqualTo(DEFAULT_RULE_SEQUENCE);
        assertThat(testRulesBook.getKey()).isEqualTo(DEFAULT_KEY);
        assertThat(testRulesBook.getLookupPlace1()).isEqualTo(DEFAULT_LOOKUP_PLACE_1);
        assertThat(testRulesBook.getOperator1()).isEqualTo(DEFAULT_OPERATOR_1);
        assertThat(testRulesBook.getValue1()).isEqualTo(DEFAULT_VALUE_1);
        assertThat(testRulesBook.getJoinField1()).isEqualTo(DEFAULT_JOIN_FIELD_1);
        assertThat(testRulesBook.getLookupPlace2()).isEqualTo(DEFAULT_LOOKUP_PLACE_2);
        assertThat(testRulesBook.getOperator2()).isEqualTo(DEFAULT_OPERATOR_2);
        assertThat(testRulesBook.getValue2()).isEqualTo(DEFAULT_VALUE_2);
        assertThat(testRulesBook.getJoinField2()).isEqualTo(DEFAULT_JOIN_FIELD_2);
        assertThat(testRulesBook.getLookupPlace3()).isEqualTo(DEFAULT_LOOKUP_PLACE_3);
        assertThat(testRulesBook.getOperator3()).isEqualTo(DEFAULT_OPERATOR_3);
        assertThat(testRulesBook.getValue3()).isEqualTo(DEFAULT_VALUE_3);
        assertThat(testRulesBook.getResult()).isEqualTo(DEFAULT_RESULT);
        assertThat(testRulesBook.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createRulesBookWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = rulesBookRepository.findAll().size();

        // Create the RulesBook with an existing ID
        rulesBook.setId(1L);
        RulesBookDTO rulesBookDTO = rulesBookMapper.toDto(rulesBook);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRulesBookMockMvc.perform(post("/api/rules-books")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(rulesBookDTO)))
            .andExpect(status().isBadRequest());

        // Validate the RulesBook in the database
        List<RulesBook> rulesBookList = rulesBookRepository.findAll();
        assertThat(rulesBookList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllRulesBooks() throws Exception {
        // Initialize the database
        rulesBookRepository.saveAndFlush(rulesBook);

        // Get all the rulesBookList
        restRulesBookMockMvc.perform(get("/api/rules-books?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(rulesBook.getId().intValue())))
            .andExpect(jsonPath("$.[*].ruleNumber").value(hasItem(DEFAULT_RULE_NUMBER.toString())))
            .andExpect(jsonPath("$.[*].ruleSequence").value(hasItem(DEFAULT_RULE_SEQUENCE)))
            .andExpect(jsonPath("$.[*].key").value(hasItem(DEFAULT_KEY)))
            .andExpect(jsonPath("$.[*].lookupPlace1").value(hasItem(DEFAULT_LOOKUP_PLACE_1.toString())))
            .andExpect(jsonPath("$.[*].operator1").value(hasItem(DEFAULT_OPERATOR_1.toString())))
            .andExpect(jsonPath("$.[*].value1").value(hasItem(DEFAULT_VALUE_1.toString())))
            .andExpect(jsonPath("$.[*].joinField1").value(hasItem(DEFAULT_JOIN_FIELD_1.toString())))
            .andExpect(jsonPath("$.[*].lookupPlace2").value(hasItem(DEFAULT_LOOKUP_PLACE_2.toString())))
            .andExpect(jsonPath("$.[*].operator2").value(hasItem(DEFAULT_OPERATOR_2.toString())))
            .andExpect(jsonPath("$.[*].value2").value(hasItem(DEFAULT_VALUE_2.toString())))
            .andExpect(jsonPath("$.[*].joinField2").value(hasItem(DEFAULT_JOIN_FIELD_2.toString())))
            .andExpect(jsonPath("$.[*].lookupPlace3").value(hasItem(DEFAULT_LOOKUP_PLACE_3.toString())))
            .andExpect(jsonPath("$.[*].operator3").value(hasItem(DEFAULT_OPERATOR_3.toString())))
            .andExpect(jsonPath("$.[*].value3").value(hasItem(DEFAULT_VALUE_3.toString())))
            .andExpect(jsonPath("$.[*].result").value(hasItem(DEFAULT_RESULT)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }
    
    @Test
    @Transactional
    public void getRulesBook() throws Exception {
        // Initialize the database
        rulesBookRepository.saveAndFlush(rulesBook);

        // Get the rulesBook
        restRulesBookMockMvc.perform(get("/api/rules-books/{id}", rulesBook.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(rulesBook.getId().intValue()))
            .andExpect(jsonPath("$.ruleNumber").value(DEFAULT_RULE_NUMBER.toString()))
            .andExpect(jsonPath("$.ruleSequence").value(DEFAULT_RULE_SEQUENCE))
            .andExpect(jsonPath("$.key").value(DEFAULT_KEY))
            .andExpect(jsonPath("$.lookupPlace1").value(DEFAULT_LOOKUP_PLACE_1.toString()))
            .andExpect(jsonPath("$.operator1").value(DEFAULT_OPERATOR_1.toString()))
            .andExpect(jsonPath("$.value1").value(DEFAULT_VALUE_1.toString()))
            .andExpect(jsonPath("$.joinField1").value(DEFAULT_JOIN_FIELD_1.toString()))
            .andExpect(jsonPath("$.lookupPlace2").value(DEFAULT_LOOKUP_PLACE_2.toString()))
            .andExpect(jsonPath("$.operator2").value(DEFAULT_OPERATOR_2.toString()))
            .andExpect(jsonPath("$.value2").value(DEFAULT_VALUE_2.toString()))
            .andExpect(jsonPath("$.joinField2").value(DEFAULT_JOIN_FIELD_2.toString()))
            .andExpect(jsonPath("$.lookupPlace3").value(DEFAULT_LOOKUP_PLACE_3.toString()))
            .andExpect(jsonPath("$.operator3").value(DEFAULT_OPERATOR_3.toString()))
            .andExpect(jsonPath("$.value3").value(DEFAULT_VALUE_3.toString()))
            .andExpect(jsonPath("$.result").value(DEFAULT_RESULT))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getAllRulesBooksByRuleNumberIsEqualToSomething() throws Exception {
        // Initialize the database
        rulesBookRepository.saveAndFlush(rulesBook);

        // Get all the rulesBookList where ruleNumber equals to DEFAULT_RULE_NUMBER
        defaultRulesBookShouldBeFound("ruleNumber.equals=" + DEFAULT_RULE_NUMBER);

        // Get all the rulesBookList where ruleNumber equals to UPDATED_RULE_NUMBER
        defaultRulesBookShouldNotBeFound("ruleNumber.equals=" + UPDATED_RULE_NUMBER);
    }

    @Test
    @Transactional
    public void getAllRulesBooksByRuleNumberIsInShouldWork() throws Exception {
        // Initialize the database
        rulesBookRepository.saveAndFlush(rulesBook);

        // Get all the rulesBookList where ruleNumber in DEFAULT_RULE_NUMBER or UPDATED_RULE_NUMBER
        defaultRulesBookShouldBeFound("ruleNumber.in=" + DEFAULT_RULE_NUMBER + "," + UPDATED_RULE_NUMBER);

        // Get all the rulesBookList where ruleNumber equals to UPDATED_RULE_NUMBER
        defaultRulesBookShouldNotBeFound("ruleNumber.in=" + UPDATED_RULE_NUMBER);
    }

    @Test
    @Transactional
    public void getAllRulesBooksByRuleNumberIsNullOrNotNull() throws Exception {
        // Initialize the database
        rulesBookRepository.saveAndFlush(rulesBook);

        // Get all the rulesBookList where ruleNumber is not null
        defaultRulesBookShouldBeFound("ruleNumber.specified=true");

        // Get all the rulesBookList where ruleNumber is null
        defaultRulesBookShouldNotBeFound("ruleNumber.specified=false");
    }

    @Test
    @Transactional
    public void getAllRulesBooksByRuleSequenceIsEqualToSomething() throws Exception {
        // Initialize the database
        rulesBookRepository.saveAndFlush(rulesBook);

        // Get all the rulesBookList where ruleSequence equals to DEFAULT_RULE_SEQUENCE
        defaultRulesBookShouldBeFound("ruleSequence.equals=" + DEFAULT_RULE_SEQUENCE);

        // Get all the rulesBookList where ruleSequence equals to UPDATED_RULE_SEQUENCE
        defaultRulesBookShouldNotBeFound("ruleSequence.equals=" + UPDATED_RULE_SEQUENCE);
    }

    @Test
    @Transactional
    public void getAllRulesBooksByRuleSequenceIsInShouldWork() throws Exception {
        // Initialize the database
        rulesBookRepository.saveAndFlush(rulesBook);

        // Get all the rulesBookList where ruleSequence in DEFAULT_RULE_SEQUENCE or UPDATED_RULE_SEQUENCE
        defaultRulesBookShouldBeFound("ruleSequence.in=" + DEFAULT_RULE_SEQUENCE + "," + UPDATED_RULE_SEQUENCE);

        // Get all the rulesBookList where ruleSequence equals to UPDATED_RULE_SEQUENCE
        defaultRulesBookShouldNotBeFound("ruleSequence.in=" + UPDATED_RULE_SEQUENCE);
    }

    @Test
    @Transactional
    public void getAllRulesBooksByRuleSequenceIsNullOrNotNull() throws Exception {
        // Initialize the database
        rulesBookRepository.saveAndFlush(rulesBook);

        // Get all the rulesBookList where ruleSequence is not null
        defaultRulesBookShouldBeFound("ruleSequence.specified=true");

        // Get all the rulesBookList where ruleSequence is null
        defaultRulesBookShouldNotBeFound("ruleSequence.specified=false");
    }

    @Test
    @Transactional
    public void getAllRulesBooksByRuleSequenceIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        rulesBookRepository.saveAndFlush(rulesBook);

        // Get all the rulesBookList where ruleSequence greater than or equals to DEFAULT_RULE_SEQUENCE
        defaultRulesBookShouldBeFound("ruleSequence.greaterOrEqualThan=" + DEFAULT_RULE_SEQUENCE);

        // Get all the rulesBookList where ruleSequence greater than or equals to UPDATED_RULE_SEQUENCE
        defaultRulesBookShouldNotBeFound("ruleSequence.greaterOrEqualThan=" + UPDATED_RULE_SEQUENCE);
    }

    @Test
    @Transactional
    public void getAllRulesBooksByRuleSequenceIsLessThanSomething() throws Exception {
        // Initialize the database
        rulesBookRepository.saveAndFlush(rulesBook);

        // Get all the rulesBookList where ruleSequence less than or equals to DEFAULT_RULE_SEQUENCE
        defaultRulesBookShouldNotBeFound("ruleSequence.lessThan=" + DEFAULT_RULE_SEQUENCE);

        // Get all the rulesBookList where ruleSequence less than or equals to UPDATED_RULE_SEQUENCE
        defaultRulesBookShouldBeFound("ruleSequence.lessThan=" + UPDATED_RULE_SEQUENCE);
    }


    @Test
    @Transactional
    public void getAllRulesBooksByKeyIsEqualToSomething() throws Exception {
        // Initialize the database
        rulesBookRepository.saveAndFlush(rulesBook);

        // Get all the rulesBookList where key equals to DEFAULT_KEY
        defaultRulesBookShouldBeFound("key.equals=" + DEFAULT_KEY);

        // Get all the rulesBookList where key equals to UPDATED_KEY
        defaultRulesBookShouldNotBeFound("key.equals=" + UPDATED_KEY);
    }

    @Test
    @Transactional
    public void getAllRulesBooksByKeyIsInShouldWork() throws Exception {
        // Initialize the database
        rulesBookRepository.saveAndFlush(rulesBook);

        // Get all the rulesBookList where key in DEFAULT_KEY or UPDATED_KEY
        defaultRulesBookShouldBeFound("key.in=" + DEFAULT_KEY + "," + UPDATED_KEY);

        // Get all the rulesBookList where key equals to UPDATED_KEY
        defaultRulesBookShouldNotBeFound("key.in=" + UPDATED_KEY);
    }

    @Test
    @Transactional
    public void getAllRulesBooksByKeyIsNullOrNotNull() throws Exception {
        // Initialize the database
        rulesBookRepository.saveAndFlush(rulesBook);

        // Get all the rulesBookList where key is not null
        defaultRulesBookShouldBeFound("key.specified=true");

        // Get all the rulesBookList where key is null
        defaultRulesBookShouldNotBeFound("key.specified=false");
    }

    @Test
    @Transactional
    public void getAllRulesBooksByKeyIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        rulesBookRepository.saveAndFlush(rulesBook);

        // Get all the rulesBookList where key greater than or equals to DEFAULT_KEY
        defaultRulesBookShouldBeFound("key.greaterOrEqualThan=" + DEFAULT_KEY);

        // Get all the rulesBookList where key greater than or equals to UPDATED_KEY
        defaultRulesBookShouldNotBeFound("key.greaterOrEqualThan=" + UPDATED_KEY);
    }

    @Test
    @Transactional
    public void getAllRulesBooksByKeyIsLessThanSomething() throws Exception {
        // Initialize the database
        rulesBookRepository.saveAndFlush(rulesBook);

        // Get all the rulesBookList where key less than or equals to DEFAULT_KEY
        defaultRulesBookShouldNotBeFound("key.lessThan=" + DEFAULT_KEY);

        // Get all the rulesBookList where key less than or equals to UPDATED_KEY
        defaultRulesBookShouldBeFound("key.lessThan=" + UPDATED_KEY);
    }


    @Test
    @Transactional
    public void getAllRulesBooksByLookupPlace1IsEqualToSomething() throws Exception {
        // Initialize the database
        rulesBookRepository.saveAndFlush(rulesBook);

        // Get all the rulesBookList where lookupPlace1 equals to DEFAULT_LOOKUP_PLACE_1
        defaultRulesBookShouldBeFound("lookupPlace1.equals=" + DEFAULT_LOOKUP_PLACE_1);

        // Get all the rulesBookList where lookupPlace1 equals to UPDATED_LOOKUP_PLACE_1
        defaultRulesBookShouldNotBeFound("lookupPlace1.equals=" + UPDATED_LOOKUP_PLACE_1);
    }

    @Test
    @Transactional
    public void getAllRulesBooksByLookupPlace1IsInShouldWork() throws Exception {
        // Initialize the database
        rulesBookRepository.saveAndFlush(rulesBook);

        // Get all the rulesBookList where lookupPlace1 in DEFAULT_LOOKUP_PLACE_1 or UPDATED_LOOKUP_PLACE_1
        defaultRulesBookShouldBeFound("lookupPlace1.in=" + DEFAULT_LOOKUP_PLACE_1 + "," + UPDATED_LOOKUP_PLACE_1);

        // Get all the rulesBookList where lookupPlace1 equals to UPDATED_LOOKUP_PLACE_1
        defaultRulesBookShouldNotBeFound("lookupPlace1.in=" + UPDATED_LOOKUP_PLACE_1);
    }

    @Test
    @Transactional
    public void getAllRulesBooksByLookupPlace1IsNullOrNotNull() throws Exception {
        // Initialize the database
        rulesBookRepository.saveAndFlush(rulesBook);

        // Get all the rulesBookList where lookupPlace1 is not null
        defaultRulesBookShouldBeFound("lookupPlace1.specified=true");

        // Get all the rulesBookList where lookupPlace1 is null
        defaultRulesBookShouldNotBeFound("lookupPlace1.specified=false");
    }

    @Test
    @Transactional
    public void getAllRulesBooksByOperator1IsEqualToSomething() throws Exception {
        // Initialize the database
        rulesBookRepository.saveAndFlush(rulesBook);

        // Get all the rulesBookList where operator1 equals to DEFAULT_OPERATOR_1
        defaultRulesBookShouldBeFound("operator1.equals=" + DEFAULT_OPERATOR_1);

        // Get all the rulesBookList where operator1 equals to UPDATED_OPERATOR_1
        defaultRulesBookShouldNotBeFound("operator1.equals=" + UPDATED_OPERATOR_1);
    }

    @Test
    @Transactional
    public void getAllRulesBooksByOperator1IsInShouldWork() throws Exception {
        // Initialize the database
        rulesBookRepository.saveAndFlush(rulesBook);

        // Get all the rulesBookList where operator1 in DEFAULT_OPERATOR_1 or UPDATED_OPERATOR_1
        defaultRulesBookShouldBeFound("operator1.in=" + DEFAULT_OPERATOR_1 + "," + UPDATED_OPERATOR_1);

        // Get all the rulesBookList where operator1 equals to UPDATED_OPERATOR_1
        defaultRulesBookShouldNotBeFound("operator1.in=" + UPDATED_OPERATOR_1);
    }

    @Test
    @Transactional
    public void getAllRulesBooksByOperator1IsNullOrNotNull() throws Exception {
        // Initialize the database
        rulesBookRepository.saveAndFlush(rulesBook);

        // Get all the rulesBookList where operator1 is not null
        defaultRulesBookShouldBeFound("operator1.specified=true");

        // Get all the rulesBookList where operator1 is null
        defaultRulesBookShouldNotBeFound("operator1.specified=false");
    }

    @Test
    @Transactional
    public void getAllRulesBooksByValue1IsEqualToSomething() throws Exception {
        // Initialize the database
        rulesBookRepository.saveAndFlush(rulesBook);

        // Get all the rulesBookList where value1 equals to DEFAULT_VALUE_1
        defaultRulesBookShouldBeFound("value1.equals=" + DEFAULT_VALUE_1);

        // Get all the rulesBookList where value1 equals to UPDATED_VALUE_1
        defaultRulesBookShouldNotBeFound("value1.equals=" + UPDATED_VALUE_1);
    }

    @Test
    @Transactional
    public void getAllRulesBooksByValue1IsInShouldWork() throws Exception {
        // Initialize the database
        rulesBookRepository.saveAndFlush(rulesBook);

        // Get all the rulesBookList where value1 in DEFAULT_VALUE_1 or UPDATED_VALUE_1
        defaultRulesBookShouldBeFound("value1.in=" + DEFAULT_VALUE_1 + "," + UPDATED_VALUE_1);

        // Get all the rulesBookList where value1 equals to UPDATED_VALUE_1
        defaultRulesBookShouldNotBeFound("value1.in=" + UPDATED_VALUE_1);
    }

    @Test
    @Transactional
    public void getAllRulesBooksByValue1IsNullOrNotNull() throws Exception {
        // Initialize the database
        rulesBookRepository.saveAndFlush(rulesBook);

        // Get all the rulesBookList where value1 is not null
        defaultRulesBookShouldBeFound("value1.specified=true");

        // Get all the rulesBookList where value1 is null
        defaultRulesBookShouldNotBeFound("value1.specified=false");
    }

    @Test
    @Transactional
    public void getAllRulesBooksByJoinField1IsEqualToSomething() throws Exception {
        // Initialize the database
        rulesBookRepository.saveAndFlush(rulesBook);

        // Get all the rulesBookList where joinField1 equals to DEFAULT_JOIN_FIELD_1
        defaultRulesBookShouldBeFound("joinField1.equals=" + DEFAULT_JOIN_FIELD_1);

        // Get all the rulesBookList where joinField1 equals to UPDATED_JOIN_FIELD_1
        defaultRulesBookShouldNotBeFound("joinField1.equals=" + UPDATED_JOIN_FIELD_1);
    }

    @Test
    @Transactional
    public void getAllRulesBooksByJoinField1IsInShouldWork() throws Exception {
        // Initialize the database
        rulesBookRepository.saveAndFlush(rulesBook);

        // Get all the rulesBookList where joinField1 in DEFAULT_JOIN_FIELD_1 or UPDATED_JOIN_FIELD_1
        defaultRulesBookShouldBeFound("joinField1.in=" + DEFAULT_JOIN_FIELD_1 + "," + UPDATED_JOIN_FIELD_1);

        // Get all the rulesBookList where joinField1 equals to UPDATED_JOIN_FIELD_1
        defaultRulesBookShouldNotBeFound("joinField1.in=" + UPDATED_JOIN_FIELD_1);
    }

    @Test
    @Transactional
    public void getAllRulesBooksByJoinField1IsNullOrNotNull() throws Exception {
        // Initialize the database
        rulesBookRepository.saveAndFlush(rulesBook);

        // Get all the rulesBookList where joinField1 is not null
        defaultRulesBookShouldBeFound("joinField1.specified=true");

        // Get all the rulesBookList where joinField1 is null
        defaultRulesBookShouldNotBeFound("joinField1.specified=false");
    }

    @Test
    @Transactional
    public void getAllRulesBooksByLookupPlace2IsEqualToSomething() throws Exception {
        // Initialize the database
        rulesBookRepository.saveAndFlush(rulesBook);

        // Get all the rulesBookList where lookupPlace2 equals to DEFAULT_LOOKUP_PLACE_2
        defaultRulesBookShouldBeFound("lookupPlace2.equals=" + DEFAULT_LOOKUP_PLACE_2);

        // Get all the rulesBookList where lookupPlace2 equals to UPDATED_LOOKUP_PLACE_2
        defaultRulesBookShouldNotBeFound("lookupPlace2.equals=" + UPDATED_LOOKUP_PLACE_2);
    }

    @Test
    @Transactional
    public void getAllRulesBooksByLookupPlace2IsInShouldWork() throws Exception {
        // Initialize the database
        rulesBookRepository.saveAndFlush(rulesBook);

        // Get all the rulesBookList where lookupPlace2 in DEFAULT_LOOKUP_PLACE_2 or UPDATED_LOOKUP_PLACE_2
        defaultRulesBookShouldBeFound("lookupPlace2.in=" + DEFAULT_LOOKUP_PLACE_2 + "," + UPDATED_LOOKUP_PLACE_2);

        // Get all the rulesBookList where lookupPlace2 equals to UPDATED_LOOKUP_PLACE_2
        defaultRulesBookShouldNotBeFound("lookupPlace2.in=" + UPDATED_LOOKUP_PLACE_2);
    }

    @Test
    @Transactional
    public void getAllRulesBooksByLookupPlace2IsNullOrNotNull() throws Exception {
        // Initialize the database
        rulesBookRepository.saveAndFlush(rulesBook);

        // Get all the rulesBookList where lookupPlace2 is not null
        defaultRulesBookShouldBeFound("lookupPlace2.specified=true");

        // Get all the rulesBookList where lookupPlace2 is null
        defaultRulesBookShouldNotBeFound("lookupPlace2.specified=false");
    }

    @Test
    @Transactional
    public void getAllRulesBooksByOperator2IsEqualToSomething() throws Exception {
        // Initialize the database
        rulesBookRepository.saveAndFlush(rulesBook);

        // Get all the rulesBookList where operator2 equals to DEFAULT_OPERATOR_2
        defaultRulesBookShouldBeFound("operator2.equals=" + DEFAULT_OPERATOR_2);

        // Get all the rulesBookList where operator2 equals to UPDATED_OPERATOR_2
        defaultRulesBookShouldNotBeFound("operator2.equals=" + UPDATED_OPERATOR_2);
    }

    @Test
    @Transactional
    public void getAllRulesBooksByOperator2IsInShouldWork() throws Exception {
        // Initialize the database
        rulesBookRepository.saveAndFlush(rulesBook);

        // Get all the rulesBookList where operator2 in DEFAULT_OPERATOR_2 or UPDATED_OPERATOR_2
        defaultRulesBookShouldBeFound("operator2.in=" + DEFAULT_OPERATOR_2 + "," + UPDATED_OPERATOR_2);

        // Get all the rulesBookList where operator2 equals to UPDATED_OPERATOR_2
        defaultRulesBookShouldNotBeFound("operator2.in=" + UPDATED_OPERATOR_2);
    }

    @Test
    @Transactional
    public void getAllRulesBooksByOperator2IsNullOrNotNull() throws Exception {
        // Initialize the database
        rulesBookRepository.saveAndFlush(rulesBook);

        // Get all the rulesBookList where operator2 is not null
        defaultRulesBookShouldBeFound("operator2.specified=true");

        // Get all the rulesBookList where operator2 is null
        defaultRulesBookShouldNotBeFound("operator2.specified=false");
    }

    @Test
    @Transactional
    public void getAllRulesBooksByValue2IsEqualToSomething() throws Exception {
        // Initialize the database
        rulesBookRepository.saveAndFlush(rulesBook);

        // Get all the rulesBookList where value2 equals to DEFAULT_VALUE_2
        defaultRulesBookShouldBeFound("value2.equals=" + DEFAULT_VALUE_2);

        // Get all the rulesBookList where value2 equals to UPDATED_VALUE_2
        defaultRulesBookShouldNotBeFound("value2.equals=" + UPDATED_VALUE_2);
    }

    @Test
    @Transactional
    public void getAllRulesBooksByValue2IsInShouldWork() throws Exception {
        // Initialize the database
        rulesBookRepository.saveAndFlush(rulesBook);

        // Get all the rulesBookList where value2 in DEFAULT_VALUE_2 or UPDATED_VALUE_2
        defaultRulesBookShouldBeFound("value2.in=" + DEFAULT_VALUE_2 + "," + UPDATED_VALUE_2);

        // Get all the rulesBookList where value2 equals to UPDATED_VALUE_2
        defaultRulesBookShouldNotBeFound("value2.in=" + UPDATED_VALUE_2);
    }

    @Test
    @Transactional
    public void getAllRulesBooksByValue2IsNullOrNotNull() throws Exception {
        // Initialize the database
        rulesBookRepository.saveAndFlush(rulesBook);

        // Get all the rulesBookList where value2 is not null
        defaultRulesBookShouldBeFound("value2.specified=true");

        // Get all the rulesBookList where value2 is null
        defaultRulesBookShouldNotBeFound("value2.specified=false");
    }

    @Test
    @Transactional
    public void getAllRulesBooksByJoinField2IsEqualToSomething() throws Exception {
        // Initialize the database
        rulesBookRepository.saveAndFlush(rulesBook);

        // Get all the rulesBookList where joinField2 equals to DEFAULT_JOIN_FIELD_2
        defaultRulesBookShouldBeFound("joinField2.equals=" + DEFAULT_JOIN_FIELD_2);

        // Get all the rulesBookList where joinField2 equals to UPDATED_JOIN_FIELD_2
        defaultRulesBookShouldNotBeFound("joinField2.equals=" + UPDATED_JOIN_FIELD_2);
    }

    @Test
    @Transactional
    public void getAllRulesBooksByJoinField2IsInShouldWork() throws Exception {
        // Initialize the database
        rulesBookRepository.saveAndFlush(rulesBook);

        // Get all the rulesBookList where joinField2 in DEFAULT_JOIN_FIELD_2 or UPDATED_JOIN_FIELD_2
        defaultRulesBookShouldBeFound("joinField2.in=" + DEFAULT_JOIN_FIELD_2 + "," + UPDATED_JOIN_FIELD_2);

        // Get all the rulesBookList where joinField2 equals to UPDATED_JOIN_FIELD_2
        defaultRulesBookShouldNotBeFound("joinField2.in=" + UPDATED_JOIN_FIELD_2);
    }

    @Test
    @Transactional
    public void getAllRulesBooksByJoinField2IsNullOrNotNull() throws Exception {
        // Initialize the database
        rulesBookRepository.saveAndFlush(rulesBook);

        // Get all the rulesBookList where joinField2 is not null
        defaultRulesBookShouldBeFound("joinField2.specified=true");

        // Get all the rulesBookList where joinField2 is null
        defaultRulesBookShouldNotBeFound("joinField2.specified=false");
    }

    @Test
    @Transactional
    public void getAllRulesBooksByLookupPlace3IsEqualToSomething() throws Exception {
        // Initialize the database
        rulesBookRepository.saveAndFlush(rulesBook);

        // Get all the rulesBookList where lookupPlace3 equals to DEFAULT_LOOKUP_PLACE_3
        defaultRulesBookShouldBeFound("lookupPlace3.equals=" + DEFAULT_LOOKUP_PLACE_3);

        // Get all the rulesBookList where lookupPlace3 equals to UPDATED_LOOKUP_PLACE_3
        defaultRulesBookShouldNotBeFound("lookupPlace3.equals=" + UPDATED_LOOKUP_PLACE_3);
    }

    @Test
    @Transactional
    public void getAllRulesBooksByLookupPlace3IsInShouldWork() throws Exception {
        // Initialize the database
        rulesBookRepository.saveAndFlush(rulesBook);

        // Get all the rulesBookList where lookupPlace3 in DEFAULT_LOOKUP_PLACE_3 or UPDATED_LOOKUP_PLACE_3
        defaultRulesBookShouldBeFound("lookupPlace3.in=" + DEFAULT_LOOKUP_PLACE_3 + "," + UPDATED_LOOKUP_PLACE_3);

        // Get all the rulesBookList where lookupPlace3 equals to UPDATED_LOOKUP_PLACE_3
        defaultRulesBookShouldNotBeFound("lookupPlace3.in=" + UPDATED_LOOKUP_PLACE_3);
    }

    @Test
    @Transactional
    public void getAllRulesBooksByLookupPlace3IsNullOrNotNull() throws Exception {
        // Initialize the database
        rulesBookRepository.saveAndFlush(rulesBook);

        // Get all the rulesBookList where lookupPlace3 is not null
        defaultRulesBookShouldBeFound("lookupPlace3.specified=true");

        // Get all the rulesBookList where lookupPlace3 is null
        defaultRulesBookShouldNotBeFound("lookupPlace3.specified=false");
    }

    @Test
    @Transactional
    public void getAllRulesBooksByOperator3IsEqualToSomething() throws Exception {
        // Initialize the database
        rulesBookRepository.saveAndFlush(rulesBook);

        // Get all the rulesBookList where operator3 equals to DEFAULT_OPERATOR_3
        defaultRulesBookShouldBeFound("operator3.equals=" + DEFAULT_OPERATOR_3);

        // Get all the rulesBookList where operator3 equals to UPDATED_OPERATOR_3
        defaultRulesBookShouldNotBeFound("operator3.equals=" + UPDATED_OPERATOR_3);
    }

    @Test
    @Transactional
    public void getAllRulesBooksByOperator3IsInShouldWork() throws Exception {
        // Initialize the database
        rulesBookRepository.saveAndFlush(rulesBook);

        // Get all the rulesBookList where operator3 in DEFAULT_OPERATOR_3 or UPDATED_OPERATOR_3
        defaultRulesBookShouldBeFound("operator3.in=" + DEFAULT_OPERATOR_3 + "," + UPDATED_OPERATOR_3);

        // Get all the rulesBookList where operator3 equals to UPDATED_OPERATOR_3
        defaultRulesBookShouldNotBeFound("operator3.in=" + UPDATED_OPERATOR_3);
    }

    @Test
    @Transactional
    public void getAllRulesBooksByOperator3IsNullOrNotNull() throws Exception {
        // Initialize the database
        rulesBookRepository.saveAndFlush(rulesBook);

        // Get all the rulesBookList where operator3 is not null
        defaultRulesBookShouldBeFound("operator3.specified=true");

        // Get all the rulesBookList where operator3 is null
        defaultRulesBookShouldNotBeFound("operator3.specified=false");
    }

    @Test
    @Transactional
    public void getAllRulesBooksByValue3IsEqualToSomething() throws Exception {
        // Initialize the database
        rulesBookRepository.saveAndFlush(rulesBook);

        // Get all the rulesBookList where value3 equals to DEFAULT_VALUE_3
        defaultRulesBookShouldBeFound("value3.equals=" + DEFAULT_VALUE_3);

        // Get all the rulesBookList where value3 equals to UPDATED_VALUE_3
        defaultRulesBookShouldNotBeFound("value3.equals=" + UPDATED_VALUE_3);
    }

    @Test
    @Transactional
    public void getAllRulesBooksByValue3IsInShouldWork() throws Exception {
        // Initialize the database
        rulesBookRepository.saveAndFlush(rulesBook);

        // Get all the rulesBookList where value3 in DEFAULT_VALUE_3 or UPDATED_VALUE_3
        defaultRulesBookShouldBeFound("value3.in=" + DEFAULT_VALUE_3 + "," + UPDATED_VALUE_3);

        // Get all the rulesBookList where value3 equals to UPDATED_VALUE_3
        defaultRulesBookShouldNotBeFound("value3.in=" + UPDATED_VALUE_3);
    }

    @Test
    @Transactional
    public void getAllRulesBooksByValue3IsNullOrNotNull() throws Exception {
        // Initialize the database
        rulesBookRepository.saveAndFlush(rulesBook);

        // Get all the rulesBookList where value3 is not null
        defaultRulesBookShouldBeFound("value3.specified=true");

        // Get all the rulesBookList where value3 is null
        defaultRulesBookShouldNotBeFound("value3.specified=false");
    }

    @Test
    @Transactional
    public void getAllRulesBooksByResultIsEqualToSomething() throws Exception {
        // Initialize the database
        rulesBookRepository.saveAndFlush(rulesBook);

        // Get all the rulesBookList where result equals to DEFAULT_RESULT
        defaultRulesBookShouldBeFound("result.equals=" + DEFAULT_RESULT);

        // Get all the rulesBookList where result equals to UPDATED_RESULT
        defaultRulesBookShouldNotBeFound("result.equals=" + UPDATED_RESULT);
    }

    @Test
    @Transactional
    public void getAllRulesBooksByResultIsInShouldWork() throws Exception {
        // Initialize the database
        rulesBookRepository.saveAndFlush(rulesBook);

        // Get all the rulesBookList where result in DEFAULT_RESULT or UPDATED_RESULT
        defaultRulesBookShouldBeFound("result.in=" + DEFAULT_RESULT + "," + UPDATED_RESULT);

        // Get all the rulesBookList where result equals to UPDATED_RESULT
        defaultRulesBookShouldNotBeFound("result.in=" + UPDATED_RESULT);
    }

    @Test
    @Transactional
    public void getAllRulesBooksByResultIsNullOrNotNull() throws Exception {
        // Initialize the database
        rulesBookRepository.saveAndFlush(rulesBook);

        // Get all the rulesBookList where result is not null
        defaultRulesBookShouldBeFound("result.specified=true");

        // Get all the rulesBookList where result is null
        defaultRulesBookShouldNotBeFound("result.specified=false");
    }

    @Test
    @Transactional
    public void getAllRulesBooksByResultIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        rulesBookRepository.saveAndFlush(rulesBook);

        // Get all the rulesBookList where result greater than or equals to DEFAULT_RESULT
        defaultRulesBookShouldBeFound("result.greaterOrEqualThan=" + DEFAULT_RESULT);

        // Get all the rulesBookList where result greater than or equals to UPDATED_RESULT
        defaultRulesBookShouldNotBeFound("result.greaterOrEqualThan=" + UPDATED_RESULT);
    }

    @Test
    @Transactional
    public void getAllRulesBooksByResultIsLessThanSomething() throws Exception {
        // Initialize the database
        rulesBookRepository.saveAndFlush(rulesBook);

        // Get all the rulesBookList where result less than or equals to DEFAULT_RESULT
        defaultRulesBookShouldNotBeFound("result.lessThan=" + DEFAULT_RESULT);

        // Get all the rulesBookList where result less than or equals to UPDATED_RESULT
        defaultRulesBookShouldBeFound("result.lessThan=" + UPDATED_RESULT);
    }


    @Test
    @Transactional
    public void getAllRulesBooksByDescriptionIsEqualToSomething() throws Exception {
        // Initialize the database
        rulesBookRepository.saveAndFlush(rulesBook);

        // Get all the rulesBookList where description equals to DEFAULT_DESCRIPTION
        defaultRulesBookShouldBeFound("description.equals=" + DEFAULT_DESCRIPTION);

        // Get all the rulesBookList where description equals to UPDATED_DESCRIPTION
        defaultRulesBookShouldNotBeFound("description.equals=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllRulesBooksByDescriptionIsInShouldWork() throws Exception {
        // Initialize the database
        rulesBookRepository.saveAndFlush(rulesBook);

        // Get all the rulesBookList where description in DEFAULT_DESCRIPTION or UPDATED_DESCRIPTION
        defaultRulesBookShouldBeFound("description.in=" + DEFAULT_DESCRIPTION + "," + UPDATED_DESCRIPTION);

        // Get all the rulesBookList where description equals to UPDATED_DESCRIPTION
        defaultRulesBookShouldNotBeFound("description.in=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllRulesBooksByDescriptionIsNullOrNotNull() throws Exception {
        // Initialize the database
        rulesBookRepository.saveAndFlush(rulesBook);

        // Get all the rulesBookList where description is not null
        defaultRulesBookShouldBeFound("description.specified=true");

        // Get all the rulesBookList where description is null
        defaultRulesBookShouldNotBeFound("description.specified=false");
    }
    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultRulesBookShouldBeFound(String filter) throws Exception {
        restRulesBookMockMvc.perform(get("/api/rules-books?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(rulesBook.getId().intValue())))
            .andExpect(jsonPath("$.[*].ruleNumber").value(hasItem(DEFAULT_RULE_NUMBER.toString())))
            .andExpect(jsonPath("$.[*].ruleSequence").value(hasItem(DEFAULT_RULE_SEQUENCE)))
            .andExpect(jsonPath("$.[*].key").value(hasItem(DEFAULT_KEY)))
            .andExpect(jsonPath("$.[*].lookupPlace1").value(hasItem(DEFAULT_LOOKUP_PLACE_1.toString())))
            .andExpect(jsonPath("$.[*].operator1").value(hasItem(DEFAULT_OPERATOR_1.toString())))
            .andExpect(jsonPath("$.[*].value1").value(hasItem(DEFAULT_VALUE_1.toString())))
            .andExpect(jsonPath("$.[*].joinField1").value(hasItem(DEFAULT_JOIN_FIELD_1.toString())))
            .andExpect(jsonPath("$.[*].lookupPlace2").value(hasItem(DEFAULT_LOOKUP_PLACE_2.toString())))
            .andExpect(jsonPath("$.[*].operator2").value(hasItem(DEFAULT_OPERATOR_2.toString())))
            .andExpect(jsonPath("$.[*].value2").value(hasItem(DEFAULT_VALUE_2.toString())))
            .andExpect(jsonPath("$.[*].joinField2").value(hasItem(DEFAULT_JOIN_FIELD_2.toString())))
            .andExpect(jsonPath("$.[*].lookupPlace3").value(hasItem(DEFAULT_LOOKUP_PLACE_3.toString())))
            .andExpect(jsonPath("$.[*].operator3").value(hasItem(DEFAULT_OPERATOR_3.toString())))
            .andExpect(jsonPath("$.[*].value3").value(hasItem(DEFAULT_VALUE_3.toString())))
            .andExpect(jsonPath("$.[*].result").value(hasItem(DEFAULT_RESULT)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultRulesBookShouldNotBeFound(String filter) throws Exception {
        restRulesBookMockMvc.perform(get("/api/rules-books?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingRulesBook() throws Exception {
        // Get the rulesBook
        restRulesBookMockMvc.perform(get("/api/rules-books/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRulesBook() throws Exception {
        // Initialize the database
        rulesBookRepository.saveAndFlush(rulesBook);

        int databaseSizeBeforeUpdate = rulesBookRepository.findAll().size();

        // Update the rulesBook
        RulesBook updatedRulesBook = rulesBookRepository.findById(rulesBook.getId()).get();
        // Disconnect from session so that the updates on updatedRulesBook are not directly saved in db
        em.detach(updatedRulesBook);
        updatedRulesBook
            .ruleNumber(UPDATED_RULE_NUMBER)
            .ruleSequence(UPDATED_RULE_SEQUENCE)
            .key(UPDATED_KEY)
            .lookupPlace1(UPDATED_LOOKUP_PLACE_1)
            .operator1(UPDATED_OPERATOR_1)
            .value1(UPDATED_VALUE_1)
            .joinField1(UPDATED_JOIN_FIELD_1)
            .lookupPlace2(UPDATED_LOOKUP_PLACE_2)
            .operator2(UPDATED_OPERATOR_2)
            .value2(UPDATED_VALUE_2)
            .joinField2(UPDATED_JOIN_FIELD_2)
            .lookupPlace3(UPDATED_LOOKUP_PLACE_3)
            .operator3(UPDATED_OPERATOR_3)
            .value3(UPDATED_VALUE_3)
            .result(UPDATED_RESULT)
            .description(UPDATED_DESCRIPTION);
        RulesBookDTO rulesBookDTO = rulesBookMapper.toDto(updatedRulesBook);

        restRulesBookMockMvc.perform(put("/api/rules-books")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(rulesBookDTO)))
            .andExpect(status().isOk());

        // Validate the RulesBook in the database
        List<RulesBook> rulesBookList = rulesBookRepository.findAll();
        assertThat(rulesBookList).hasSize(databaseSizeBeforeUpdate);
        RulesBook testRulesBook = rulesBookList.get(rulesBookList.size() - 1);
        assertThat(testRulesBook.getRuleNumber()).isEqualTo(UPDATED_RULE_NUMBER);
        assertThat(testRulesBook.getRuleSequence()).isEqualTo(UPDATED_RULE_SEQUENCE);
        assertThat(testRulesBook.getKey()).isEqualTo(UPDATED_KEY);
        assertThat(testRulesBook.getLookupPlace1()).isEqualTo(UPDATED_LOOKUP_PLACE_1);
        assertThat(testRulesBook.getOperator1()).isEqualTo(UPDATED_OPERATOR_1);
        assertThat(testRulesBook.getValue1()).isEqualTo(UPDATED_VALUE_1);
        assertThat(testRulesBook.getJoinField1()).isEqualTo(UPDATED_JOIN_FIELD_1);
        assertThat(testRulesBook.getLookupPlace2()).isEqualTo(UPDATED_LOOKUP_PLACE_2);
        assertThat(testRulesBook.getOperator2()).isEqualTo(UPDATED_OPERATOR_2);
        assertThat(testRulesBook.getValue2()).isEqualTo(UPDATED_VALUE_2);
        assertThat(testRulesBook.getJoinField2()).isEqualTo(UPDATED_JOIN_FIELD_2);
        assertThat(testRulesBook.getLookupPlace3()).isEqualTo(UPDATED_LOOKUP_PLACE_3);
        assertThat(testRulesBook.getOperator3()).isEqualTo(UPDATED_OPERATOR_3);
        assertThat(testRulesBook.getValue3()).isEqualTo(UPDATED_VALUE_3);
        assertThat(testRulesBook.getResult()).isEqualTo(UPDATED_RESULT);
        assertThat(testRulesBook.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void updateNonExistingRulesBook() throws Exception {
        int databaseSizeBeforeUpdate = rulesBookRepository.findAll().size();

        // Create the RulesBook
        RulesBookDTO rulesBookDTO = rulesBookMapper.toDto(rulesBook);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRulesBookMockMvc.perform(put("/api/rules-books")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(rulesBookDTO)))
            .andExpect(status().isBadRequest());

        // Validate the RulesBook in the database
        List<RulesBook> rulesBookList = rulesBookRepository.findAll();
        assertThat(rulesBookList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteRulesBook() throws Exception {
        // Initialize the database
        rulesBookRepository.saveAndFlush(rulesBook);

        int databaseSizeBeforeDelete = rulesBookRepository.findAll().size();

        // Get the rulesBook
        restRulesBookMockMvc.perform(delete("/api/rules-books/{id}", rulesBook.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<RulesBook> rulesBookList = rulesBookRepository.findAll();
        assertThat(rulesBookList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RulesBook.class);
        RulesBook rulesBook1 = new RulesBook();
        rulesBook1.setId(1L);
        RulesBook rulesBook2 = new RulesBook();
        rulesBook2.setId(rulesBook1.getId());
        assertThat(rulesBook1).isEqualTo(rulesBook2);
        rulesBook2.setId(2L);
        assertThat(rulesBook1).isNotEqualTo(rulesBook2);
        rulesBook1.setId(null);
        assertThat(rulesBook1).isNotEqualTo(rulesBook2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(RulesBookDTO.class);
        RulesBookDTO rulesBookDTO1 = new RulesBookDTO();
        rulesBookDTO1.setId(1L);
        RulesBookDTO rulesBookDTO2 = new RulesBookDTO();
        assertThat(rulesBookDTO1).isNotEqualTo(rulesBookDTO2);
        rulesBookDTO2.setId(rulesBookDTO1.getId());
        assertThat(rulesBookDTO1).isEqualTo(rulesBookDTO2);
        rulesBookDTO2.setId(2L);
        assertThat(rulesBookDTO1).isNotEqualTo(rulesBookDTO2);
        rulesBookDTO1.setId(null);
        assertThat(rulesBookDTO1).isNotEqualTo(rulesBookDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(rulesBookMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(rulesBookMapper.fromId(null)).isNull();
    }
}
