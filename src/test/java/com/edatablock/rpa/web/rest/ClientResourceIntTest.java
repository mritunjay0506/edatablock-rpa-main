package com.edatablock.rpa.web.rest;

import com.edatablock.rpa.EdatablockrpamainApp;

import com.edatablock.rpa.domain.Client;
import com.edatablock.rpa.domain.Organization;
import com.edatablock.rpa.domain.InputTemplate;
import com.edatablock.rpa.repository.ClientRepository;
import com.edatablock.rpa.service.ClientService;
import com.edatablock.rpa.service.dto.ClientDTO;
import com.edatablock.rpa.service.mapper.ClientMapper;
import com.edatablock.rpa.web.rest.errors.ExceptionTranslator;
import com.edatablock.rpa.service.dto.ClientCriteria;
import com.edatablock.rpa.service.ClientQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;


import static com.edatablock.rpa.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ClientResource REST controller.
 *
 * @see ClientResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = EdatablockrpamainApp.class)
public class ClientResourceIntTest {

    private static final String DEFAULT_CLIENT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_CLIENT_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_CLIENT_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_CLIENT_ADDRESS = "BBBBBBBBBB";

    private static final String DEFAULT_CLIENT_EMAIL_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_CLIENT_EMAIL_ADDRESS = "BBBBBBBBBB";

    private static final Integer DEFAULT_IS_ACTIVE = 1;
    private static final Integer UPDATED_IS_ACTIVE = 2;

    private static final Instant DEFAULT_CREATE_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATE_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_UPDATE_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_UPDATE_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_IS_MERGED_DOCUMENT = 1;
    private static final Integer UPDATED_IS_MERGED_DOCUMENT = 2;

    private static final String DEFAULT_UPDATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_UPDATED_BY = "BBBBBBBBBB";

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private ClientMapper clientMapper;
    
    @Autowired
    private ClientService clientService;

    @Autowired
    private ClientQueryService clientQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restClientMockMvc;

    private Client client;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ClientResource clientResource = new ClientResource(clientService, clientQueryService);
        this.restClientMockMvc = MockMvcBuilders.standaloneSetup(clientResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Client createEntity(EntityManager em) {
        Client client = new Client()
            .clientName(DEFAULT_CLIENT_NAME)
            .description(DEFAULT_DESCRIPTION)
            .clientAddress(DEFAULT_CLIENT_ADDRESS)
            .clientEmailAddress(DEFAULT_CLIENT_EMAIL_ADDRESS)
            .isActive(DEFAULT_IS_ACTIVE)
            .createDate(DEFAULT_CREATE_DATE)
            .createdBy(DEFAULT_CREATED_BY)
            .updateDate(DEFAULT_UPDATE_DATE)
            .isMergedDocument(DEFAULT_IS_MERGED_DOCUMENT)
            .updatedBy(DEFAULT_UPDATED_BY);
        return client;
    }

    @Before
    public void initTest() {
        client = createEntity(em);
    }

    @Test
    @Transactional
    public void createClient() throws Exception {
        int databaseSizeBeforeCreate = clientRepository.findAll().size();

        // Create the Client
        ClientDTO clientDTO = clientMapper.toDto(client);
        restClientMockMvc.perform(post("/api/clients")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientDTO)))
            .andExpect(status().isCreated());

        // Validate the Client in the database
        List<Client> clientList = clientRepository.findAll();
        assertThat(clientList).hasSize(databaseSizeBeforeCreate + 1);
        Client testClient = clientList.get(clientList.size() - 1);
        assertThat(testClient.getClientName()).isEqualTo(DEFAULT_CLIENT_NAME);
        assertThat(testClient.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testClient.getClientAddress()).isEqualTo(DEFAULT_CLIENT_ADDRESS);
        assertThat(testClient.getClientEmailAddress()).isEqualTo(DEFAULT_CLIENT_EMAIL_ADDRESS);
        assertThat(testClient.getIsActive()).isEqualTo(DEFAULT_IS_ACTIVE);
        assertThat(testClient.getCreateDate()).isEqualTo(DEFAULT_CREATE_DATE);
        assertThat(testClient.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testClient.getUpdateDate()).isEqualTo(DEFAULT_UPDATE_DATE);
        assertThat(testClient.getIsMergedDocument()).isEqualTo(DEFAULT_IS_MERGED_DOCUMENT);
        assertThat(testClient.getUpdatedBy()).isEqualTo(DEFAULT_UPDATED_BY);
    }

    @Test
    @Transactional
    public void createClientWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = clientRepository.findAll().size();

        // Create the Client with an existing ID
        client.setId(1L);
        ClientDTO clientDTO = clientMapper.toDto(client);

        // An entity with an existing ID cannot be created, so this API call must fail
        restClientMockMvc.perform(post("/api/clients")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Client in the database
        List<Client> clientList = clientRepository.findAll();
        assertThat(clientList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkClientNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = clientRepository.findAll().size();
        // set the field null
        client.setClientName(null);

        // Create the Client, which fails.
        ClientDTO clientDTO = clientMapper.toDto(client);

        restClientMockMvc.perform(post("/api/clients")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientDTO)))
            .andExpect(status().isBadRequest());

        List<Client> clientList = clientRepository.findAll();
        assertThat(clientList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkClientEmailAddressIsRequired() throws Exception {
        int databaseSizeBeforeTest = clientRepository.findAll().size();
        // set the field null
        client.setClientEmailAddress(null);

        // Create the Client, which fails.
        ClientDTO clientDTO = clientMapper.toDto(client);

        restClientMockMvc.perform(post("/api/clients")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientDTO)))
            .andExpect(status().isBadRequest());

        List<Client> clientList = clientRepository.findAll();
        assertThat(clientList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllClients() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList
        restClientMockMvc.perform(get("/api/clients?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(client.getId().intValue())))
            .andExpect(jsonPath("$.[*].clientName").value(hasItem(DEFAULT_CLIENT_NAME.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].clientAddress").value(hasItem(DEFAULT_CLIENT_ADDRESS.toString())))
            .andExpect(jsonPath("$.[*].clientEmailAddress").value(hasItem(DEFAULT_CLIENT_EMAIL_ADDRESS.toString())))
            .andExpect(jsonPath("$.[*].isActive").value(hasItem(DEFAULT_IS_ACTIVE)))
            .andExpect(jsonPath("$.[*].createDate").value(hasItem(DEFAULT_CREATE_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY.toString())))
            .andExpect(jsonPath("$.[*].updateDate").value(hasItem(DEFAULT_UPDATE_DATE.toString())))
            .andExpect(jsonPath("$.[*].isMergedDocument").value(hasItem(DEFAULT_IS_MERGED_DOCUMENT)))
            .andExpect(jsonPath("$.[*].updatedBy").value(hasItem(DEFAULT_UPDATED_BY.toString())));
    }
    
    @Test
    @Transactional
    public void getClient() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get the client
        restClientMockMvc.perform(get("/api/clients/{id}", client.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(client.getId().intValue()))
            .andExpect(jsonPath("$.clientName").value(DEFAULT_CLIENT_NAME.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.clientAddress").value(DEFAULT_CLIENT_ADDRESS.toString()))
            .andExpect(jsonPath("$.clientEmailAddress").value(DEFAULT_CLIENT_EMAIL_ADDRESS.toString()))
            .andExpect(jsonPath("$.isActive").value(DEFAULT_IS_ACTIVE))
            .andExpect(jsonPath("$.createDate").value(DEFAULT_CREATE_DATE.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY.toString()))
            .andExpect(jsonPath("$.updateDate").value(DEFAULT_UPDATE_DATE.toString()))
            .andExpect(jsonPath("$.isMergedDocument").value(DEFAULT_IS_MERGED_DOCUMENT))
            .andExpect(jsonPath("$.updatedBy").value(DEFAULT_UPDATED_BY.toString()));
    }

    @Test
    @Transactional
    public void getAllClientsByClientNameIsEqualToSomething() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where clientName equals to DEFAULT_CLIENT_NAME
        defaultClientShouldBeFound("clientName.equals=" + DEFAULT_CLIENT_NAME);

        // Get all the clientList where clientName equals to UPDATED_CLIENT_NAME
        defaultClientShouldNotBeFound("clientName.equals=" + UPDATED_CLIENT_NAME);
    }

    @Test
    @Transactional
    public void getAllClientsByClientNameIsInShouldWork() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where clientName in DEFAULT_CLIENT_NAME or UPDATED_CLIENT_NAME
        defaultClientShouldBeFound("clientName.in=" + DEFAULT_CLIENT_NAME + "," + UPDATED_CLIENT_NAME);

        // Get all the clientList where clientName equals to UPDATED_CLIENT_NAME
        defaultClientShouldNotBeFound("clientName.in=" + UPDATED_CLIENT_NAME);
    }

    @Test
    @Transactional
    public void getAllClientsByClientNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where clientName is not null
        defaultClientShouldBeFound("clientName.specified=true");

        // Get all the clientList where clientName is null
        defaultClientShouldNotBeFound("clientName.specified=false");
    }

    @Test
    @Transactional
    public void getAllClientsByDescriptionIsEqualToSomething() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where description equals to DEFAULT_DESCRIPTION
        defaultClientShouldBeFound("description.equals=" + DEFAULT_DESCRIPTION);

        // Get all the clientList where description equals to UPDATED_DESCRIPTION
        defaultClientShouldNotBeFound("description.equals=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllClientsByDescriptionIsInShouldWork() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where description in DEFAULT_DESCRIPTION or UPDATED_DESCRIPTION
        defaultClientShouldBeFound("description.in=" + DEFAULT_DESCRIPTION + "," + UPDATED_DESCRIPTION);

        // Get all the clientList where description equals to UPDATED_DESCRIPTION
        defaultClientShouldNotBeFound("description.in=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllClientsByDescriptionIsNullOrNotNull() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where description is not null
        defaultClientShouldBeFound("description.specified=true");

        // Get all the clientList where description is null
        defaultClientShouldNotBeFound("description.specified=false");
    }

    @Test
    @Transactional
    public void getAllClientsByClientAddressIsEqualToSomething() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where clientAddress equals to DEFAULT_CLIENT_ADDRESS
        defaultClientShouldBeFound("clientAddress.equals=" + DEFAULT_CLIENT_ADDRESS);

        // Get all the clientList where clientAddress equals to UPDATED_CLIENT_ADDRESS
        defaultClientShouldNotBeFound("clientAddress.equals=" + UPDATED_CLIENT_ADDRESS);
    }

    @Test
    @Transactional
    public void getAllClientsByClientAddressIsInShouldWork() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where clientAddress in DEFAULT_CLIENT_ADDRESS or UPDATED_CLIENT_ADDRESS
        defaultClientShouldBeFound("clientAddress.in=" + DEFAULT_CLIENT_ADDRESS + "," + UPDATED_CLIENT_ADDRESS);

        // Get all the clientList where clientAddress equals to UPDATED_CLIENT_ADDRESS
        defaultClientShouldNotBeFound("clientAddress.in=" + UPDATED_CLIENT_ADDRESS);
    }

    @Test
    @Transactional
    public void getAllClientsByClientAddressIsNullOrNotNull() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where clientAddress is not null
        defaultClientShouldBeFound("clientAddress.specified=true");

        // Get all the clientList where clientAddress is null
        defaultClientShouldNotBeFound("clientAddress.specified=false");
    }

    @Test
    @Transactional
    public void getAllClientsByClientEmailAddressIsEqualToSomething() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where clientEmailAddress equals to DEFAULT_CLIENT_EMAIL_ADDRESS
        defaultClientShouldBeFound("clientEmailAddress.equals=" + DEFAULT_CLIENT_EMAIL_ADDRESS);

        // Get all the clientList where clientEmailAddress equals to UPDATED_CLIENT_EMAIL_ADDRESS
        defaultClientShouldNotBeFound("clientEmailAddress.equals=" + UPDATED_CLIENT_EMAIL_ADDRESS);
    }

    @Test
    @Transactional
    public void getAllClientsByClientEmailAddressIsInShouldWork() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where clientEmailAddress in DEFAULT_CLIENT_EMAIL_ADDRESS or UPDATED_CLIENT_EMAIL_ADDRESS
        defaultClientShouldBeFound("clientEmailAddress.in=" + DEFAULT_CLIENT_EMAIL_ADDRESS + "," + UPDATED_CLIENT_EMAIL_ADDRESS);

        // Get all the clientList where clientEmailAddress equals to UPDATED_CLIENT_EMAIL_ADDRESS
        defaultClientShouldNotBeFound("clientEmailAddress.in=" + UPDATED_CLIENT_EMAIL_ADDRESS);
    }

    @Test
    @Transactional
    public void getAllClientsByClientEmailAddressIsNullOrNotNull() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where clientEmailAddress is not null
        defaultClientShouldBeFound("clientEmailAddress.specified=true");

        // Get all the clientList where clientEmailAddress is null
        defaultClientShouldNotBeFound("clientEmailAddress.specified=false");
    }

    @Test
    @Transactional
    public void getAllClientsByIsActiveIsEqualToSomething() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where isActive equals to DEFAULT_IS_ACTIVE
        defaultClientShouldBeFound("isActive.equals=" + DEFAULT_IS_ACTIVE);

        // Get all the clientList where isActive equals to UPDATED_IS_ACTIVE
        defaultClientShouldNotBeFound("isActive.equals=" + UPDATED_IS_ACTIVE);
    }

    @Test
    @Transactional
    public void getAllClientsByIsActiveIsInShouldWork() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where isActive in DEFAULT_IS_ACTIVE or UPDATED_IS_ACTIVE
        defaultClientShouldBeFound("isActive.in=" + DEFAULT_IS_ACTIVE + "," + UPDATED_IS_ACTIVE);

        // Get all the clientList where isActive equals to UPDATED_IS_ACTIVE
        defaultClientShouldNotBeFound("isActive.in=" + UPDATED_IS_ACTIVE);
    }

    @Test
    @Transactional
    public void getAllClientsByIsActiveIsNullOrNotNull() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where isActive is not null
        defaultClientShouldBeFound("isActive.specified=true");

        // Get all the clientList where isActive is null
        defaultClientShouldNotBeFound("isActive.specified=false");
    }

    @Test
    @Transactional
    public void getAllClientsByIsActiveIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where isActive greater than or equals to DEFAULT_IS_ACTIVE
        defaultClientShouldBeFound("isActive.greaterOrEqualThan=" + DEFAULT_IS_ACTIVE);

        // Get all the clientList where isActive greater than or equals to UPDATED_IS_ACTIVE
        defaultClientShouldNotBeFound("isActive.greaterOrEqualThan=" + UPDATED_IS_ACTIVE);
    }

    @Test
    @Transactional
    public void getAllClientsByIsActiveIsLessThanSomething() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where isActive less than or equals to DEFAULT_IS_ACTIVE
        defaultClientShouldNotBeFound("isActive.lessThan=" + DEFAULT_IS_ACTIVE);

        // Get all the clientList where isActive less than or equals to UPDATED_IS_ACTIVE
        defaultClientShouldBeFound("isActive.lessThan=" + UPDATED_IS_ACTIVE);
    }


    @Test
    @Transactional
    public void getAllClientsByCreateDateIsEqualToSomething() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where createDate equals to DEFAULT_CREATE_DATE
        defaultClientShouldBeFound("createDate.equals=" + DEFAULT_CREATE_DATE);

        // Get all the clientList where createDate equals to UPDATED_CREATE_DATE
        defaultClientShouldNotBeFound("createDate.equals=" + UPDATED_CREATE_DATE);
    }

    @Test
    @Transactional
    public void getAllClientsByCreateDateIsInShouldWork() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where createDate in DEFAULT_CREATE_DATE or UPDATED_CREATE_DATE
        defaultClientShouldBeFound("createDate.in=" + DEFAULT_CREATE_DATE + "," + UPDATED_CREATE_DATE);

        // Get all the clientList where createDate equals to UPDATED_CREATE_DATE
        defaultClientShouldNotBeFound("createDate.in=" + UPDATED_CREATE_DATE);
    }

    @Test
    @Transactional
    public void getAllClientsByCreateDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where createDate is not null
        defaultClientShouldBeFound("createDate.specified=true");

        // Get all the clientList where createDate is null
        defaultClientShouldNotBeFound("createDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllClientsByCreatedByIsEqualToSomething() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where createdBy equals to DEFAULT_CREATED_BY
        defaultClientShouldBeFound("createdBy.equals=" + DEFAULT_CREATED_BY);

        // Get all the clientList where createdBy equals to UPDATED_CREATED_BY
        defaultClientShouldNotBeFound("createdBy.equals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllClientsByCreatedByIsInShouldWork() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where createdBy in DEFAULT_CREATED_BY or UPDATED_CREATED_BY
        defaultClientShouldBeFound("createdBy.in=" + DEFAULT_CREATED_BY + "," + UPDATED_CREATED_BY);

        // Get all the clientList where createdBy equals to UPDATED_CREATED_BY
        defaultClientShouldNotBeFound("createdBy.in=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllClientsByCreatedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where createdBy is not null
        defaultClientShouldBeFound("createdBy.specified=true");

        // Get all the clientList where createdBy is null
        defaultClientShouldNotBeFound("createdBy.specified=false");
    }

    @Test
    @Transactional
    public void getAllClientsByUpdateDateIsEqualToSomething() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where updateDate equals to DEFAULT_UPDATE_DATE
        defaultClientShouldBeFound("updateDate.equals=" + DEFAULT_UPDATE_DATE);

        // Get all the clientList where updateDate equals to UPDATED_UPDATE_DATE
        defaultClientShouldNotBeFound("updateDate.equals=" + UPDATED_UPDATE_DATE);
    }

    @Test
    @Transactional
    public void getAllClientsByUpdateDateIsInShouldWork() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where updateDate in DEFAULT_UPDATE_DATE or UPDATED_UPDATE_DATE
        defaultClientShouldBeFound("updateDate.in=" + DEFAULT_UPDATE_DATE + "," + UPDATED_UPDATE_DATE);

        // Get all the clientList where updateDate equals to UPDATED_UPDATE_DATE
        defaultClientShouldNotBeFound("updateDate.in=" + UPDATED_UPDATE_DATE);
    }

    @Test
    @Transactional
    public void getAllClientsByUpdateDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where updateDate is not null
        defaultClientShouldBeFound("updateDate.specified=true");

        // Get all the clientList where updateDate is null
        defaultClientShouldNotBeFound("updateDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllClientsByIsMergedDocumentIsEqualToSomething() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where isMergedDocument equals to DEFAULT_IS_MERGED_DOCUMENT
        defaultClientShouldBeFound("isMergedDocument.equals=" + DEFAULT_IS_MERGED_DOCUMENT);

        // Get all the clientList where isMergedDocument equals to UPDATED_IS_MERGED_DOCUMENT
        defaultClientShouldNotBeFound("isMergedDocument.equals=" + UPDATED_IS_MERGED_DOCUMENT);
    }

    @Test
    @Transactional
    public void getAllClientsByIsMergedDocumentIsInShouldWork() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where isMergedDocument in DEFAULT_IS_MERGED_DOCUMENT or UPDATED_IS_MERGED_DOCUMENT
        defaultClientShouldBeFound("isMergedDocument.in=" + DEFAULT_IS_MERGED_DOCUMENT + "," + UPDATED_IS_MERGED_DOCUMENT);

        // Get all the clientList where isMergedDocument equals to UPDATED_IS_MERGED_DOCUMENT
        defaultClientShouldNotBeFound("isMergedDocument.in=" + UPDATED_IS_MERGED_DOCUMENT);
    }

    @Test
    @Transactional
    public void getAllClientsByIsMergedDocumentIsNullOrNotNull() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where isMergedDocument is not null
        defaultClientShouldBeFound("isMergedDocument.specified=true");

        // Get all the clientList where isMergedDocument is null
        defaultClientShouldNotBeFound("isMergedDocument.specified=false");
    }

    @Test
    @Transactional
    public void getAllClientsByIsMergedDocumentIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where isMergedDocument greater than or equals to DEFAULT_IS_MERGED_DOCUMENT
        defaultClientShouldBeFound("isMergedDocument.greaterOrEqualThan=" + DEFAULT_IS_MERGED_DOCUMENT);

        // Get all the clientList where isMergedDocument greater than or equals to UPDATED_IS_MERGED_DOCUMENT
        defaultClientShouldNotBeFound("isMergedDocument.greaterOrEqualThan=" + UPDATED_IS_MERGED_DOCUMENT);
    }

    @Test
    @Transactional
    public void getAllClientsByIsMergedDocumentIsLessThanSomething() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where isMergedDocument less than or equals to DEFAULT_IS_MERGED_DOCUMENT
        defaultClientShouldNotBeFound("isMergedDocument.lessThan=" + DEFAULT_IS_MERGED_DOCUMENT);

        // Get all the clientList where isMergedDocument less than or equals to UPDATED_IS_MERGED_DOCUMENT
        defaultClientShouldBeFound("isMergedDocument.lessThan=" + UPDATED_IS_MERGED_DOCUMENT);
    }


    @Test
    @Transactional
    public void getAllClientsByUpdatedByIsEqualToSomething() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where updatedBy equals to DEFAULT_UPDATED_BY
        defaultClientShouldBeFound("updatedBy.equals=" + DEFAULT_UPDATED_BY);

        // Get all the clientList where updatedBy equals to UPDATED_UPDATED_BY
        defaultClientShouldNotBeFound("updatedBy.equals=" + UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    public void getAllClientsByUpdatedByIsInShouldWork() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where updatedBy in DEFAULT_UPDATED_BY or UPDATED_UPDATED_BY
        defaultClientShouldBeFound("updatedBy.in=" + DEFAULT_UPDATED_BY + "," + UPDATED_UPDATED_BY);

        // Get all the clientList where updatedBy equals to UPDATED_UPDATED_BY
        defaultClientShouldNotBeFound("updatedBy.in=" + UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    public void getAllClientsByUpdatedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where updatedBy is not null
        defaultClientShouldBeFound("updatedBy.specified=true");

        // Get all the clientList where updatedBy is null
        defaultClientShouldNotBeFound("updatedBy.specified=false");
    }

    @Test
    @Transactional
    public void getAllClientsByOrgNameIsEqualToSomething() throws Exception {
        // Initialize the database
        Organization orgName = OrganizationResourceIntTest.createEntity(em);
        em.persist(orgName);
        em.flush();
        client.setOrgName(orgName);
        clientRepository.saveAndFlush(client);
        Long orgNameId = orgName.getId();

        // Get all the clientList where orgName equals to orgNameId
        defaultClientShouldBeFound("orgNameId.equals=" + orgNameId);

        // Get all the clientList where orgName equals to orgNameId + 1
        defaultClientShouldNotBeFound("orgNameId.equals=" + (orgNameId + 1));
    }


    @Test
    @Transactional
    public void getAllClientsByInputTemplateIsEqualToSomething() throws Exception {
        // Initialize the database
        InputTemplate inputTemplate = InputTemplateResourceIntTest.createEntity(em);
        em.persist(inputTemplate);
        em.flush();
        client.addInputTemplate(inputTemplate);
        clientRepository.saveAndFlush(client);
        Long inputTemplateId = inputTemplate.getId();

        // Get all the clientList where inputTemplate equals to inputTemplateId
        defaultClientShouldBeFound("inputTemplateId.equals=" + inputTemplateId);

        // Get all the clientList where inputTemplate equals to inputTemplateId + 1
        defaultClientShouldNotBeFound("inputTemplateId.equals=" + (inputTemplateId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultClientShouldBeFound(String filter) throws Exception {
        restClientMockMvc.perform(get("/api/clients?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(client.getId().intValue())))
            .andExpect(jsonPath("$.[*].clientName").value(hasItem(DEFAULT_CLIENT_NAME.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].clientAddress").value(hasItem(DEFAULT_CLIENT_ADDRESS.toString())))
            .andExpect(jsonPath("$.[*].clientEmailAddress").value(hasItem(DEFAULT_CLIENT_EMAIL_ADDRESS.toString())))
            .andExpect(jsonPath("$.[*].isActive").value(hasItem(DEFAULT_IS_ACTIVE)))
            .andExpect(jsonPath("$.[*].createDate").value(hasItem(DEFAULT_CREATE_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY.toString())))
            .andExpect(jsonPath("$.[*].updateDate").value(hasItem(DEFAULT_UPDATE_DATE.toString())))
            .andExpect(jsonPath("$.[*].isMergedDocument").value(hasItem(DEFAULT_IS_MERGED_DOCUMENT)))
            .andExpect(jsonPath("$.[*].updatedBy").value(hasItem(DEFAULT_UPDATED_BY.toString())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultClientShouldNotBeFound(String filter) throws Exception {
        restClientMockMvc.perform(get("/api/clients?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingClient() throws Exception {
        // Get the client
        restClientMockMvc.perform(get("/api/clients/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateClient() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        int databaseSizeBeforeUpdate = clientRepository.findAll().size();

        // Update the client
        Client updatedClient = clientRepository.findById(client.getId()).get();
        // Disconnect from session so that the updates on updatedClient are not directly saved in db
        em.detach(updatedClient);
        updatedClient
            .clientName(UPDATED_CLIENT_NAME)
            .description(UPDATED_DESCRIPTION)
            .clientAddress(UPDATED_CLIENT_ADDRESS)
            .clientEmailAddress(UPDATED_CLIENT_EMAIL_ADDRESS)
            .isActive(UPDATED_IS_ACTIVE)
            .createDate(UPDATED_CREATE_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .updateDate(UPDATED_UPDATE_DATE)
            .isMergedDocument(UPDATED_IS_MERGED_DOCUMENT)
            .updatedBy(UPDATED_UPDATED_BY);
        ClientDTO clientDTO = clientMapper.toDto(updatedClient);

        restClientMockMvc.perform(put("/api/clients")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientDTO)))
            .andExpect(status().isOk());

        // Validate the Client in the database
        List<Client> clientList = clientRepository.findAll();
        assertThat(clientList).hasSize(databaseSizeBeforeUpdate);
        Client testClient = clientList.get(clientList.size() - 1);
        assertThat(testClient.getClientName()).isEqualTo(UPDATED_CLIENT_NAME);
        assertThat(testClient.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testClient.getClientAddress()).isEqualTo(UPDATED_CLIENT_ADDRESS);
        assertThat(testClient.getClientEmailAddress()).isEqualTo(UPDATED_CLIENT_EMAIL_ADDRESS);
        assertThat(testClient.getIsActive()).isEqualTo(UPDATED_IS_ACTIVE);
        assertThat(testClient.getCreateDate()).isEqualTo(UPDATED_CREATE_DATE);
        assertThat(testClient.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testClient.getUpdateDate()).isEqualTo(UPDATED_UPDATE_DATE);
        assertThat(testClient.getIsMergedDocument()).isEqualTo(UPDATED_IS_MERGED_DOCUMENT);
        assertThat(testClient.getUpdatedBy()).isEqualTo(UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    public void updateNonExistingClient() throws Exception {
        int databaseSizeBeforeUpdate = clientRepository.findAll().size();

        // Create the Client
        ClientDTO clientDTO = clientMapper.toDto(client);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restClientMockMvc.perform(put("/api/clients")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Client in the database
        List<Client> clientList = clientRepository.findAll();
        assertThat(clientList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteClient() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        int databaseSizeBeforeDelete = clientRepository.findAll().size();

        // Get the client
        restClientMockMvc.perform(delete("/api/clients/{id}", client.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Client> clientList = clientRepository.findAll();
        assertThat(clientList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Client.class);
        Client client1 = new Client();
        client1.setId(1L);
        Client client2 = new Client();
        client2.setId(client1.getId());
        assertThat(client1).isEqualTo(client2);
        client2.setId(2L);
        assertThat(client1).isNotEqualTo(client2);
        client1.setId(null);
        assertThat(client1).isNotEqualTo(client2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ClientDTO.class);
        ClientDTO clientDTO1 = new ClientDTO();
        clientDTO1.setId(1L);
        ClientDTO clientDTO2 = new ClientDTO();
        assertThat(clientDTO1).isNotEqualTo(clientDTO2);
        clientDTO2.setId(clientDTO1.getId());
        assertThat(clientDTO1).isEqualTo(clientDTO2);
        clientDTO2.setId(2L);
        assertThat(clientDTO1).isNotEqualTo(clientDTO2);
        clientDTO1.setId(null);
        assertThat(clientDTO1).isNotEqualTo(clientDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(clientMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(clientMapper.fromId(null)).isNull();
    }
}
