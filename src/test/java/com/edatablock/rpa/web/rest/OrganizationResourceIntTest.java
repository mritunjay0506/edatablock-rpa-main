package com.edatablock.rpa.web.rest;

import com.edatablock.rpa.EdatablockrpamainApp;

import com.edatablock.rpa.domain.Organization;
import com.edatablock.rpa.repository.OrganizationRepository;
import com.edatablock.rpa.service.OrganizationService;
import com.edatablock.rpa.service.dto.OrganizationDTO;
import com.edatablock.rpa.service.mapper.OrganizationMapper;
import com.edatablock.rpa.web.rest.errors.ExceptionTranslator;
import com.edatablock.rpa.service.dto.OrganizationCriteria;
import com.edatablock.rpa.service.OrganizationQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;


import static com.edatablock.rpa.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the OrganizationResource REST controller.
 *
 * @see OrganizationResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = EdatablockrpamainApp.class)
public class OrganizationResourceIntTest {

    private static final String DEFAULT_ORG_NAME = "AAAAAAAAAA";
    private static final String UPDATED_ORG_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_ORG_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_ORG_ADDRESS = "BBBBBBBBBB";

    private static final String DEFAULT_ORG_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_ORG_EMAIL = "BBBBBBBBBB";

    private static final Integer DEFAULT_IS_ACTIVE = 1;
    private static final Integer UPDATED_IS_ACTIVE = 2;

    private static final Instant DEFAULT_CREATE_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATE_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_UPDATE_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_UPDATE_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_UPDATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_UPDATED_BY = "BBBBBBBBBB";

    @Autowired
    private OrganizationRepository organizationRepository;

    @Autowired
    private OrganizationMapper organizationMapper;
    
    @Autowired
    private OrganizationService organizationService;

    @Autowired
    private OrganizationQueryService organizationQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restOrganizationMockMvc;

    private Organization organization;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final OrganizationResource organizationResource = new OrganizationResource(organizationService, organizationQueryService);
        this.restOrganizationMockMvc = MockMvcBuilders.standaloneSetup(organizationResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Organization createEntity(EntityManager em) {
        Organization organization = new Organization()
            .orgName(DEFAULT_ORG_NAME)
            .description(DEFAULT_DESCRIPTION)
            .orgAddress(DEFAULT_ORG_ADDRESS)
            .orgEmail(DEFAULT_ORG_EMAIL)
            .isActive(DEFAULT_IS_ACTIVE)
            .createDate(DEFAULT_CREATE_DATE)
            .createdBy(DEFAULT_CREATED_BY)
            .updateDate(DEFAULT_UPDATE_DATE)
            .updatedBy(DEFAULT_UPDATED_BY);
        return organization;
    }

    @Before
    public void initTest() {
        organization = createEntity(em);
    }

    @Test
    @Transactional
    public void createOrganization() throws Exception {
        int databaseSizeBeforeCreate = organizationRepository.findAll().size();

        // Create the Organization
        OrganizationDTO organizationDTO = organizationMapper.toDto(organization);
        restOrganizationMockMvc.perform(post("/api/organizations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(organizationDTO)))
            .andExpect(status().isCreated());

        // Validate the Organization in the database
        List<Organization> organizationList = organizationRepository.findAll();
        assertThat(organizationList).hasSize(databaseSizeBeforeCreate + 1);
        Organization testOrganization = organizationList.get(organizationList.size() - 1);
        assertThat(testOrganization.getOrgName()).isEqualTo(DEFAULT_ORG_NAME);
        assertThat(testOrganization.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testOrganization.getOrgAddress()).isEqualTo(DEFAULT_ORG_ADDRESS);
        assertThat(testOrganization.getOrgEmail()).isEqualTo(DEFAULT_ORG_EMAIL);
        assertThat(testOrganization.getIsActive()).isEqualTo(DEFAULT_IS_ACTIVE);
        assertThat(testOrganization.getCreateDate()).isEqualTo(DEFAULT_CREATE_DATE);
        assertThat(testOrganization.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testOrganization.getUpdateDate()).isEqualTo(DEFAULT_UPDATE_DATE);
        assertThat(testOrganization.getUpdatedBy()).isEqualTo(DEFAULT_UPDATED_BY);
    }

    @Test
    @Transactional
    public void createOrganizationWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = organizationRepository.findAll().size();

        // Create the Organization with an existing ID
        organization.setId(1L);
        OrganizationDTO organizationDTO = organizationMapper.toDto(organization);

        // An entity with an existing ID cannot be created, so this API call must fail
        restOrganizationMockMvc.perform(post("/api/organizations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(organizationDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Organization in the database
        List<Organization> organizationList = organizationRepository.findAll();
        assertThat(organizationList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkOrgNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = organizationRepository.findAll().size();
        // set the field null
        organization.setOrgName(null);

        // Create the Organization, which fails.
        OrganizationDTO organizationDTO = organizationMapper.toDto(organization);

        restOrganizationMockMvc.perform(post("/api/organizations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(organizationDTO)))
            .andExpect(status().isBadRequest());

        List<Organization> organizationList = organizationRepository.findAll();
        assertThat(organizationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkOrgEmailIsRequired() throws Exception {
        int databaseSizeBeforeTest = organizationRepository.findAll().size();
        // set the field null
        organization.setOrgEmail(null);

        // Create the Organization, which fails.
        OrganizationDTO organizationDTO = organizationMapper.toDto(organization);

        restOrganizationMockMvc.perform(post("/api/organizations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(organizationDTO)))
            .andExpect(status().isBadRequest());

        List<Organization> organizationList = organizationRepository.findAll();
        assertThat(organizationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllOrganizations() throws Exception {
        // Initialize the database
        organizationRepository.saveAndFlush(organization);

        // Get all the organizationList
        restOrganizationMockMvc.perform(get("/api/organizations?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(organization.getId().intValue())))
            .andExpect(jsonPath("$.[*].orgName").value(hasItem(DEFAULT_ORG_NAME.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].orgAddress").value(hasItem(DEFAULT_ORG_ADDRESS.toString())))
            .andExpect(jsonPath("$.[*].orgEmail").value(hasItem(DEFAULT_ORG_EMAIL.toString())))
            .andExpect(jsonPath("$.[*].isActive").value(hasItem(DEFAULT_IS_ACTIVE)))
            .andExpect(jsonPath("$.[*].createDate").value(hasItem(DEFAULT_CREATE_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY.toString())))
            .andExpect(jsonPath("$.[*].updateDate").value(hasItem(DEFAULT_UPDATE_DATE.toString())))
            .andExpect(jsonPath("$.[*].updatedBy").value(hasItem(DEFAULT_UPDATED_BY.toString())));
    }
    
    @Test
    @Transactional
    public void getOrganization() throws Exception {
        // Initialize the database
        organizationRepository.saveAndFlush(organization);

        // Get the organization
        restOrganizationMockMvc.perform(get("/api/organizations/{id}", organization.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(organization.getId().intValue()))
            .andExpect(jsonPath("$.orgName").value(DEFAULT_ORG_NAME.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.orgAddress").value(DEFAULT_ORG_ADDRESS.toString()))
            .andExpect(jsonPath("$.orgEmail").value(DEFAULT_ORG_EMAIL.toString()))
            .andExpect(jsonPath("$.isActive").value(DEFAULT_IS_ACTIVE))
            .andExpect(jsonPath("$.createDate").value(DEFAULT_CREATE_DATE.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY.toString()))
            .andExpect(jsonPath("$.updateDate").value(DEFAULT_UPDATE_DATE.toString()))
            .andExpect(jsonPath("$.updatedBy").value(DEFAULT_UPDATED_BY.toString()));
    }

    @Test
    @Transactional
    public void getAllOrganizationsByOrgNameIsEqualToSomething() throws Exception {
        // Initialize the database
        organizationRepository.saveAndFlush(organization);

        // Get all the organizationList where orgName equals to DEFAULT_ORG_NAME
        defaultOrganizationShouldBeFound("orgName.equals=" + DEFAULT_ORG_NAME);

        // Get all the organizationList where orgName equals to UPDATED_ORG_NAME
        defaultOrganizationShouldNotBeFound("orgName.equals=" + UPDATED_ORG_NAME);
    }

    @Test
    @Transactional
    public void getAllOrganizationsByOrgNameIsInShouldWork() throws Exception {
        // Initialize the database
        organizationRepository.saveAndFlush(organization);

        // Get all the organizationList where orgName in DEFAULT_ORG_NAME or UPDATED_ORG_NAME
        defaultOrganizationShouldBeFound("orgName.in=" + DEFAULT_ORG_NAME + "," + UPDATED_ORG_NAME);

        // Get all the organizationList where orgName equals to UPDATED_ORG_NAME
        defaultOrganizationShouldNotBeFound("orgName.in=" + UPDATED_ORG_NAME);
    }

    @Test
    @Transactional
    public void getAllOrganizationsByOrgNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        organizationRepository.saveAndFlush(organization);

        // Get all the organizationList where orgName is not null
        defaultOrganizationShouldBeFound("orgName.specified=true");

        // Get all the organizationList where orgName is null
        defaultOrganizationShouldNotBeFound("orgName.specified=false");
    }

    @Test
    @Transactional
    public void getAllOrganizationsByDescriptionIsEqualToSomething() throws Exception {
        // Initialize the database
        organizationRepository.saveAndFlush(organization);

        // Get all the organizationList where description equals to DEFAULT_DESCRIPTION
        defaultOrganizationShouldBeFound("description.equals=" + DEFAULT_DESCRIPTION);

        // Get all the organizationList where description equals to UPDATED_DESCRIPTION
        defaultOrganizationShouldNotBeFound("description.equals=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllOrganizationsByDescriptionIsInShouldWork() throws Exception {
        // Initialize the database
        organizationRepository.saveAndFlush(organization);

        // Get all the organizationList where description in DEFAULT_DESCRIPTION or UPDATED_DESCRIPTION
        defaultOrganizationShouldBeFound("description.in=" + DEFAULT_DESCRIPTION + "," + UPDATED_DESCRIPTION);

        // Get all the organizationList where description equals to UPDATED_DESCRIPTION
        defaultOrganizationShouldNotBeFound("description.in=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllOrganizationsByDescriptionIsNullOrNotNull() throws Exception {
        // Initialize the database
        organizationRepository.saveAndFlush(organization);

        // Get all the organizationList where description is not null
        defaultOrganizationShouldBeFound("description.specified=true");

        // Get all the organizationList where description is null
        defaultOrganizationShouldNotBeFound("description.specified=false");
    }

    @Test
    @Transactional
    public void getAllOrganizationsByOrgAddressIsEqualToSomething() throws Exception {
        // Initialize the database
        organizationRepository.saveAndFlush(organization);

        // Get all the organizationList where orgAddress equals to DEFAULT_ORG_ADDRESS
        defaultOrganizationShouldBeFound("orgAddress.equals=" + DEFAULT_ORG_ADDRESS);

        // Get all the organizationList where orgAddress equals to UPDATED_ORG_ADDRESS
        defaultOrganizationShouldNotBeFound("orgAddress.equals=" + UPDATED_ORG_ADDRESS);
    }

    @Test
    @Transactional
    public void getAllOrganizationsByOrgAddressIsInShouldWork() throws Exception {
        // Initialize the database
        organizationRepository.saveAndFlush(organization);

        // Get all the organizationList where orgAddress in DEFAULT_ORG_ADDRESS or UPDATED_ORG_ADDRESS
        defaultOrganizationShouldBeFound("orgAddress.in=" + DEFAULT_ORG_ADDRESS + "," + UPDATED_ORG_ADDRESS);

        // Get all the organizationList where orgAddress equals to UPDATED_ORG_ADDRESS
        defaultOrganizationShouldNotBeFound("orgAddress.in=" + UPDATED_ORG_ADDRESS);
    }

    @Test
    @Transactional
    public void getAllOrganizationsByOrgAddressIsNullOrNotNull() throws Exception {
        // Initialize the database
        organizationRepository.saveAndFlush(organization);

        // Get all the organizationList where orgAddress is not null
        defaultOrganizationShouldBeFound("orgAddress.specified=true");

        // Get all the organizationList where orgAddress is null
        defaultOrganizationShouldNotBeFound("orgAddress.specified=false");
    }

    @Test
    @Transactional
    public void getAllOrganizationsByOrgEmailIsEqualToSomething() throws Exception {
        // Initialize the database
        organizationRepository.saveAndFlush(organization);

        // Get all the organizationList where orgEmail equals to DEFAULT_ORG_EMAIL
        defaultOrganizationShouldBeFound("orgEmail.equals=" + DEFAULT_ORG_EMAIL);

        // Get all the organizationList where orgEmail equals to UPDATED_ORG_EMAIL
        defaultOrganizationShouldNotBeFound("orgEmail.equals=" + UPDATED_ORG_EMAIL);
    }

    @Test
    @Transactional
    public void getAllOrganizationsByOrgEmailIsInShouldWork() throws Exception {
        // Initialize the database
        organizationRepository.saveAndFlush(organization);

        // Get all the organizationList where orgEmail in DEFAULT_ORG_EMAIL or UPDATED_ORG_EMAIL
        defaultOrganizationShouldBeFound("orgEmail.in=" + DEFAULT_ORG_EMAIL + "," + UPDATED_ORG_EMAIL);

        // Get all the organizationList where orgEmail equals to UPDATED_ORG_EMAIL
        defaultOrganizationShouldNotBeFound("orgEmail.in=" + UPDATED_ORG_EMAIL);
    }

    @Test
    @Transactional
    public void getAllOrganizationsByOrgEmailIsNullOrNotNull() throws Exception {
        // Initialize the database
        organizationRepository.saveAndFlush(organization);

        // Get all the organizationList where orgEmail is not null
        defaultOrganizationShouldBeFound("orgEmail.specified=true");

        // Get all the organizationList where orgEmail is null
        defaultOrganizationShouldNotBeFound("orgEmail.specified=false");
    }

    @Test
    @Transactional
    public void getAllOrganizationsByIsActiveIsEqualToSomething() throws Exception {
        // Initialize the database
        organizationRepository.saveAndFlush(organization);

        // Get all the organizationList where isActive equals to DEFAULT_IS_ACTIVE
        defaultOrganizationShouldBeFound("isActive.equals=" + DEFAULT_IS_ACTIVE);

        // Get all the organizationList where isActive equals to UPDATED_IS_ACTIVE
        defaultOrganizationShouldNotBeFound("isActive.equals=" + UPDATED_IS_ACTIVE);
    }

    @Test
    @Transactional
    public void getAllOrganizationsByIsActiveIsInShouldWork() throws Exception {
        // Initialize the database
        organizationRepository.saveAndFlush(organization);

        // Get all the organizationList where isActive in DEFAULT_IS_ACTIVE or UPDATED_IS_ACTIVE
        defaultOrganizationShouldBeFound("isActive.in=" + DEFAULT_IS_ACTIVE + "," + UPDATED_IS_ACTIVE);

        // Get all the organizationList where isActive equals to UPDATED_IS_ACTIVE
        defaultOrganizationShouldNotBeFound("isActive.in=" + UPDATED_IS_ACTIVE);
    }

    @Test
    @Transactional
    public void getAllOrganizationsByIsActiveIsNullOrNotNull() throws Exception {
        // Initialize the database
        organizationRepository.saveAndFlush(organization);

        // Get all the organizationList where isActive is not null
        defaultOrganizationShouldBeFound("isActive.specified=true");

        // Get all the organizationList where isActive is null
        defaultOrganizationShouldNotBeFound("isActive.specified=false");
    }

    @Test
    @Transactional
    public void getAllOrganizationsByIsActiveIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        organizationRepository.saveAndFlush(organization);

        // Get all the organizationList where isActive greater than or equals to DEFAULT_IS_ACTIVE
        defaultOrganizationShouldBeFound("isActive.greaterOrEqualThan=" + DEFAULT_IS_ACTIVE);

        // Get all the organizationList where isActive greater than or equals to UPDATED_IS_ACTIVE
        defaultOrganizationShouldNotBeFound("isActive.greaterOrEqualThan=" + UPDATED_IS_ACTIVE);
    }

    @Test
    @Transactional
    public void getAllOrganizationsByIsActiveIsLessThanSomething() throws Exception {
        // Initialize the database
        organizationRepository.saveAndFlush(organization);

        // Get all the organizationList where isActive less than or equals to DEFAULT_IS_ACTIVE
        defaultOrganizationShouldNotBeFound("isActive.lessThan=" + DEFAULT_IS_ACTIVE);

        // Get all the organizationList where isActive less than or equals to UPDATED_IS_ACTIVE
        defaultOrganizationShouldBeFound("isActive.lessThan=" + UPDATED_IS_ACTIVE);
    }


    @Test
    @Transactional
    public void getAllOrganizationsByCreateDateIsEqualToSomething() throws Exception {
        // Initialize the database
        organizationRepository.saveAndFlush(organization);

        // Get all the organizationList where createDate equals to DEFAULT_CREATE_DATE
        defaultOrganizationShouldBeFound("createDate.equals=" + DEFAULT_CREATE_DATE);

        // Get all the organizationList where createDate equals to UPDATED_CREATE_DATE
        defaultOrganizationShouldNotBeFound("createDate.equals=" + UPDATED_CREATE_DATE);
    }

    @Test
    @Transactional
    public void getAllOrganizationsByCreateDateIsInShouldWork() throws Exception {
        // Initialize the database
        organizationRepository.saveAndFlush(organization);

        // Get all the organizationList where createDate in DEFAULT_CREATE_DATE or UPDATED_CREATE_DATE
        defaultOrganizationShouldBeFound("createDate.in=" + DEFAULT_CREATE_DATE + "," + UPDATED_CREATE_DATE);

        // Get all the organizationList where createDate equals to UPDATED_CREATE_DATE
        defaultOrganizationShouldNotBeFound("createDate.in=" + UPDATED_CREATE_DATE);
    }

    @Test
    @Transactional
    public void getAllOrganizationsByCreateDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        organizationRepository.saveAndFlush(organization);

        // Get all the organizationList where createDate is not null
        defaultOrganizationShouldBeFound("createDate.specified=true");

        // Get all the organizationList where createDate is null
        defaultOrganizationShouldNotBeFound("createDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllOrganizationsByCreatedByIsEqualToSomething() throws Exception {
        // Initialize the database
        organizationRepository.saveAndFlush(organization);

        // Get all the organizationList where createdBy equals to DEFAULT_CREATED_BY
        defaultOrganizationShouldBeFound("createdBy.equals=" + DEFAULT_CREATED_BY);

        // Get all the organizationList where createdBy equals to UPDATED_CREATED_BY
        defaultOrganizationShouldNotBeFound("createdBy.equals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllOrganizationsByCreatedByIsInShouldWork() throws Exception {
        // Initialize the database
        organizationRepository.saveAndFlush(organization);

        // Get all the organizationList where createdBy in DEFAULT_CREATED_BY or UPDATED_CREATED_BY
        defaultOrganizationShouldBeFound("createdBy.in=" + DEFAULT_CREATED_BY + "," + UPDATED_CREATED_BY);

        // Get all the organizationList where createdBy equals to UPDATED_CREATED_BY
        defaultOrganizationShouldNotBeFound("createdBy.in=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllOrganizationsByCreatedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        organizationRepository.saveAndFlush(organization);

        // Get all the organizationList where createdBy is not null
        defaultOrganizationShouldBeFound("createdBy.specified=true");

        // Get all the organizationList where createdBy is null
        defaultOrganizationShouldNotBeFound("createdBy.specified=false");
    }

    @Test
    @Transactional
    public void getAllOrganizationsByUpdateDateIsEqualToSomething() throws Exception {
        // Initialize the database
        organizationRepository.saveAndFlush(organization);

        // Get all the organizationList where updateDate equals to DEFAULT_UPDATE_DATE
        defaultOrganizationShouldBeFound("updateDate.equals=" + DEFAULT_UPDATE_DATE);

        // Get all the organizationList where updateDate equals to UPDATED_UPDATE_DATE
        defaultOrganizationShouldNotBeFound("updateDate.equals=" + UPDATED_UPDATE_DATE);
    }

    @Test
    @Transactional
    public void getAllOrganizationsByUpdateDateIsInShouldWork() throws Exception {
        // Initialize the database
        organizationRepository.saveAndFlush(organization);

        // Get all the organizationList where updateDate in DEFAULT_UPDATE_DATE or UPDATED_UPDATE_DATE
        defaultOrganizationShouldBeFound("updateDate.in=" + DEFAULT_UPDATE_DATE + "," + UPDATED_UPDATE_DATE);

        // Get all the organizationList where updateDate equals to UPDATED_UPDATE_DATE
        defaultOrganizationShouldNotBeFound("updateDate.in=" + UPDATED_UPDATE_DATE);
    }

    @Test
    @Transactional
    public void getAllOrganizationsByUpdateDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        organizationRepository.saveAndFlush(organization);

        // Get all the organizationList where updateDate is not null
        defaultOrganizationShouldBeFound("updateDate.specified=true");

        // Get all the organizationList where updateDate is null
        defaultOrganizationShouldNotBeFound("updateDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllOrganizationsByUpdatedByIsEqualToSomething() throws Exception {
        // Initialize the database
        organizationRepository.saveAndFlush(organization);

        // Get all the organizationList where updatedBy equals to DEFAULT_UPDATED_BY
        defaultOrganizationShouldBeFound("updatedBy.equals=" + DEFAULT_UPDATED_BY);

        // Get all the organizationList where updatedBy equals to UPDATED_UPDATED_BY
        defaultOrganizationShouldNotBeFound("updatedBy.equals=" + UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    public void getAllOrganizationsByUpdatedByIsInShouldWork() throws Exception {
        // Initialize the database
        organizationRepository.saveAndFlush(organization);

        // Get all the organizationList where updatedBy in DEFAULT_UPDATED_BY or UPDATED_UPDATED_BY
        defaultOrganizationShouldBeFound("updatedBy.in=" + DEFAULT_UPDATED_BY + "," + UPDATED_UPDATED_BY);

        // Get all the organizationList where updatedBy equals to UPDATED_UPDATED_BY
        defaultOrganizationShouldNotBeFound("updatedBy.in=" + UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    public void getAllOrganizationsByUpdatedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        organizationRepository.saveAndFlush(organization);

        // Get all the organizationList where updatedBy is not null
        defaultOrganizationShouldBeFound("updatedBy.specified=true");

        // Get all the organizationList where updatedBy is null
        defaultOrganizationShouldNotBeFound("updatedBy.specified=false");
    }
    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultOrganizationShouldBeFound(String filter) throws Exception {
        restOrganizationMockMvc.perform(get("/api/organizations?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(organization.getId().intValue())))
            .andExpect(jsonPath("$.[*].orgName").value(hasItem(DEFAULT_ORG_NAME.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].orgAddress").value(hasItem(DEFAULT_ORG_ADDRESS.toString())))
            .andExpect(jsonPath("$.[*].orgEmail").value(hasItem(DEFAULT_ORG_EMAIL.toString())))
            .andExpect(jsonPath("$.[*].isActive").value(hasItem(DEFAULT_IS_ACTIVE)))
            .andExpect(jsonPath("$.[*].createDate").value(hasItem(DEFAULT_CREATE_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY.toString())))
            .andExpect(jsonPath("$.[*].updateDate").value(hasItem(DEFAULT_UPDATE_DATE.toString())))
            .andExpect(jsonPath("$.[*].updatedBy").value(hasItem(DEFAULT_UPDATED_BY.toString())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultOrganizationShouldNotBeFound(String filter) throws Exception {
        restOrganizationMockMvc.perform(get("/api/organizations?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingOrganization() throws Exception {
        // Get the organization
        restOrganizationMockMvc.perform(get("/api/organizations/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateOrganization() throws Exception {
        // Initialize the database
        organizationRepository.saveAndFlush(organization);

        int databaseSizeBeforeUpdate = organizationRepository.findAll().size();

        // Update the organization
        Organization updatedOrganization = organizationRepository.findById(organization.getId()).get();
        // Disconnect from session so that the updates on updatedOrganization are not directly saved in db
        em.detach(updatedOrganization);
        updatedOrganization
            .orgName(UPDATED_ORG_NAME)
            .description(UPDATED_DESCRIPTION)
            .orgAddress(UPDATED_ORG_ADDRESS)
            .orgEmail(UPDATED_ORG_EMAIL)
            .isActive(UPDATED_IS_ACTIVE)
            .createDate(UPDATED_CREATE_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .updateDate(UPDATED_UPDATE_DATE)
            .updatedBy(UPDATED_UPDATED_BY);
        OrganizationDTO organizationDTO = organizationMapper.toDto(updatedOrganization);

        restOrganizationMockMvc.perform(put("/api/organizations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(organizationDTO)))
            .andExpect(status().isOk());

        // Validate the Organization in the database
        List<Organization> organizationList = organizationRepository.findAll();
        assertThat(organizationList).hasSize(databaseSizeBeforeUpdate);
        Organization testOrganization = organizationList.get(organizationList.size() - 1);
        assertThat(testOrganization.getOrgName()).isEqualTo(UPDATED_ORG_NAME);
        assertThat(testOrganization.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testOrganization.getOrgAddress()).isEqualTo(UPDATED_ORG_ADDRESS);
        assertThat(testOrganization.getOrgEmail()).isEqualTo(UPDATED_ORG_EMAIL);
        assertThat(testOrganization.getIsActive()).isEqualTo(UPDATED_IS_ACTIVE);
        assertThat(testOrganization.getCreateDate()).isEqualTo(UPDATED_CREATE_DATE);
        assertThat(testOrganization.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testOrganization.getUpdateDate()).isEqualTo(UPDATED_UPDATE_DATE);
        assertThat(testOrganization.getUpdatedBy()).isEqualTo(UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    public void updateNonExistingOrganization() throws Exception {
        int databaseSizeBeforeUpdate = organizationRepository.findAll().size();

        // Create the Organization
        OrganizationDTO organizationDTO = organizationMapper.toDto(organization);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOrganizationMockMvc.perform(put("/api/organizations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(organizationDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Organization in the database
        List<Organization> organizationList = organizationRepository.findAll();
        assertThat(organizationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteOrganization() throws Exception {
        // Initialize the database
        organizationRepository.saveAndFlush(organization);

        int databaseSizeBeforeDelete = organizationRepository.findAll().size();

        // Get the organization
        restOrganizationMockMvc.perform(delete("/api/organizations/{id}", organization.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Organization> organizationList = organizationRepository.findAll();
        assertThat(organizationList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Organization.class);
        Organization organization1 = new Organization();
        organization1.setId(1L);
        Organization organization2 = new Organization();
        organization2.setId(organization1.getId());
        assertThat(organization1).isEqualTo(organization2);
        organization2.setId(2L);
        assertThat(organization1).isNotEqualTo(organization2);
        organization1.setId(null);
        assertThat(organization1).isNotEqualTo(organization2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(OrganizationDTO.class);
        OrganizationDTO organizationDTO1 = new OrganizationDTO();
        organizationDTO1.setId(1L);
        OrganizationDTO organizationDTO2 = new OrganizationDTO();
        assertThat(organizationDTO1).isNotEqualTo(organizationDTO2);
        organizationDTO2.setId(organizationDTO1.getId());
        assertThat(organizationDTO1).isEqualTo(organizationDTO2);
        organizationDTO2.setId(2L);
        assertThat(organizationDTO1).isNotEqualTo(organizationDTO2);
        organizationDTO1.setId(null);
        assertThat(organizationDTO1).isNotEqualTo(organizationDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(organizationMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(organizationMapper.fromId(null)).isNull();
    }
}
